package it.polito.malnatidid.poleato.util;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Firebase;

import it.polito.malnatidid.poleato.activities.HomeCustActivity;
import it.polito.malnatidid.poleato.R;
import it.polito.malnatidid.poleato.activities.CustProfileActivity;
import it.polito.malnatidid.poleato.activities.ResProfileActivity;

public class Toolbar {

    public static void menuClick(ImageView img, final Activity a) {
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DrawerLayout) a.findViewById(R.id.drawer_layout)).openDrawer(GravityCompat.START);
            }
        });
    }

    public static void homeClick(ImageView img, final Activity a) {
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(a, HomeCustActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                a.startActivity(i);

            }
        });
    }

    private static void homeResClick(ImageView img, final Activity a) {
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                a.onBackPressed();
            }
        });
    }

    public static void profileInResClick(ImageView img, final Activity a) {
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(a.getApplicationContext(), ResProfileActivity.class);
                a.startActivity(i);
            }
        });
    }

    public static void profileClick(ImageView img, final Activity a) {
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(a.getApplicationContext(), CustProfileActivity.class);
                a.startActivity(i);
            }
        });
    }

    public static void logoutClick(ImageView img, final Activity a, final Firebase logger) {
        img.setVisibility(View.VISIBLE);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logger.unauth();
                Toast.makeText(a.getApplicationContext(), "Logged out!", Toast.LENGTH_LONG).show();
                Intent i = new Intent(a, HomeCustActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                a.startActivity(i);
            }
        });
    }

    public static void onCreateToolbar(final Activity a) {

        ImageView menu = (ImageView) a.findViewById(R.id.menu_button);
        ImageView home = (ImageView) a.findViewById(R.id.home_button);
        ImageView list = (ImageView) a.findViewById(R.id.list_button);
        ImageView profileIn = (ImageView) a.findViewById(R.id.profile_in);
        ImageView profileOut = (ImageView) a.findViewById(R.id.profile_out);
//        ImageView notify = (ImageView) a.findViewById(R.id.notify_button);
        ImageView logout = (ImageView) a.findViewById(R.id.logout_button);

        final Firebase logger = new Firebase("https://poleato.firebaseio.com/");

        switch (a.getLocalClassName()) {

            case ".activities.MapsActivity":
                break;

            //###################
            //    RESTAURANT
            //###################

            case "activities.HomeResActivity":

                profileOut.setVisibility(View.GONE);
                profileInResClick(profileIn, a);
//                notifyClick(notify, a);

                break;

            case "activities.ResProfileActivity":

                homeResClick(list, a);
                profileOut.setVisibility(View.GONE);
                if (logger.getAuth() != null) {
                    logoutClick(logout, a, logger);
                }
//                notifyClick(notify, a);

                break;

            //###################
            //    CUSTOMER
            //###################

            case "activities.HomeCustActivity":

                menuClick(menu, a);
                if (logger.getAuth() != null) {
                    profileOut.setVisibility(View.GONE);
                    profileClick(profileIn, a);
//                    notifyClick(notify, a);

//                    history.setVisibility(View.VISIBLE);
//                    history.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            Intent i = new Intent(a.getResources().getString(R.string.pending_table), null, a.getApplicationContext(), CustProfileActivity.class);
//                            a.startActivity(i);
//                        }
//                    });
                } else {
                    profileIn.setVisibility(View.GONE);
                    profileClick(profileOut, a);
//                    history.setVisibility(View.GONE);
                }

                break;

            case "activities.ListActivity":

                homeClick(home, a);

                if (logger.getAuth() != null) {
                    profileOut.setVisibility(View.GONE);
                    profileClick(profileIn, a);
//                    notifyClick(notify, a);

//                    history.setVisibility(View.VISIBLE);
//                    history.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            Intent i = new Intent(a.getResources().getString(R.string.pending_table), null, a.getApplicationContext(), CustProfileActivity.class);
//                            a.startActivity(i);
//                        }
//                    });
                } else {
                    profileIn.setVisibility(View.GONE);
                    profileClick(profileOut, a);
//                    history.setVisibility(View.GONE);
                }
                break;

            case "activities.CustProfileActivity":

                homeClick(home, a);
                profileOut.setVisibility(View.GONE);
                if (logger.getAuth() != null) {
                    logoutClick(logout, a, logger);
//                    notifyClick(notify, a);
                }

                break;

            case "activities.CustRestaurantActivity":

                homeClick(home, a);
                if (logger.getAuth() != null) {
                    profileOut.setVisibility(View.GONE);
                    profileClick(profileIn, a);
//                    notifyClick(notify, a);

//                    history.setVisibility(View.VISIBLE);
//                    history.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            Intent i = new Intent(a.getResources().getString(R.string.pending_table), null, a.getApplicationContext(), CustProfileActivity.class);
//                            a.startActivity(i);
//                        }
//                    });
                } else {
                    profileIn.setVisibility(View.GONE);
                    profileClick(profileOut, a);
//                    history.setVisibility(View.GONE);
                }

                break;
        }
    }

    public static void setTitle(Activity a, String s) {
        TextView tv = (TextView) a.findViewById(R.id.toolbarTitle);
        tv.setText(s);
    }
}
