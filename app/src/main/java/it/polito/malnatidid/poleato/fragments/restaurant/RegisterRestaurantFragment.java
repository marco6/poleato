package it.polito.malnatidid.poleato.fragments.restaurant;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;

import java.util.Map;

import it.polito.malnatidid.poleato.R;
import it.polito.malnatidid.poleato.data.Coordinates;
import it.polito.malnatidid.poleato.data.Evaluation;
import it.polito.malnatidid.poleato.data.FBProfileRestaurant;
import it.polito.malnatidid.poleato.data.Timetable;
import it.polito.malnatidid.poleato.databinding.RegistrationResBinding;
import it.polito.malnatidid.poleato.fragments.LoadingDialogFragment;
import it.polito.malnatidid.poleato.fragments.LoginFragment;
import it.polito.malnatidid.poleato.util.MapsUtility;
import it.polito.malnatidid.poleato.util.Triplet;

public class RegisterRestaurantFragment extends Fragment implements View.OnClickListener, Firebase.ValueResultHandler<Map<String, Object>> {

    private RegistrationResBinding binding;
    private LoginFragment loginFragment;
    private String mail, password;
    private Firebase logger = new Firebase("https://poleato.firebaseio.com/");
    private LoadingDialogFragment loading = new LoadingDialogFragment();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        loginFragment = (LoginFragment) getFragmentManager().findFragmentByTag("login");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = RegistrationResBinding.inflate(inflater, container, false);
        binding.setOnOk(this);
        return binding.getRoot();
    }

    @Override
    public void onClick(View v) {

        mail = binding.emailEdit.getText().toString();
        password = binding.passwordEdit.getText().toString();
        if (password.equals(binding.passwordConfirmEdit.getText().toString())) {
            //TODO: check email with regex
            logger.createUser(mail, password, this);
            loading.show(getFragmentManager(), "loading");
        } else {
            Toast.makeText(getContext(), "Password mismatch!", Toast.LENGTH_LONG).show();
            binding.passwordConfirmEdit.requestFocus();
        }
    }

    @Override
    public void onSuccess(Map<String, Object> result) {
        loading.dismiss();
        /*
         * Force keyboard closure when return on InfoFragment
         */
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        getFragmentManager().popBackStack();
        Toast.makeText(getContext(), "User created! Logging...", Toast.LENGTH_LONG).show();

        // Ok ora proviamo ad autenticarci subito!
        loginFragment.login(mail, password);

        String uid = result.get("uid").toString();
        FBProfileRestaurant profile = new FBProfileRestaurant(uid);

        profile.setName(binding.nameEdit.getText().toString());
        profile.setAddress(String.format("%s, %s, %s", binding.streetAddressEdit.getText().toString(), binding.civicAddressEdit.getText().toString(), binding.cityAddressEdit.getText().toString()));
        profile.setPhone(binding.phoneEdit.getText().toString());
        profile.setDelivery(binding.delivery.isChecked());
        profile.setSelfService(binding.selfService.isChecked());
        profile.setTakeAway(binding.takeaway.isChecked());
        profile.setServed(binding.tableService.isChecked());

        profile.setPizza(binding.pizza.isChecked());
        profile.setAsian(binding.asian.isChecked());
        profile.setCeliac(binding.celiac.isChecked());
        profile.setGreek(binding.greek.isChecked());
        profile.setFastFood(binding.fastfood.isChecked());
        profile.setIndian(binding.indian.isChecked());
        profile.setKebab(binding.kebab.isChecked());
        profile.setMexican(binding.mexican.isChecked());
        profile.setVegetarian(binding.vegetarian.isChecked());
        profile.setOther(binding.others.isChecked());
        profile.setDescription(binding.descriptionEdit.getText().toString());

        String newAddress = String.format("%s, %s, %s",
                binding.streetAddressEdit.getText().toString(),
                binding.civicAddressEdit.getText().toString(),
                binding.cityAddressEdit.getText().toString());

        Triplet<Double, Double, String> t = MapsUtility.getCoordinates(getActivity(), newAddress);

        if (t != null) {
            GeoFire geoFire = new GeoFire(logger.child("geofire"));
            profile.setAddress(newAddress);
            profile.setCoordinates(new Coordinates(t.first, t.second));
            geoFire.setLocation(uid, new GeoLocation(t.first, t.second));
        } else
            Toast.makeText(getActivity(), R.string.address_error, Toast.LENGTH_LONG).show();


        profile.setEvaluation(new Evaluation());
        profile.setTimetable(new Timetable());

        profile.commit();
    }

    @Override
    public void onError(FirebaseError firebaseError) {
        loading.dismiss();
        Toast.makeText(getContext(), firebaseError.getMessage(), Toast.LENGTH_LONG).show();
    }
}
