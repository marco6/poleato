package it.polito.malnatidid.poleato.fragments.customer;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import java.util.Map;

import it.polito.malnatidid.poleato.data.FBProfileCustomer;
import it.polito.malnatidid.poleato.databinding.RegistrationCustBinding;
import it.polito.malnatidid.poleato.fragments.LoadingDialogFragment;
import it.polito.malnatidid.poleato.fragments.LoginFragment;

public class RegisterClientFragment extends Fragment implements View.OnClickListener, Firebase.ValueResultHandler<Map<String, Object>> {
    private RegistrationCustBinding binding;
    private LoginFragment loginFragment;
    private String mail, password;
    private Firebase logger = new Firebase("https://poleato.firebaseio.com/");
    private LoadingDialogFragment loading = new LoadingDialogFragment();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        loginFragment = (LoginFragment)getFragmentManager().findFragmentByTag("login");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = RegistrationCustBinding.inflate(inflater, container, false);
        binding.setOnOk(this);
        return binding.getRoot();
    }

    @Override
    public void onClick(View v) {
        mail = binding.email.getText().toString();
        password = binding.password.getText().toString();
        if (password.equals(binding.passwordConfirm.getText().toString())) {

            logger.createUser(mail, password, this);
            loading.show(getFragmentManager(), "loading");
        } else {
            Toast.makeText(getContext(), "Password mismatch!", Toast.LENGTH_LONG).show();
            binding.passwordConfirm.requestFocus();
        }
    }

    @Override
    public void onSuccess(Map<String, Object> result) {
        loading.dismiss();
        /*
         * Force keyboard closure when return on InfoFragment
         */
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

        loginFragment.register = true;

        // Ok ora proviamo ad autenticarci subito!
        loginFragment.login(mail, password);

        String uid = result.get("uid").toString();
        FBProfileCustomer profile = new FBProfileCustomer(uid);
        profile.setMail(binding.email.getText().toString());
        profile.commit();

        getFragmentManager().popBackStack();
        Toast.makeText(getContext(), "User created! Logging...", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onError(FirebaseError firebaseError) {
        loading.dismiss();
        Toast.makeText(getContext(), firebaseError.getMessage(), Toast.LENGTH_LONG).show();
    }
}
