package it.polito.malnatidid.poleato.util;

import android.databinding.ObservableField;
import android.graphics.Bitmap;

public class CachedBitmap extends ObservableField<Bitmap> {

    public CachedBitmap(String id) {
        super();
        // Questa era facile.
        this.set(FBImageCache.getInstance().get(id, new FBImageCache.BitmapLoaded() {
            @Override
            public void loaded(String key, Bitmap value) {
                CachedBitmap.this.set(value);
            }
        }));
    }
}
