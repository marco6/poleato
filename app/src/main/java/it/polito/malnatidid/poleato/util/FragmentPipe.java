package it.polito.malnatidid.poleato.util;

import android.os.Parcel;
import android.os.Parcelable;

import java.lang.ref.WeakReference;
import java.util.HashMap;


public class FragmentPipe<T> implements Parcelable {

    /**
     * Fare le cose così e non con un Binding o una IPC rende le cose valide solo all'interno dello
     * stesso contesto (= activity); però niente cast strani all'activity, niente cambiamenti... Etc..
     * Inoltre non vengono memorizzate variabili aggiuntive quando non servono rendendo il tutto più
     * efficiente.
     */
    private static final HashMap<Long, WeakReference<FragmentPipe<?> > > pipes = new HashMap<>();

    public interface IReceiver<R> {
        void deliver(R pack); // Consegna un pacchetto ["Ha un pacco enorme" cit. Leonida]
    }

    private IReceiver<T> receiver;
    private final long ID;

    public void setReceiver(IReceiver<T> receiver) {
        this.receiver = receiver;
    }

    private FragmentPipe(long ID) {
        this.ID = ID;
        WeakReference< FragmentPipe<?> > wr = pipes.get(ID);
        if(wr == null ||  wr.get() == null)
            pipes.put(ID, new WeakReference<FragmentPipe<?>>(this));
    }

    public FragmentPipe() {
        // Ok... qui la faccio semplice e uso il tempo... Potrebbe creare conflitti una volta ogni
        // morte di papa ma amen.
        this(System.currentTimeMillis());
    }

    public void deliver(T pack) {
        receiver.deliver(pack);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        pipes.remove(ID);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(ID);
    }

    public static final Creator<FragmentPipe> CREATOR = new Creator<FragmentPipe>() {
        @Override
        public FragmentPipe createFromParcel(Parcel in) {
            long id = in.readLong();
            WeakReference< FragmentPipe<?> > fp = pipes.get(id);
            if(fp == null || fp.get() == null)
                return new FragmentPipe(id);
            else
                return fp.get();
        }

        @Override
        public FragmentPipe[] newArray(int size) {
            return new FragmentPipe[size];
        }
    };

}
