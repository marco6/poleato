package it.polito.malnatidid.poleato.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;

import it.polito.malnatidid.poleato.util.FragmentPipe;
import it.polito.malnatidid.poleato.util.Triplet;

public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    private FragmentPipe<Triplet<Integer, Integer, Integer>> dayPipe;

    private int day, month, year;

    public static DatePickerFragment newInstance(FragmentPipe<Triplet<Integer, Integer, Integer>> pipe, Calendar calendar) {
        DatePickerFragment dpf = new DatePickerFragment();
        dpf.dayPipe = pipe;
        dpf.day = calendar.get(Calendar.DAY_OF_MONTH);
        dpf.month = calendar.get(Calendar.MONTH);
        dpf.year = calendar.get(Calendar.YEAR);
        return dpf;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null)
            dayPipe = savedInstanceState.getParcelable("dayPipe");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("dayPipe", dayPipe);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        dayPipe.deliver(new Triplet<>(year, month, day));
    }
}
