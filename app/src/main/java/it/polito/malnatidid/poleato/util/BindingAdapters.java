package it.polito.malnatidid.poleato.util;

import android.database.DataSetObserver;
import android.databinding.BindingAdapter;
import android.databinding.BindingConversion;
import android.databinding.Observable;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.TimePicker;

import java.lang.ref.WeakReference;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;
import java.util.Map;

import it.polito.malnatidid.poleato.R;
import it.polito.malnatidid.poleato.data.FBMenu;

public class BindingAdapters {

    @BindingAdapter("android:text")
    public static void printMap(TextView tv, Map<String, Integer> map) {

        String result = "";

        int i=1;
        if (map != null)
            for (Map.Entry<String, Integer> entry : map.entrySet()) {
                result += entry.getKey() + " x" + entry.getValue();
                if(i++ != map.entrySet().size())
                    result += "\n";
            }

        tv.setText(result);
    }

    @BindingAdapter("fill")
    public static void fillSpinner(Spinner sp, Collection<?> roba) {
        if (roba == null)
            return;
        ArrayAdapter<?> adapter = new ArrayAdapter<>(sp.getContext(), android.R.layout.simple_spinner_item, new ArrayList<>(roba));
        adapter.setDropDownViewResource(R.layout.spinner);
        sp.setAdapter(adapter);
    }

    private static class DishSpinnerAdapter extends Observable.OnPropertyChangedCallback implements SpinnerAdapter {

        private final ArrayList<DataSetObserver> updater = new ArrayList<>();

        private final FBMenu.MenuSection[] dishes;

        public DishSpinnerAdapter(FBMenu.MenuSection[] dishes) {
            this.dishes = dishes;
            for (FBMenu.MenuSection ms : dishes)
                ms.addOnPropertyChangedCallback(this);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {

            if (convertView == null)
                convertView = new TextView(parent.getContext());

            for (FBMenu.MenuSection section : dishes) {
                if (position < section.size()) {
                    ((TextView) convertView).setText(section.get(position).getName());
                    return convertView;
                } else
                    position -= section.size();
            }

            return convertView;
        }

        @Override
        public void registerDataSetObserver(DataSetObserver observer) {
            updater.add(observer);
        }

        @Override
        public void unregisterDataSetObserver(DataSetObserver observer) {
            updater.remove(observer);
        }

        @Override
        public int getCount() {
            int count = 0;

            for (FBMenu.MenuSection section : dishes)
                count += section.size();

            return count;
        }

        @Override
        public Object getItem(int position) {
            for (FBMenu.MenuSection section : dishes) {
                if (position < section.size()) {
                    return section.get(position);
                } else
                    position -= section.size();
            }

            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null)
                convertView = new TextView(parent.getContext());

            for (FBMenu.MenuSection section : dishes) {
                if (position < section.size()) {
                    ((TextView) convertView).setText(section.get(position).getName());
                    return convertView;
                } else
                    position -= section.size();
            }

            return convertView;
        }

        @Override
        public int getItemViewType(int position) {
            return 0;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public boolean isEmpty() {
            int count = 0;

            for (FBMenu.MenuSection section : dishes)
                count += section.size();

            return count == 0;
        }

        @Override
        public void onPropertyChanged(Observable sender, int propertyId) {
            for (DataSetObserver obs : updater)
                obs.onChanged();
        }
    }

    @BindingAdapter("fill")
    public static void fillSpinner(Spinner sp, FBMenu.MenuSection[] dishes) {
        if (dishes != null)
            sp.setAdapter(new DishSpinnerAdapter(dishes));
    }

    private static class OfferSpinnerAdapter extends Observable.OnPropertyChangedCallback implements SpinnerAdapter {

        private DataSetObserver updater;

        private FBMenu.OfferSection offers;

        public OfferSpinnerAdapter(FBMenu.OfferSection offers) {
            this.offers = offers;
            offers.addOnPropertyChangedCallback(this);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {

            if (convertView == null)
                convertView = new TextView(parent.getContext());

            ((TextView) convertView).setText(offers.get(position).getKey());

            return convertView;
        }

        @Override
        public void registerDataSetObserver(DataSetObserver observer) {
            updater = observer;
        }

        @Override
        public void unregisterDataSetObserver(DataSetObserver observer) {
            if (updater == observer)
                updater = null;
        }

        @Override
        public int getCount() {
            return offers.size();
        }

        @Override
        public Object getItem(int position) {
            return offers.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null)
                convertView = new TextView(parent.getContext());

            ((TextView) convertView).setText(offers.get(position).getKey());

            return convertView;
        }

        @Override
        public int getItemViewType(int position) {
            return 0;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public boolean isEmpty() {

            return offers.size() == 0;
        }

        @Override
        public void onPropertyChanged(Observable sender, int propertyId) {
            updater.onChanged();
        }
    }

    @BindingAdapter("fill")
    public static void fillSpinner(Spinner sp, FBMenu.OfferSection offers) {
        if (offers != null)
            sp.setAdapter(new OfferSpinnerAdapter(offers));
    }

    @BindingAdapter("fill")
    public static void fillSpinner(Spinner sp, ArrayList<?> roba) {
        if (roba != null) {
            ArrayAdapter<?> adapter = new ArrayAdapter<>(sp.getContext(), android.R.layout.simple_spinner_item, roba);
            adapter.setDropDownViewResource(R.layout.spinner);
            sp.setAdapter(adapter);
        }
    }

    private static final DateFormat formatter = new SimpleDateFormat("MMMM", Locale.getDefault());

    @BindingAdapter("monday")
    public static void setMonday(TextView cw, Timestamp time) {
        if (time == null)
            return;
        cw.setText(formatter.format(time));
    }

    private static DateFormat fmt;

    static {
        fmt = new SimpleDateFormat("EEE dd", Locale.getDefault());
    }

    @BindingAdapter("android:hour")
    public static void setHour(TimePicker tp, int hour) {
        if (Build.VERSION.SDK_INT >= 23)
            tp.setHour(hour);
        else
            tp.setCurrentHour(hour);
    }

    @BindingAdapter("android:minute")
    public static void setMinute(TimePicker tp, int minutes) {
        if (Build.VERSION.SDK_INT >= 23)
            tp.setMinute(minutes);
        else
            tp.setCurrentMinute(minutes);
    }

    @BindingAdapter({"android:text", "android:date_format"})
    public static void setDateAsText(TextView tv, Timestamp t, String format) {
        if (t != null) {
            SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
            tv.setText(sdf.format(t))
            ;
        }
    }

    @BindingAdapter({"android:text", "android:date_format"})
    public static void setDateAsText(TextView tv, long t, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
        tv.setText(sdf.format(new Timestamp(t)));
    }

    @BindingConversion
    public static String calendarToString(Timestamp t) {
        if (t == null)
            return "--";
        return fmt.format(t);
    }

    // Ora un setter di convenienza per gradire
    @BindingAdapter("android:src")
    public static void loadImage(ImageView iv, String id) {
        // Creo l'immagine
        final CachedBitmap cb = new CachedBitmap(id);

        // Setto subito quello che serve. Questo potrebbe poi essere usato per un placeholder...
        iv.setImageBitmap(cb.get());

        // Uso una weak reference per permettere se serve di liberare la memoria.
        final WeakReference<ImageView> wr = new WeakReference<ImageView>(iv);

        cb.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                // Se la mia view esiste ancora
                ImageView imageView = wr.get();
                if (imageView != null)
                    // Setto l'immagine
                    imageView.setImageBitmap(cb.get());
            }
        });
    }

    @BindingAdapter("android:background")
    public static void loadBackground(View iv, String id) {
        // Creo l'immagine
        final CachedBitmap cb = new CachedBitmap(id);

        // Setto subito quello che serve. Questo potrebbe poi essere usato per un placeholder...
        iv.setBackgroundDrawable(new BitmapDrawable(iv.getResources(), cb.get()));

        // Uso una weak reference per permettere se serve di liberare la memoria.
        final WeakReference<View> wr = new WeakReference<>(iv);

        cb.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                // Se la mia view esiste ancora
                View view = wr.get();
                if (view != null)
                    // Setto l'immagine
                    view.setBackgroundDrawable(new BitmapDrawable(view.getResources(), cb.get()));
            }
        });
    }
}
