package it.polito.malnatidid.poleato.fragments.restaurant;

import android.databinding.DataBindingUtil;
import android.databinding.Observable;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.client.Firebase;

import it.polito.malnatidid.poleato.R;
import it.polito.malnatidid.poleato.activities.HomeResActivity;
import it.polito.malnatidid.poleato.data.FBReservations;
import it.polito.malnatidid.poleato.databinding.FbHomeResFragmentBinding;
import it.polito.malnatidid.poleato.databinding.FbHomeResSeparatorBinding;
import it.polito.malnatidid.poleato.databinding.FbHomeResTableBinding;
import it.polito.malnatidid.poleato.databinding.FbHomeResTakeAwayBinding;
import it.polito.malnatidid.poleato.util.FirebaseCollectionBase;

public class FBHomeResFragment extends Fragment implements HomeResActivity.AdapterChanged {
    private long from, now, to;
    private int index;
    private final String UID = new Firebase("https://poleato.firebaseio.com").getAuth().getUid();

    private final RecyclerView.Adapter[] adapters = new RecyclerView.Adapter[2];
    private FbHomeResFragmentBinding binding;

    public void setAdapter(int index) {
        if (index != this.index && index >= 0 && index < 2) {
            this.index = index;
            if (binding != null) {
                binding.setAdapter(adapters[index]);
                binding.executePendingBindings();
            }
        }
    }

    @Override
    public void fire(int index) {
        setAdapter(index);
    }

    public class BindingsHolder<B extends ViewDataBinding> extends RecyclerView.ViewHolder {
        public final B bindings;

        public BindingsHolder(B bindings) {
            super(bindings.getRoot());
            this.bindings = bindings;
        }

        public BindingsHolder(View v) {
            super(v);
            bindings = null;
        }

    }

    private abstract class Adapter<B extends ViewDataBinding, D extends FirebaseCollectionBase> extends RecyclerView.Adapter<BindingsHolder<B>> {
        protected final D upper, lower;
        private final int itemLayout;

        private final Observable.OnPropertyChangedCallback updater = new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
//            if(sender instanceof FirebaseCollectionBase) {
//                if(propertyId == ((FirebaseCollectionBase) sender).size())
//                    takeAwayAdapter.notifyItemInserted(propertyId);
//                else if(propertyId >= 0)
//                    takeAwayAdapter.notifyItemChanged(propertyId);
//                else
//                    takeAwayAdapter.notifyItemRemoved(~propertyId);
//            }
                notifyDataSetChanged();
            }
        };

        public Adapter(int itemLayout, D upper, D lower) {
            super();
            this.itemLayout = itemLayout;
            this.upper = upper;
            this.lower = lower;
            upper.addOnPropertyChangedCallback(updater);
            lower.addOnPropertyChangedCallback(updater);
        }

        @Override
        public BindingsHolder<B> onCreateViewHolder(ViewGroup parent, int viewType) {
            if (viewType == 0) {
                B bind = DataBindingUtil.inflate(getActivity().getLayoutInflater(), itemLayout, parent, false);
                return new BindingsHolder<>(bind);
            } else {
                FbHomeResSeparatorBinding binding = DataBindingUtil.inflate(getActivity().getLayoutInflater(), R.layout.fb_home_res_separator, parent, false);
                if (viewType == 1) {
                    binding.setText("Pending");
                    binding.setColor(true);
                } else {
                    binding.setText("History");
                    binding.setColor(false);
                }
                return new BindingsHolder<>(binding.getRoot());
            }

        }

        @Override
        public int getItemViewType(int position) {
            if (position == upper.size() + 1)
                return 2;
            if (position == 0)
                return 1;
            return 0;
        }

        @Override
        public int getItemCount() {
            return upper.size() + lower.size() + 2;
        }
    }

    private class TableAdapter extends Adapter<FbHomeResTableBinding, FBReservations.TablesSection> {
        public TableAdapter() {
            super(R.layout.fb_home_res_table,
                    new FBReservations.TablesSection(UID, false, now, to),
                    new FBReservations.TablesSection(UID, false, from, now)
            );
        }

        @Override
        public void onBindViewHolder(BindingsHolder<FbHomeResTableBinding> holder, int position) {
            if (holder.bindings != null) {
                FBReservations.TablesSection actual;
                position--;
                if (position > upper.size()) {
                    position -= upper.size() + 1;
                    actual = lower;
                    holder.bindings.setColor(false);
                } else {
                    actual = upper;
                    holder.bindings.setColor(true);
                }
                holder.bindings.setRes(actual.get(position));
            }
        }
    }

    private class TakeAwayAdapter extends Adapter<FbHomeResTakeAwayBinding, FBReservations.TakeAwaySection> {
        public TakeAwayAdapter() {
            super(R.layout.fb_home_res_take_away,
                    new FBReservations.TakeAwaySection(UID, false, now, to),
                    new FBReservations.TakeAwaySection(UID, false, from, now)
            );
        }

        @Override
        public void onBindViewHolder(BindingsHolder<FbHomeResTakeAwayBinding> holder, int position) {
            if (holder.bindings != null) {
                FBReservations.TakeAwaySection actual;
                position--;
                if (position > upper.size()) {
                    position -= upper.size() + 1;
                    actual = lower;
                    holder.bindings.setColor(false);
                } else {
                    actual = upper;
                    holder.bindings.setColor(true);
                }

                holder.bindings.setRes(actual.get(position));
            }
        }
    }

    public static FBHomeResFragment newInstance(long from, long to, int index) {
        FBHomeResFragment fragment = new FBHomeResFragment();
        fragment.from = from;
        fragment.to = to;
        fragment.setAdapter(index);
        return fragment;
    }

//    @Override
//    public void setArguments(Bundle args) {
//        super.setArguments(args);
//        from = args.getLong("from");
//        to = args.getLong("to");
//        index = args.getInt("index", 0);
//    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong("from", from);
        outState.putLong("to", to);
        outState.putInt("index", index);
        Log.d("HomeResFragment", "SAVING... \nTO: " + to + "\nFROM: " + from + "\nNOW: " + now);

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            from = savedInstanceState.getLong("from");
            to = savedInstanceState.getLong("to");
            index = savedInstanceState.getInt("index");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FbHomeResFragmentBinding.inflate(inflater, container, false);
        now = System.currentTimeMillis();
        //Check now within bounds
        now = Math.max(Math.min(to, now), from);

        adapters[0] = new TakeAwayAdapter();
        adapters[1] = new TableAdapter();

        binding.setToday(from);
        binding.setAdapter(adapters[index]);
        binding.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        binding.setAnimator(new DefaultItemAnimator());

        return binding.getRoot();
    }
}
