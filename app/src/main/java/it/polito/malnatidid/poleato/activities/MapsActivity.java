package it.polito.malnatidid.poleato.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.databinding.DataBindingUtil;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.firebase.geofire.LocationCallback;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.VisibleRegion;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

import java.util.HashMap;

import it.polito.malnatidid.poleato.R;
import it.polito.malnatidid.poleato.data.FBProfileRestaurant;
import it.polito.malnatidid.poleato.data.FBRestaurantList;
import it.polito.malnatidid.poleato.databinding.ActivityMapsBinding;
import it.polito.malnatidid.poleato.fragments.customer.MapFilterFragment;
import it.polito.malnatidid.poleato.util.BooleanOperator;
import it.polito.malnatidid.poleato.util.FBFilter;
import it.polito.malnatidid.poleato.util.FragmentPipe;
import it.polito.malnatidid.poleato.util.GPSTracker;

public class MapsActivity
        extends FragmentActivity
        implements OnMapReadyCallback, LocationListener, LocationCallback, GoogleMap.OnCameraChangeListener,
        GoogleMap.OnMapClickListener, GeoQueryEventListener, ValueEventListener, FragmentPipe.IReceiver<BooleanOperator<FBRestaurantList.LightResProfile, DataSnapshot>>, ClusterManager.OnClusterClickListener<MapsActivity.MyItem>, ClusterManager.OnClusterItemClickListener<MapsActivity.MyItem> {

    private GoogleMap mMap;
    private String resID;
    private final Firebase node = new Firebase("https://poleato.firebaseio.com/users");
    private final GeoFire geoFire = new GeoFire(new Firebase("https://poleato.firebaseio.com/geofire"));
    private GeoQuery mapQuery;
    //private final HashMap<String, Marker> resIDMap = new HashMap<>();
    private final HashMap<String, MyItem> resIDMap = new HashMap<>();
    private ActivityMapsBinding binding;
    private boolean isResInfoVisibile = false;
    private FragmentPipe<BooleanOperator<FBRestaurantList.LightResProfile, DataSnapshot>> filterPipe;
    private FBFilter<String, FBRestaurantList.LightResProfile> filter = (FBFilter<String, FBRestaurantList.LightResProfile>) FBFilter.identity;
    private ClusterManager<MyItem> mClusterManager;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        binding.setLandscape(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            filter = savedInstanceState.getParcelable("filter");
            filterPipe = savedInstanceState.getParcelable("filterPipe");
            resID = savedInstanceState.getString("resid", getIntent().getStringExtra("resid"));
        } else {
            filterPipe = new FragmentPipe<>();
            resID = getIntent().getStringExtra("resid");
        }

        filterPipe.setReceiver(this);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_maps);

        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    GPSTracker.REQUEST_ACCESS_FINE_LOCATION);
        } else {
            ((SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map)).getMapAsync(this);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("filterPipe", filterPipe);
        outState.putParcelable("filter", filter);
        outState.putString("resid", resID);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        GPSTracker.inst.update();

        ((SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map)).getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMapClickListener(this);

        try {
            mMap.setMyLocationEnabled(true);
        } catch (SecurityException ignore) {
        }

        if (resID != null)
            geoFire.getLocation(resID, this);
        else {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(GPSTracker.inst.getLatitude(), GPSTracker.inst.getLongitude()), 15f));
            GPSTracker.inst.requestSingleUpdate(this);
            mMap.setOnCameraChangeListener(this);
        }

        // mMap.getUiSettings().setZoomControlsEnabled(true);

        setUpClusterer();
    }

    @Override
    public void onLocationChanged(Location location) {
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 15f));
    }

    @Override
    public void onLocationResult(String key, GeoLocation location) {
        if (location != null) {
            // Add a marker and move the camera
            LatLng resPosition = new LatLng(location.latitude, location.longitude);
            MyItem marker = new MyItem(new MarkerOptions()
                    .snippet(key)
                    .position(resPosition)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker)));
            mClusterManager.addItem(marker);
            resIDMap.put(key, marker);

            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.latitude, location.longitude), 15f));

            mClusterManager.cluster();
        }
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        FBRestaurantList.LightResProfile profile = dataSnapshot.getValue(FBRestaurantList.LightResProfile.class);

        if (!filter.eval(profile, dataSnapshot))
            return;

        LatLng resPosition = new LatLng(
                profile.getCoordinates().getLatitude(), profile.getCoordinates().getLongitude());

        MyItem marker = resIDMap.get(dataSnapshot.getKey());
        if (marker == null) {
            marker = new MyItem(new MarkerOptions()
                    .snippet(dataSnapshot.getKey())
                    .position(resPosition)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker)));

            mClusterManager.addItem(marker);

            resIDMap.put(marker.getSnippet(), marker);
        } else {
            marker.setPosition(resPosition);
        }
        mClusterManager.cluster();
    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        GPSTracker.inst.removeUpdates(this);

        float[] out = new float[1];

        VisibleRegion vr = mMap.getProjection().getVisibleRegion();

        Location.distanceBetween(vr.latLngBounds.northeast.latitude, cameraPosition.target.longitude,
                cameraPosition.target.latitude, cameraPosition.target.longitude, out);

        float kmDis = Math.min(out[0] / 1000, 1000.0f);

        if (mapQuery != null)
            mapQuery.removeAllListeners();
        mapQuery = geoFire.queryAtLocation(new GeoLocation(cameraPosition.target.latitude, cameraPosition.target.longitude), kmDis);
        mapQuery.addGeoQueryEventListener(this);
    }

    @Override
    public void onMapClick(LatLng latLng) {
        isResInfoVisibile = false;
        binding.setResInfoVisibility(false);
    }

    @Override
    public void onKeyEntered(String key, GeoLocation location) {
        // Listen for value
        node.child(key).addListenerForSingleValueEvent(this);
    }

    @Override
    public void onKeyExited(String key) {
        MyItem m = resIDMap.remove(key);

        if (m != null)
            mClusterManager.removeItem(m);
    }

    @Override
    public void onKeyMoved(String key, GeoLocation location) {
        MyItem m = resIDMap.get(key);

        if (m != null)
            m.setPosition(new LatLng(location.latitude, location.longitude));
    }

    @Override
    public void onBackPressed() {
        if (isResInfoVisibile)
            binding.setResInfoVisibility(isResInfoVisibile = false);
        else
            super.onBackPressed();
    }

    public void showFilters(View v) {
        getSupportFragmentManager()
                .beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(R.id.fraghost, MapFilterFragment.newInstance(filterPipe), "filter")
                .addToBackStack(null)
                .commit();
        if (isResInfoVisibile)
            binding.setResInfoVisibility(isResInfoVisibile = false);
    }

    @Override
    public void deliver(BooleanOperator<FBRestaurantList.LightResProfile, DataSnapshot> pack) {
        filter = new FBFilter<>(pack);
        resIDMap.clear();
        mClusterManager.clearItems();
        onCameraChange(mMap.getCameraPosition());
        //mClusterManager.onCameraChange(mMap.getCameraPosition());
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onCancelled(FirebaseError firebaseError) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onGeoQueryReady() {
    }

    @Override
    public void onGeoQueryError(FirebaseError error) {
    }

    @Override
    public boolean onClusterClick(Cluster<MyItem> cluster) {
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(cluster.getPosition(), mMap.getCameraPosition().zoom + 3f));
        return true;
    }

    @Override
    public boolean onClusterItemClick(final MyItem marker) {
        isResInfoVisibile = true;
        binding.setProfile(new FBProfileRestaurant(marker.getSnippet()));
        binding.setResInfoVisibility(isResInfoVisibile);
        binding.setResClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MapsActivity.this, CustRestaurantActivity.class);
                i.putExtra("resid", marker.getSnippet());
                startActivity(i);
            }
        });
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 18f));
        return true;
    }

    public class MyItem implements ClusterItem {
        private LatLng mPosition;
        private final String mSnippet;
        private final BitmapDescriptor mIcon;

        public MyItem(MarkerOptions markerOptions) {
            mPosition = markerOptions.getPosition();
            mSnippet = markerOptions.getSnippet();
            mIcon = markerOptions.getIcon();
        }

        @Override
        public LatLng getPosition() {
            return mPosition;
        }

        public BitmapDescriptor getIcon() {
            return mIcon;
        }

        public String getSnippet() {
            return mSnippet;
        }

        public void setPosition(LatLng position) {
            mPosition = position;
        }
    }

    class OwnIconRendered extends DefaultClusterRenderer<MyItem> {

        public OwnIconRendered(Context context, GoogleMap map,
                               ClusterManager<MyItem> clusterManager) {
            super(context, map, clusterManager);
        }

        @Override
        protected void onBeforeClusterItemRendered(MyItem item, MarkerOptions markerOptions) {
            markerOptions.icon(item.getIcon());
            markerOptions.snippet(item.getSnippet());
            super.onBeforeClusterItemRendered(item, markerOptions);
        }
    }

    private void setUpClusterer() {
        // Initialize the manager with the context and the map.
        // (Activity extends context, so we can pass 'this' in the constructor.)
        mClusterManager = new ClusterManager<MyItem>(this, mMap);
        // Custom Marker
        mClusterManager.setRenderer(new OwnIconRendered(getApplicationContext(), mMap, mClusterManager));

        mMap.setOnMarkerClickListener(mClusterManager);
        mClusterManager.setOnClusterClickListener(this);
        mClusterManager.setOnClusterItemClickListener(this);

    }
}