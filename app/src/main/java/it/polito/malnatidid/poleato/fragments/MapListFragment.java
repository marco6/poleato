package it.polito.malnatidid.poleato.fragments;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.LocationCallback;
import com.google.android.gms.maps.SupportMapFragment;

import it.polito.malnatidid.poleato.R;
import it.polito.malnatidid.poleato.activities.CustRestaurantActivity;
import it.polito.malnatidid.poleato.activities.MapsActivity;
import it.polito.malnatidid.poleato.data.FBProfileRestaurant;
import it.polito.malnatidid.poleato.data.FBRestaurantList;
import it.polito.malnatidid.poleato.databinding.LightResProfileItemBinding;
import it.polito.malnatidid.poleato.fragments.customer.MapFilterFragment;
import it.polito.malnatidid.poleato.util.BooleanOperator;
import it.polito.malnatidid.poleato.util.FBFilter;
import it.polito.malnatidid.poleato.util.FBRecycleViewFragment;
import it.polito.malnatidid.poleato.util.FragmentPipe;
import it.polito.malnatidid.poleato.util.GPSTracker;

public class MapListFragment
        extends FBRecycleViewFragment<FBRestaurantList, LightResProfileItemBinding>
        implements FragmentPipe.IReceiver<BooleanOperator<FBRestaurantList.LightResProfile, DataSnapshot>> {

    private FBFilter.StringCheck<FBRestaurantList.LightResProfile> stringCheck;
    private FragmentPipe<BooleanOperator<FBRestaurantList.LightResProfile, DataSnapshot>> filterPipe;
    private FBFilter<String, FBRestaurantList.LightResProfile> filter = (FBFilter<String, FBRestaurantList.LightResProfile>) FBFilter.identity;

    public static MapListFragment newInstance(String query) {
        MapListFragment fragment = new MapListFragment();
        fragment.stringCheck = new FBFilter.StringCheck<>("name", query == null ? "" : query);
        fragment.filter = new FBFilter<>(fragment.stringCheck);
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState==null && getActivity().getIntent().getBooleanExtra("home_filter", false)) {
            filterClick();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            filter = savedInstanceState.getParcelable("filter");
            filterPipe = savedInstanceState.getParcelable("filterPipe");
            stringCheck = savedInstanceState.getParcelable("query");
        } else {
            filterPipe = new FragmentPipe<>();
        }

        filterPipe.setReceiver(this);

        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            this.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    GPSTracker.REQUEST_ACCESS_FINE_LOCATION);
        } else
            setData(new FBRestaurantList(filter));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("filterPipe", filterPipe);
        outState.putParcelable("filter", filter);
        outState.putParcelable("query", stringCheck);
    }

    public MapListFragment() {
        super(R.layout.light_res_profile_item, R.drawable.ic_filter_white);

        setActionClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterClick();
            }
        });
    }

    public void filterClick() {
        getFragmentManager()
                .beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .add(R.id.fraghost, MapFilterFragment.newInstance(filterPipe), "filter")
                .addToBackStack(null)
                .commit();
    }

    @Override
    protected void bindItem(final BindingsHolder holder, FBRestaurantList data, int position) {

        final String resID = data.get(position).getKey();

        holder.bindings.setOnClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity().getApplicationContext(), CustRestaurantActivity.class);
                i.putExtra("resid", resID);
                getActivity().startActivity(i);
            }
        });

        holder.bindings.setOnMapClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity().getApplicationContext(), MapsActivity.class);
                i.putExtra("resid", resID);
                getActivity().startActivity(i);
            }
        });

        final Location myLocation = new Location("myLocation"), resLocation = new Location("resLocation");


            double latitude = GPSTracker.inst.getLatitude();
            double longitude = GPSTracker.inst.getLongitude();

            myLocation.setLatitude(latitude);
            myLocation.setLongitude(longitude);

            final GeoFire geoFire = new GeoFire(new Firebase("https://poleato.firebaseio.com/geofire"));

            geoFire.getLocation(resID, new LocationCallback() {
                @Override
                public void onLocationResult(String key, GeoLocation location) {
                    resLocation.setLatitude(location.latitude);
                    resLocation.setLongitude(location.longitude);

                    float distance = myLocation.distanceTo(resLocation);

                    if (distance > 1000)
                        holder.bindings.setDistance(String.format("%.1f", distance / 1000) + " km");
                    else
                        holder.bindings.setDistance(String.format("%.1f", distance) + " m");
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {
                    System.err.println("There was an error getting the GeoFire location: " + firebaseError);
                }
            });

        holder.bindings.setRestaurant(new FBProfileRestaurant(resID));
    }

    @Override
    public void deliver(BooleanOperator<FBRestaurantList.LightResProfile, DataSnapshot> pack) {
        setData(new FBRestaurantList(filter = new FBFilter<>(new FBFilter.AND<>(pack, stringCheck))));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        GPSTracker.inst.update();
        setData(new FBRestaurantList(filter));
    }
}
