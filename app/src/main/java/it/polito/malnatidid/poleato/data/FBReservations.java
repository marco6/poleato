package it.polito.malnatidid.poleato.data;

import java.util.Map;

import it.polito.malnatidid.poleato.util.FirebaseCollectionBase;
import it.polito.malnatidid.poleato.util.KeyedValue;

public class FBReservations {

    public final TakeAwaySection takeAways;
    public final TablesSection tables;

    public FBReservations(String ID, boolean customer, long start, long end){
        takeAways = new TakeAwaySection(ID, customer, start, end);
        tables = new TablesSection(ID, customer, start, end);
    }

    public static class TablesSection extends FirebaseCollectionBase<String, TableReservation> {

        public TablesSection(String ID, boolean customer, long start, long end){
            super(TableReservation.class, "https://poleato.firebaseio.com/reservations/tables",
                    customer ? "custIndex" : "resIndex", ID + "|" + start, ID + "|" + end);
        }

        @Override
        public String getKey(String keyString) {
            return keyString;
        }
    }

    public static class TakeAwaySection extends FirebaseCollectionBase<String, TakeAwayReservation>{

        public TakeAwaySection(String ID, boolean customer, long start, long end) {
            super(TakeAwayReservation.class, "https://poleato.firebaseio.com/reservations/takeAway",
                    customer ? "custIndex" : "resIndex", ID + "|" + start, ID + "|" + end);
        }

        @Override
        public String getKey(String keyString) {
            return keyString;
        }
    }

    public static class TableReservation implements  KeyedValue<String>{

        private String key, res_uid, cust_uid, res_name, cust_name;
        private long date;
        private int persons;

        public TableReservation() { }

        public String getCustIndex() {
            return cust_uid + "|" + date;
        }

        public String getResIndex() {
            return res_uid + "|" + date;
        }

        @Override
        public String getKey() {
            return key;
        }

        @Override
        public void setKey(String key) {
            this.key = key;
        }

        public String getRes_uid() {
            return res_uid;
        }

        public void setRes_uid(String res_uid) {
            this.res_uid = res_uid;
        }

        public String getCust_uid() {
            return cust_uid;
        }

        public void setCust_uid(String cust_uid) {
            this.cust_uid = cust_uid;
        }

        public String getRes_name() {
            return res_name;
        }

        public void setRes_name(String res_name) {
            this.res_name = res_name;
        }

        public String getCust_name() {
            return cust_name;
        }

        public void setCust_name(String cust_name) {
            this.cust_name = cust_name;
        }

        public long getDate() {
            return date;
        }

        public void setDate(long date) {
            this.date = date;
        }

        public int getPersons() {
            return persons;
        }

        public void setPersons(int persons) {
            this.persons = persons;
        }
    }

    public static class TakeAwayReservation implements KeyedValue<String>{

        private Map<String, Integer> dishes, promos;
        private String key, res_uid, res_name, cust_uid, cust_name;
        private long date;

        public TakeAwayReservation() { }

        public String getCustIndex() {
            return cust_uid + "|" + date;
        }

        public String getResIndex() {
            return res_uid + "|" + date;
        }

        @Override
        public String getKey() {
            return key;
        }

        @Override
        public void setKey(String key) {
            this.key = key;
        }

        public Map<String, Integer> getDishes() {
            return dishes;
        }

        public void setDishes(Map<String, Integer> dishes) {
            this.dishes = dishes;
        }

        public Map<String, Integer> getPromos() {
            return promos;
        }

        public void setPromos(Map<String, Integer> promos) {
            this.promos = promos;
        }

        public String getRes_uid() {
            return res_uid;
        }

        public void setRes_uid(String res_uid) {
            this.res_uid = res_uid;
        }

        public String getRes_name() {
            return res_name;
        }

        public void setRes_name(String res_name) {
            this.res_name = res_name;
        }

        public String getCust_uid() {
            return cust_uid;
        }

        public void setCust_uid(String cust_uid) {
            this.cust_uid = cust_uid;
        }

        public String getCust_name() {
            return cust_name;
        }

        public void setCust_name(String cust_name) {
            this.cust_name = cust_name;
        }

        public long getDate() {
            return date;
        }

        public void setDate(long date) {
            this.date = date;
        }
    }

}
