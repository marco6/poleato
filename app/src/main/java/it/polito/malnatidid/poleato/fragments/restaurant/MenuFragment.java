package it.polito.malnatidid.poleato.fragments.restaurant;

import android.databinding.DataBindingUtil;
import android.databinding.Observable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;

import com.firebase.client.Firebase;

import it.polito.malnatidid.poleato.R;
import it.polito.malnatidid.poleato.data.Dish;
import it.polito.malnatidid.poleato.data.FBMenu;
import it.polito.malnatidid.poleato.databinding.GenericExpandableGroupItemBinding;
import it.polito.malnatidid.poleato.databinding.GenericExpandableListViewBinding;
import it.polito.malnatidid.poleato.databinding.ProfileResMenuItemBinding;
import it.polito.malnatidid.poleato.util.TabFragment;

@TabFragment(R.string.tab_menu_title)
public class MenuFragment extends Fragment {

    private FBMenu menu;
    private String resID;
    private GenericExpandableListViewBinding binding;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("resID", resID);
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        if (args != null)
            menu = new FBMenu(resID = args.getString("resID"));
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState != null)
            menu = new FBMenu(resID = savedInstanceState.getString("resID"));

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        binding.setAdapter(new MenuExpandableListAdapter());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = GenericExpandableListViewBinding.inflate(inflater, container, false);

        binding.imageButton.setImageResource(R.drawable.ic_add_white);

        binding.setOnOk(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                getFragmentManager()
                        .beginTransaction()
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .replace(R.id.fraghost, new EditMenuFragment(), "cart_fragment")
                        .addToBackStack(null)
                        .commit();
            }
        });

        return binding.getRoot();
    }

    public class MenuExpandableListAdapter extends BaseExpandableListAdapter {

        private class OnPropertyChangedCallback extends Observable.OnPropertyChangedCallback {

            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                MenuExpandableListAdapter.this.notifyDataSetChanged();
            }
        }

        public MenuExpandableListAdapter() {
            super();
            OnPropertyChangedCallback p = new OnPropertyChangedCallback();
            for (int j = 0; j < menu.sections.length; j++)
                menu.sections[j].addOnPropertyChangedCallback(p);
        }

        @Override
        public int getGroupCount() {
            return menu.sections.length;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return menu.sections[groupPosition].size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return menu.sections[groupPosition];
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return menu.sections[groupPosition].get(childPosition);
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return ((long) groupPosition << 32) | childPosition;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            final GenericExpandableGroupItemBinding bind;

            if (menu.sections[groupPosition].size() == 0)
                return new View(getContext());
            else{
                if (convertView == null || convertView.getClass() == View.class)
                    bind = GenericExpandableGroupItemBinding.inflate(getActivity().getLayoutInflater(), parent, false);
                else
                    bind = DataBindingUtil.getBinding(convertView);
            }

            bind.setGroupName(menu.sections[groupPosition].section.value);

            if (bind.hasPendingBindings())
                bind.executePendingBindings();

            return bind.getRoot();
        }

        @Override
        public View getChildView(final int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

            final ProfileResMenuItemBinding bind;

            if (convertView == null) {

                bind = ProfileResMenuItemBinding.inflate(
                        getActivity().getLayoutInflater(), parent, false);

                bind.resInfoEditButton.setBackgroundResource(R.drawable.round_button_red);
                bind.resInfoEditButton.setImageResource(R.drawable.ic_delete);

                bind.setOnDeleteClick(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        delete(bind.getDish(), menu.sections[groupPosition].section.uriSection);
                    }
                });

                bind.setOnLongClick(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        edit(bind.getDish(), menu.sections[groupPosition].section);
                        return true;
                    }
                });
            } else
                bind = DataBindingUtil.getBinding(convertView);

            bind.setDish(menu.sections[groupPosition].get(childPosition));
            if (bind.hasPendingBindings())
                bind.executePendingBindings();
            return bind.getRoot();
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return false;
        }
    }

    private void edit(Dish dish, FBMenu.MenuSection.Section type) {

        EditMenuFragment editMenuFrag = EditMenuFragment.newInstance(dish, type);

        getFragmentManager()
                .beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(R.id.fraghost, editMenuFrag, "cart_fragment")
                .addToBackStack(null)
                .commit();
    }

    private void delete(Dish dish, String uriSection) {

        Firebase dishRef = new Firebase("https://poleato.firebaseio.com/menus/" + resID + "/dishes/" + uriSection + "/" + dish.getKey());
        dishRef.removeValue();
    }
}