package it.polito.malnatidid.poleato.util;

import android.os.Parcelable;

public interface BooleanOperator<K, V> extends Parcelable {
    boolean eval(K key, V value);
}
