package it.polito.malnatidid.poleato.util;

import com.firebase.client.FirebaseError;

interface OnFirebaseError {
    void manage(FirebaseError firebaseError);
}

