package it.polito.malnatidid.poleato;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.support.multidex.MultiDex;

import com.firebase.client.Firebase;

import it.polito.malnatidid.poleato.util.GPSTracker;

public class PolEatO extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Firebase.setAndroidContext(getApplicationContext());
        Firebase.getDefaultConfig().setPersistenceEnabled(true);
        Intent i = new Intent(this, NotificationService.class);
        startService(i);
        GPSTracker.init(this);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
