package it.polito.malnatidid.poleato.data;

import android.os.Parcel;
import android.os.Parcelable;

import it.polito.malnatidid.poleato.util.KeyedValue;

public class Dish implements KeyedValue<String>, Parcelable {

    private String photo, key;
    private float price;

    public Dish() { };

    protected Dish(Parcel in) {
        photo = in.readString();
        key = in.readString();
        price = in.readFloat();
    }

    public static final Creator<Dish> CREATOR = new Creator<Dish>() {
        @Override
        public Dish createFromParcel(Parcel in) {
            return new Dish(in);
        }

        @Override
        public Dish[] newArray(int size) {
            return new Dish[size];
        }
    };

    public String getName() {
        return key;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return key;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(photo);
        dest.writeString(key);
        dest.writeFloat(price);
    }
}
