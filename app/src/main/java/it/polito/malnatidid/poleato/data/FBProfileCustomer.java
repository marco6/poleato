package it.polito.malnatidid.poleato.data;

import android.databinding.Bindable;

import it.polito.malnatidid.poleato.BR;
import it.polito.malnatidid.poleato.util.FirebaseLiveObject;
import it.polito.malnatidid.poleato.util.FirebaseSetter;

public class FBProfileCustomer extends FirebaseLiveObject<FBProfileCustomer>{

    private String name, surname, address, phone, mail, photo;

    public FBProfileCustomer(String uid){
        super(FBProfileCustomer.class, "https://poleato.firebaseio.com/users/" + uid);
    }

    @Bindable
    public String getName() {
        return name;
    }

    @FirebaseSetter(BR.name)
    public void setName(String name) {
        this.name = name;
    }

    @Bindable
    public String getSurname() {
        return surname;
    }

    @FirebaseSetter(BR.surname)
    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Bindable
    public String getAddress() {
        return address;
    }

    @FirebaseSetter(BR.address)
    public void setAddress(String address) {
        this.address = address;
    }

    @Bindable
    public String getPhone() {
        return phone;
    }

    @FirebaseSetter(BR.phone)
    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Bindable
    public String getMail() {
        return mail;
    }

    @FirebaseSetter(BR.mail)
    public void setMail(String mail) {
        this.mail = mail;
    }

    @Bindable
    public String getPhoto() {
        return photo;
    }

    @FirebaseSetter(BR.photo)
    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getType() {
        return "c";
    }
}
