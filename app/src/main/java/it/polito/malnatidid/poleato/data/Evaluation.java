package it.polito.malnatidid.poleato.data;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Evaluation {
    private double clean, price, food;
    private int totalNumber;

    public Evaluation(double clean, double price, double food) {
        this.clean = clean;
        this.price = price;
        this.food = food;
    }

    public int getTotalNumber() {
        return totalNumber;
    }

    public void setTotalNumber(int totalNumber) {
        this.totalNumber = totalNumber;
    }

    @JsonIgnore
    public float getMean(){
        return (float)(clean+food)/2;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getClean() {
        return clean;
    }

    public void setClean(double clean) {
        this.clean = clean;
    }

    public double getFood() {
        return food;
    }

    public void setFood(double food) {
        this.food = food;
    }

    public Evaluation(){}
}
