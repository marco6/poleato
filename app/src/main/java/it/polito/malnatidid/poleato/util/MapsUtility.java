package it.polito.malnatidid.poleato.util;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MapsUtility {
    public static Triplet<Double, Double, String> getCoordinates(Context context, String addr) {

        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        List<Address> addresses = null;
        double latitude = 0;
        double longitude = 0;
        String loc = "";
        Address address = null;
        try {
            addresses = geocoder.getFromLocationName(addr, 1);
            address = addresses.get(0);
            loc = address.getLocality();
            if (addresses.size() > 0) {
                latitude = addresses.get(0).getLatitude();
                longitude = addresses.get(0).getLongitude();
            }
            return new Triplet<>(latitude, longitude, loc);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}