package it.polito.malnatidid.poleato.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.firebase.client.AuthData;
import com.firebase.client.Firebase;

import it.polito.malnatidid.poleato.R;
import it.polito.malnatidid.poleato.activities.CustRestaurantActivity;
import it.polito.malnatidid.poleato.data.FBReviews;
import it.polito.malnatidid.poleato.databinding.ProfileResReviewsItemBinding;
import it.polito.malnatidid.poleato.fragments.customer.AddReviewFragment;
import it.polito.malnatidid.poleato.util.FBRecycleViewFragment;
import it.polito.malnatidid.poleato.util.TabFragment;

@TabFragment(R.string.tab_review_title)
public class ReviewsFragment extends FBRecycleViewFragment<FBReviews,
        ProfileResReviewsItemBinding> {

    boolean res;

    public ReviewsFragment() {
        super(R.layout.profile_res_reviews_item, 0, true);
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        if (args != null)
            res = args.getBoolean("type_res");
    }

    @Override
    protected void bindItem(BindingsHolder holder, FBReviews data, int position) {
        holder.bindings.setReview(data.get(position));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //FIXME: fa schifo, ma siamo in consegna
        if(getActivity() instanceof CustRestaurantActivity)
            setActionClick(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getFragmentManager()
                            .beginTransaction()
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                            .add(R.id.fraghost, new AddReviewFragment(), "add_review")
                            .addToBackStack(null)
                            .commit();
                }
            });

        Firebase fb = new Firebase("https://poleato.firebaseio.com/");
        AuthData ad = fb.getAuth();
        if(ad != null && !res )
            setData(new FBReviews(ad.getUid(), "cust_uid"));
        if(ad != null && res )
            setData(new FBReviews(ad.getUid(), "res_uid"));
    }
}
