package it.polito.malnatidid.poleato.fragments;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.format.DateFormat;
import android.util.Pair;
import android.widget.TimePicker;

import java.util.Calendar;

import it.polito.malnatidid.poleato.util.FragmentPipe;

public class TimePickerFragment extends DialogFragment
        implements TimePickerDialog.OnTimeSetListener {
    public int hour;
    public int minute;
    private FragmentPipe<Pair<Integer, Integer>> timePipe;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("timePipe", timePipe);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null)
            timePipe = savedInstanceState.getParcelable("timePipe");
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new TimePickerDialog(getActivity(), this, hour, minute,
                DateFormat.is24HourFormat(getActivity()));
    }

    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        timePipe.deliver(new Pair<>(hourOfDay, minute));
    }

    public static TimePickerFragment newInstance(FragmentPipe<Pair<Integer, Integer>> timePipe, Calendar calendar) {
        TimePickerFragment tpf = new TimePickerFragment();
        tpf.timePipe = timePipe;
        tpf.hour = calendar.get(Calendar.HOUR_OF_DAY);
        tpf.minute = calendar.get(Calendar.MINUTE);
        return tpf;
    }
}
