package it.polito.malnatidid.poleato.fragments.customer;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Toast;

import it.polito.malnatidid.poleato.R;
import it.polito.malnatidid.poleato.data.FBMenu;
import it.polito.malnatidid.poleato.data.SpecialOffer;
import it.polito.malnatidid.poleato.databinding.ProfileCustPromoItemBinding;
import it.polito.malnatidid.poleato.util.FBRecycleViewFragment;
import it.polito.malnatidid.poleato.util.FragmentPipe;
import it.polito.malnatidid.poleato.util.QuantityCostParser;
import it.polito.malnatidid.poleato.util.TabFragment;

@TabFragment(R.string.tab_promo_title)
public class PromoFragment extends FBRecycleViewFragment<FBMenu.OfferSection, ProfileCustPromoItemBinding> implements FragmentPipe.IReceiver<Void> {

    private String resID;
    private FragmentPipe<Void> updatePipe;

    public PromoFragment() {
        super(R.layout.profile_cust_promo_item, R.drawable.ic_carrello_white);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("resID", resID);
        outState.putParcelable("updatePipe", updatePipe);
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        if (args != null) {
            setData(new FBMenu.OfferSection(resID = args.getString("resID")));
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState != null){
            resID = savedInstanceState.getString("resID");
            updatePipe = savedInstanceState.getParcelable("updatePipe");
        }else
            updatePipe = new FragmentPipe<>();

        updatePipe.setReceiver(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle args = new Bundle();
        args.putString("resID", resID);
        args.putParcelable("updatePipe", updatePipe);
        final CartFragment frag = new CartFragment();
        frag.setArguments(args);

        setActionClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getFragmentManager()
                        .beginTransaction()
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .replace(R.id.fraghost, frag, "cart_fragment")
                        .addToBackStack(null)
                        .commit();
            }
        });

        if (savedInstanceState != null)
            setData(new FBMenu.OfferSection(resID = savedInstanceState.getString("resID")));
    }

    @Override
    protected void bindItem(BindingsHolder holder, FBMenu.OfferSection data, int position) {

        final SpecialOffer newPromo = data.get(position);
        final SharedPreferences cartPromosPrefs = getActivity().getSharedPreferences("promos:" + resID, Activity.MODE_PRIVATE),
                resPrefs = getActivity().getSharedPreferences(resID, Activity.MODE_PRIVATE);

        final BindingsHolder bindingsHolder = holder;

        holder.bindings.setPromo(newPromo);
        QuantityCostParser p = new QuantityCostParser(cartPromosPrefs.getString(newPromo.getKey(), null));
        holder.bindings.setQuantity(p.quantity);

        holder.bindings.setOnAddClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                QuantityCostParser p = new QuantityCostParser(cartPromosPrefs.getString(newPromo.getKey(), null));

                p.quantity += 1;
                p.cost += newPromo.getPrice();

                cartPromosPrefs.edit().putString(newPromo.getKey(), p.parse()).apply();

                Float cartTotalCost = resPrefs.getFloat("cartTotalCost", 0.0f);
                cartTotalCost += newPromo.getPrice();
                resPrefs.edit().putFloat("cartTotalCost", cartTotalCost).apply();

                bindingsHolder.bindings.setQuantity(p.quantity);

                Toast.makeText(getActivity().getApplicationContext(), newPromo.getKey() + " " + getResources().getString(R.string.add_reservation_menu_item), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void deliver(Void pack) {
        this.adapter.notifyDataSetChanged();
    }
}
