package it.polito.malnatidid.poleato.data;

import it.polito.malnatidid.poleato.R;
import it.polito.malnatidid.poleato.util.FirebaseCollectionBase;

public class FBMenu {
    public static class MenuSection extends FirebaseCollectionBase<String, Dish> {

        public enum Section {
            APPETIZERS(R.string.appetizers, "Antipasti"),
            BEVERAGES(R.string.beverages, "Bibite"),
            DESSERTS(R.string.desserts, "Dolci"),
            SECOND_COURSES(R.string.second_courses, "Secondi"),
            FIRST_COURSES(R.string.first_courses, "Primi"),
            SIDE_DISHES(R.string.side_dishes, "Contorni");

            public final int value;
            public final String uriSection;

            Section(int value, String uriSection) {
                this.value = value;
                this.uriSection = uriSection;
            }

        }

        public final Section section;

        public MenuSection(String resID, Section section) {
            super(Dish.class, "https://poleato.firebaseio.com/menus/" + resID + "/dishes/" + section.uriSection);
            this.section = section;
        }

        @Override
        public String getKey(String keyString) {
            return keyString;
        }
    }

    public static class OfferSection extends FirebaseCollectionBase<String, SpecialOffer>{

        public OfferSection(String resID) {
            super(SpecialOffer.class, "https://poleato.firebaseio.com/menus/" + resID + "/special_offers");
        }

        @Override
        public String getKey(String keyString) {
            return keyString;
        }
    }

    public final MenuSection[] sections;

    public final OfferSection offers;

    public FBMenu(String resID) {
        sections = new MenuSection[] {
                new MenuSection(resID, MenuSection.Section.APPETIZERS),
                new MenuSection(resID, MenuSection.Section.FIRST_COURSES),
                new MenuSection(resID, MenuSection.Section.SECOND_COURSES),
                new MenuSection(resID, MenuSection.Section.SIDE_DISHES),
                new MenuSection(resID, MenuSection.Section.DESSERTS),
                new MenuSection(resID, MenuSection.Section.BEVERAGES)
        };

        offers = new OfferSection(resID);
    }
}
