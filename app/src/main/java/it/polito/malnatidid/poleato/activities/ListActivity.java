package it.polito.malnatidid.poleato.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import it.polito.malnatidid.poleato.R;
import it.polito.malnatidid.poleato.fragments.MapListFragment;
import it.polito.malnatidid.poleato.util.Toolbar;

public class ListActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_container);

        Toolbar.onCreateToolbar(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        if (savedInstanceState == null)
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fraghost, MapListFragment.newInstance(getIntent().getStringExtra("query")), "MapList")
                    .commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}
