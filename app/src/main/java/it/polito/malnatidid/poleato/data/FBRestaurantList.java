package it.polito.malnatidid.poleato.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import it.polito.malnatidid.poleato.util.FBFilter;
import it.polito.malnatidid.poleato.util.FirebaseCollectionBase;
import it.polito.malnatidid.poleato.util.KeyedValue;

public class FBRestaurantList extends FirebaseCollectionBase<String, FBRestaurantList.LightResProfile> {

    public FBRestaurantList() {
        super(LightResProfile.class, "https://poleato.firebaseio.com/users/", "type", "r");
    }

    public FBRestaurantList(FBFilter<String, LightResProfile> filter) {
        super(LightResProfile.class, "https://poleato.firebaseio.com/users/", "type", "r", filter);
    }

    @Override
    public String getKey(String keyString) {
        return keyString;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class LightResProfile implements KeyedValue<String>{

        String address, name, key;
        boolean asian, celiac, delivery, fastFood, greek, indian, kebab, mexican,
                other, pizza, selfService, served, takeAway, vegetarian;

        Evaluation evaluation;

        Coordinates coordinates;

        public LightResProfile() {}

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public Coordinates getCoordinates() {
            return coordinates;
        }

        public void setCoordinates(Coordinates coordinates) {
            this.coordinates = coordinates;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public boolean isAsian() {
            return asian;
        }

        public void setAsian(boolean asian) {
            this.asian = asian;
        }

        public boolean isCeliac() {
            return celiac;
        }

        public void setCeliac(boolean celiac) {
            this.celiac = celiac;
        }

        public boolean isDelivery() {
            return delivery;
        }

        public void setDelivery(boolean delivery) {
            this.delivery = delivery;
        }

        public boolean isFastFood() {
            return fastFood;
        }

        public void setFastFood(boolean fastFood) {
            this.fastFood = fastFood;
        }

        public boolean isGreek() {
            return greek;
        }

        public void setGreek(boolean greek) {
            this.greek = greek;
        }

        public boolean isIndian() {
            return indian;
        }

        public void setIndian(boolean indian) {
            this.indian = indian;
        }

        public boolean isKebab() {
            return kebab;
        }

        public void setKebab(boolean kebab) {
            this.kebab = kebab;
        }

        public boolean isMexican() {
            return mexican;
        }

        public void setMexican(boolean mexican) {
            this.mexican = mexican;
        }

        public boolean isOther() {
            return other;
        }

        public void setOther(boolean other) {
            this.other = other;
        }

        public boolean isPizza() {
            return pizza;
        }

        public void setPizza(boolean pizza) {
            this.pizza = pizza;
        }

        public boolean isSelfService() {
            return selfService;
        }

        public void setSelfService(boolean selfService) {
            this.selfService = selfService;
        }

        public boolean isServed() {
            return served;
        }

        public void setServed(boolean served) {
            this.served = served;
        }

        public boolean isTakeAway() {
            return takeAway;
        }

        public void setTakeAway(boolean takeAway) {
            this.takeAway = takeAway;
        }

        public boolean isVegetarian() {
            return vegetarian;
        }

        public void setVegetarian(boolean vegetarian) {
            this.vegetarian = vegetarian;
        }

        public Evaluation getEvaluation() {
            return evaluation;
        }

        public void setEvaluation(Evaluation evaluation) {
            this.evaluation = evaluation;
        }

        @Override
        public String getKey() {
            return key;
        }

        @Override
        public void setKey(String key) {
            this.key = key;
        }
    }

}
