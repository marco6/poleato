package it.polito.malnatidid.poleato.fragments.customer;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.client.AuthData;
import com.firebase.client.Firebase;

import it.polito.malnatidid.poleato.R;
import it.polito.malnatidid.poleato.data.FBProfileCustomer;
import it.polito.malnatidid.poleato.databinding.ProfileCustInfoBinding;
import it.polito.malnatidid.poleato.util.FBImageCache;
import it.polito.malnatidid.poleato.util.TabFragment;

@TabFragment(R.string.tab_info_title)
public class CustInfoFragment extends Fragment {
    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int GALLERY_PICTURE = 2;

    private ProfileCustInfoBinding bind;
    private AlertDialog chooser;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        chooser = new AlertDialog.Builder(getContext())
                .setTitle(R.string.select_picture)
                .setMessage(R.string.mode_picture_select)
                .setPositiveButton(R.string.gallery, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        Intent pickPhoto = new Intent(Intent.ACTION_GET_CONTENT);
                        pickPhoto.setType("image/*");
                        startActivityForResult(pickPhoto, GALLERY_PICTURE);
                    }
                })
                .setNegativeButton(R.string.camera, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null)
                            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    }
                }).create();

        bind.setChooseImageClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooser.show();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        bind = ProfileCustInfoBinding.inflate(inflater, container, false);

        bind.setEditClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editProfile();
            }
        });

        Firebase node = new Firebase("https://poleato.firebaseio.com/");
        AuthData auth = node.getAuth();

        if (auth != null)
            bind.setProfile(new FBProfileCustomer(auth.getUid()));

        return bind.getRoot();
    }

    private void editProfile() {
        getActivity()
                .getSupportFragmentManager()
                .beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .add(R.id.fraghost, new EditCustProfileFragment(), "profile_cust_edit_info")
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {

            if (resultCode == Activity.RESULT_OK) {
                Firebase updater = new Firebase("https://poleato.firebaseio.com/users/");
                updater = updater.child(updater.getAuth().getUid() + "/photo");
                switch (requestCode) {
                    case REQUEST_IMAGE_CAPTURE:
                        updater.setValue(
                                FBImageCache.getInstance().upload(
                                        (Bitmap) data.getExtras().get("data")
                                ));
                        break;
                    case GALLERY_PICTURE:
                        updater.setValue(
                                FBImageCache.getInstance().upload(
                                        getContext(), data.getData()
                                ));
                        break;
                }
            }
        }
    }
}