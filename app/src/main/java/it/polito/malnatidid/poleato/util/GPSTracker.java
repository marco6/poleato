package it.polito.malnatidid.poleato.util;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;

public class GPSTracker implements LocationListener {

    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10L;
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60L;
    public static final int REQUEST_ACCESS_FINE_LOCATION = 2;
    public static final double DEFAULT_LATITUDE = 45.062553, DEFAULT_LONGITUDE = 7.662398;
    private static final Criteria criteria;

    static {
        criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        criteria.setCostAllowed(true);
    }

    //SINGLETON
    public static final GPSTracker inst = new GPSTracker();
    private GPSTracker() { }

    private LocationManager locationManager;
    private Location location; // location
    private Looper looper;

    public static void init(Context c) {
        inst.locationManager = (LocationManager) c.getSystemService(Context.LOCATION_SERVICE);
        inst.looper = c.getMainLooper();
        inst.update();
    }

    public void removeUpdates(LocationListener listener) {
        try {
            locationManager.removeUpdates(listener);
        } catch (SecurityException ignore) { }
    }

    public void requestSingleUpdate(LocationListener listener) {
        try {
            locationManager.requestSingleUpdate(criteria, listener, looper);
        } catch (SecurityException ignore) {
            Location l = new Location((String) null);
            l.setLatitude(DEFAULT_LATITUDE);
            l.setLongitude(DEFAULT_LONGITUDE);
            listener.onLocationChanged(l);
        }
    }

    public void update() {
        try {
            locationManager.removeUpdates(this);
            locationManager.requestLocationUpdates(MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, criteria, this, looper);
            for (String provider : locationManager.getProviders(criteria, true)) {
                Location l = locationManager.getLastKnownLocation(provider);
                if (l != null) {
                    location = l;
                    return;
                }
            }
        } catch (SecurityException ignored) {
        }
    }

    public Location getLocation() {
        return location;
    }

    public double getLatitude() {
        if (location != null)
            return location.getLatitude();
        return DEFAULT_LATITUDE;
    }

    public double getLongitude() {
        if (location != null)
            return location.getLongitude();
        return DEFAULT_LONGITUDE;
    }

    @Override
    public void onLocationChanged(Location location) {
        this.location = location;
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }
}