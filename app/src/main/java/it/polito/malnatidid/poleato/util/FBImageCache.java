package it.polito.malnatidid.poleato.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Base64;
import android.util.Log;
import android.util.LruCache;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedList;

public class FBImageCache extends LruCache<String, Bitmap> implements ValueEventListener {

    public static final int CacheSize = 5 * 1024 * 1024; // 10 MB ???

    private Firebase photo = new Firebase("https://poleato.firebaseio.com/photo/");

    private HashMap<String, LinkedList<BitmapLoaded>> callbacks = new HashMap<>();

    public interface BitmapLoaded {
        void loaded(String key, Bitmap value);
    }

    private void dispatchEvents(String key, Bitmap value) {
        LinkedList<BitmapLoaded> cb = null;
        synchronized (this) {
            cb = callbacks.remove(key);
        }
        if (cb != null)
            for (BitmapLoaded bl : cb)
                bl.loaded(key, value);
    }

    private FBImageCache() {
        super(CacheSize);
    }

    public Bitmap get(String key, BitmapLoaded callback) {
        if (key == null || "".equals(key))
            return null;
        synchronized (this) {
            LinkedList<BitmapLoaded> cb;
            if (!callbacks.containsKey(key)) {
                cb = new LinkedList<>();
                callbacks.put(key, cb);
            } else
                cb = callbacks.get(key);
            cb.add(callback);
        }
        Bitmap bmp = get(key);
        if (bmp != null)
            dispatchEvents(key, bmp);
        return bmp;
    }

    @Override
    protected int sizeOf(String key, Bitmap value) {
        return value.getWidth() * value.getHeight() * 4;
    }

    @Override
    protected Bitmap create(String key) {
        Firebase obj = photo.child(key);
        obj.addListenerForSingleValueEvent(this);
        return null;
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        String b64string = (String) dataSnapshot.getValue();
        if (b64string != null) {
            byte[] decode = Base64.decode(b64string, Base64.DEFAULT);
            String key = dataSnapshot.getKey();
            Bitmap value = BitmapFactory.decodeByteArray(decode, 0, decode.length);
            put(key, value);
            dispatchEvents(key, value);
        }
    }

    private static void calculateInSampleSize(
            BitmapFactory.Options options, int maxWidth, int maxHeight) {
        int height = options.outHeight;
        int width = options.outWidth;
        options.inSampleSize = 1;
        while (height > maxHeight || width > maxWidth) {
            options.inSampleSize <<= 1;
            height >>= 1;
            width >>= 1;
        }
    }

    public String upload(Bitmap loaded) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        loaded.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);

        Firebase ref = photo.push();
        ref.setValue(Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT));
        try {
            byteArrayOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        put(ref.getKey(), loaded);
        return ref.getKey();
    }

    public String upload(Context c, Uri uri) {
        try {
            InputStream is = c.getContentResolver().openInputStream(uri);
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(is, null, options);
            is.close();

            calculateInSampleSize(options, 512, 512);// Questa occupa al massimo 1 MB...

            options.inJustDecodeBounds = false;

            is = c.getContentResolver().openInputStream(uri);
            Bitmap loaded = BitmapFactory.decodeStream(is, null, options);
            is.close();


            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            loaded.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);

            Firebase ref = photo.push();
            ref.setValue(Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT));
            byteArrayOutputStream.close();
            put(ref.getKey(), loaded);
            return ref.getKey();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onCancelled(FirebaseError firebaseError) { /*Ignoro gli errori.*/
        Log.d("poleatod", "onCancelled: " + firebaseError.getMessage());
    }

    // Singleton (lazy)
    private static class Builder {
        public static FBImageCache INSTANCE = new FBImageCache();
    }

    public static FBImageCache getInstance() {
        return Builder.INSTANCE;
    }
}
