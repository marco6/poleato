package it.polito.malnatidid.poleato.util;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

import com.firebase.client.DataSnapshot;

import java.nio.charset.Charset;
import java.util.ArrayList;

import it.polito.malnatidid.poleato.data.Coordinates;
import it.polito.malnatidid.poleato.data.FBRestaurantList;

public class FBFilter<K, V extends KeyedValue<K> > implements Parcelable {

    private static class TRUE<_K, _V> implements BooleanOperator<_K, _V> {
        @Override
        public boolean eval(_K key, _V value) {
            return true;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {}

        public static final Creator<TRUE> CREATOR = new Creator<TRUE>() {
            @Override
            public TRUE createFromParcel(Parcel source) {
                return new TRUE();
            }

            @Override
            public TRUE[] newArray(int size) {
                return new TRUE[size];
            }
        };
    }

    public static class AND<_K, _V> implements BooleanOperator<_K, _V> {

        private final BooleanOperator<_K, _V>[] args;

        @SafeVarargs
        public AND(BooleanOperator<_K, _V>... args) {
            this.args = args;
        }

        public AND(Parcel p) {
            int size = p.readInt();
            args = new BooleanOperator[size];
            for(int i = 0; i < size; i++)
                args[i] = p.readParcelable(getClass().getClassLoader());
        }

        @Override
        public boolean eval(_K key, _V value) {
            for(BooleanOperator<_K, _V> op : args)
                if(!op.eval(key, value))
                    return false;
            return true;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(args.length);
            for(BooleanOperator<_K, _V> bo : args)
                dest.writeParcelable(bo, flags);
        }

        public static final Creator<AND> CREATOR = new Creator<AND>() {
            @Override
            public AND createFromParcel(Parcel source) {
                return new AND(source);
            }

            @Override
            public AND[] newArray(int size) {
                return new AND[size];
            }
        };
    }

    public static class OR<_K, _V> implements BooleanOperator<_K, _V> {

        private final BooleanOperator<_K, _V>[] args;

        private OR(Parcel p) {
            int size = p.readInt();
            args = new BooleanOperator[size];
            for(int i = 0; i < size; i++)
                args[i] = p.readParcelable(getClass().getClassLoader());
        }

        public OR(ArrayList<BooleanOperator<_K, _V> > args) {
            BooleanOperator<_K, _V>[] pp = new BooleanOperator[0];
            this.args = args.toArray(pp);
        }

        @SafeVarargs
        public OR(BooleanOperator<_K, _V>... args) {
            this.args = args;
        }

        @Override
        public boolean eval(_K key, _V value) {
            for(BooleanOperator<_K, _V> op : args)
                if(op.eval(key, value))
                    return true;
            return args.length == 0;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(args.length);
            for(BooleanOperator<_K, _V> bo : args)
                dest.writeParcelable(bo, flags);
        }

        public static final Creator<OR> CREATOR = new Creator<OR>() {
            @Override
            public OR createFromParcel(Parcel source) {
                return new OR(source);
            }

            @Override
            public OR[] newArray(int size) {
                return new OR[size];
            }
        };
    }

    public static class StringCheck<_V extends KeyedValue<String>> implements BooleanOperator<_V, DataSnapshot> {

        private final String field;
        private final String[] words;

        public StringCheck(Parcel in) {
            field = in.readString();
            words = in.createStringArray();
        }

        public StringCheck(String field, String query) {
            this.field = field;
            this.words = query.split(" ");
        }

        private static final Charset ascii = Charset.forName("US-ASCII");

        private static boolean fuzzy_match(final String _t, final String _p) {
            byte[] pattern = _p.toLowerCase().getBytes(ascii), text = _t.toLowerCase().getBytes(ascii);
            final int hDist = pattern.length >> 2;
            int m = pattern.length, i, d;
            long[] R = new long[hDist+1], patternMask = new long[128];

            for(i = 0; i <= hDist; i++)
                R[i] = -2L;

            for(i = 0; i < 128; i++)
                patternMask[i] = -1L;

            for(i = 0; i < pattern.length; i++)
                patternMask[pattern[i]] &= ~(1L << i);

            for(i = 0; i < text.length; i++) {
                long old_Rd1 = R[0];

                R[0] |= patternMask[text[i]];
                R[0] <<= 1;

                for (d=1; d <= hDist; ++d) {
                    long tmp = R[d];
                    R[d] = (old_Rd1 & (R[d] | patternMask[text[i]])) << 1;
                    old_Rd1 = tmp;
                }

                if (0 == (R[hDist] & (1L << m)))
                    return true;
            }
            return false;
        }

        @Override
        public boolean eval(_V key, DataSnapshot value) {
            String text = value.child(field).getValue(String.class);
            for(String w : words)
                if(fuzzy_match(text, w))
                    return true;
            return false;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(field);
            dest.writeStringArray(words);
        }

        public static final Creator<StringCheck> CREATOR = new Creator<StringCheck>() {
            @Override
            public StringCheck createFromParcel(Parcel source) {
                return new StringCheck(source);
            }

            @Override
            public StringCheck[] newArray(int size) {
                return new StringCheck[size];
            }
        };
    }

    public static class FieldCheck<_K, _V extends KeyedValue<_K> > implements BooleanOperator<_V, DataSnapshot> {

        private final String field;

        public FieldCheck(Parcel in) {
            field = in.readString();
        }

        public FieldCheck(String field) {
            this.field = field;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(field);
        }

        @Override
        public boolean eval(_V key, DataSnapshot value) {
            return value.child(field).getValue(Boolean.class);
        }

        public static final Creator<FieldCheck> CREATOR = new Creator<FieldCheck>() {
            @Override
            public FieldCheck createFromParcel(Parcel source) {
                return new FieldCheck(source);
            }

            @Override
            public FieldCheck[] newArray(int size) {
                return new FieldCheck[size];
            }
        };
    }

    public static class DoubleCheck<_K, _V extends KeyedValue<_K> > implements BooleanOperator<_V, DataSnapshot> {

        private final String field;
        private final double value;

        public DoubleCheck(Parcel in) {
            field = in.readString();
            value = in.readDouble();
        }

        public DoubleCheck(String field, double value) {
            this.field = field;
            this.value = value;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(field);
            dest.writeDouble(value);
        }

        @Override
        public boolean eval(_V key, DataSnapshot value) {
            return value.child(field).getValue(Double.class) <= this.value;
        }

        public static final Creator<DoubleCheck> CREATOR = new Creator<DoubleCheck>() {
            @Override
            public DoubleCheck createFromParcel(Parcel source) {
                return new DoubleCheck(source);
            }

            @Override
            public DoubleCheck[] newArray(int size) {
                return new DoubleCheck[size];
            }
        };
    }

    public final static FBFilter<?, ?> identity = new FBFilter(new TRUE());

    private BooleanOperator<V, DataSnapshot> op;

    public FBFilter(BooleanOperator<V, DataSnapshot> expression) {
        op = expression;
    }

    public boolean eval(V value, DataSnapshot ds) {
        return op.eval(value, ds);
    }

    protected FBFilter(Parcel in) {
        op = in.readParcelable(getClass().getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(op, flags);
    }

    public static final Creator<FBFilter> CREATOR = new Creator<FBFilter>() {
        @Override
        public FBFilter createFromParcel(Parcel in) {
            return new FBFilter(in);
        }

        @Override
        public FBFilter[] newArray(int size) {
            return new FBFilter[size];
        }
    };

    public static class DistanceCheck implements BooleanOperator<FBRestaurantList.LightResProfile, DataSnapshot> {

        private final double maxDistance;
        private final Coordinates myLocation;

        public DistanceCheck(Parcel source) {
            maxDistance = source.readDouble();
            myLocation = source.readParcelable(this.getClass().getClassLoader());
        }

        public DistanceCheck(double v, Coordinates myLocation) {
            this.maxDistance = v;
            this.myLocation = myLocation;
        }

        @Override
        public boolean eval(FBRestaurantList.LightResProfile profile, DataSnapshot value) {

            float[] res = new float[1];

            Location.distanceBetween(this.myLocation.getLatitude(),this.myLocation.getLongitude(),
                    profile.getCoordinates().getLatitude(),
                    profile.getCoordinates().getLongitude(), res );

            return (res[0] <= this.maxDistance);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeDouble(maxDistance);
            dest.writeParcelable(myLocation, flags);
        }

        public static final Creator<DistanceCheck> CREATOR = new Creator<DistanceCheck>() {
            @Override
            public DistanceCheck createFromParcel(Parcel source) {
                return new DistanceCheck(source);
            }

            @Override
            public DistanceCheck[] newArray(int size) {
                return new DistanceCheck[size];
            }
        };
    }
}
