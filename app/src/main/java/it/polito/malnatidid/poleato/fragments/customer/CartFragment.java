package it.polito.malnatidid.poleato.fragments.customer;

import android.app.Activity;
import android.content.SharedPreferences;
import android.databinding.Observable;
import android.databinding.ObservableFloat;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.util.ArrayMap;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.firebase.client.AuthData;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Map;

import it.polito.malnatidid.poleato.NotificationService;
import it.polito.malnatidid.poleato.R;
import it.polito.malnatidid.poleato.data.FBReservations;
import it.polito.malnatidid.poleato.data.Timetable;
import it.polito.malnatidid.poleato.databinding.CustCartBinding;
import it.polito.malnatidid.poleato.databinding.CustCartItemBinding;
import it.polito.malnatidid.poleato.fragments.DatePickerFragment;
import it.polito.malnatidid.poleato.fragments.LoadingDialogFragment;
import it.polito.malnatidid.poleato.fragments.LoginFragment;
import it.polito.malnatidid.poleato.fragments.TimePickerFragment;
import it.polito.malnatidid.poleato.util.FragmentPipe;
import it.polito.malnatidid.poleato.util.QuantityCostParser;
import it.polito.malnatidid.poleato.util.Triplet;

public class CartFragment extends Fragment {

    private String resID;
    private ObservableFloat totalCost;

    final Firebase fb = new Firebase("https://poleato.firebaseio.com/reservations/takeAway");

    private Map<String, String> cartDishes;
    private Map<String, String> cartPromos;

    private final LoadingDialogFragment loading = new LoadingDialogFragment();

    private CustCartBinding bind;
    private FragmentPipe<Pair<Integer, Integer>> timePipe;
    private FragmentPipe<Triplet<Integer, Integer, Integer>> dayPipe;
    private FragmentPipe<AuthData> authPipe;
    private FragmentPipe<Void> updatePipe;

    private final GregorianCalendar chosenTime = new GregorianCalendar();

    private class timeListener implements FragmentPipe.IReceiver<Pair<Integer, Integer>> {
        @Override
        public void deliver(Pair<Integer, Integer> pack) {
            chosenTime.set(Calendar.HOUR_OF_DAY, pack.first);
            chosenTime.set(Calendar.MINUTE, pack.second);
            bind.setCurrCal(chosenTime);
        }
    }

    private class dayListener implements FragmentPipe.IReceiver<Triplet<Integer, Integer, Integer>> {
        @Override
        public void deliver(Triplet<Integer, Integer, Integer> pack) {
            chosenTime.set(pack.first, pack.second, pack.third);
            bind.setCurrCal(chosenTime);
        }
    }

    private class authListener implements FragmentPipe.IReceiver<AuthData> {
        @Override
        public void deliver(AuthData pack) {
            checkOut(pack);
            getFragmentManager().popBackStack();
        }
    }

    public CartFragment() {
        chosenTime.set(Calendar.MILLISECOND, 0); // Clear milliseconds
        chosenTime.set(Calendar.SECOND, 0); // Clear seconds
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            authPipe = savedInstanceState.getParcelable("authPipe");
            timePipe = savedInstanceState.getParcelable("timePipe");
            dayPipe = savedInstanceState.getParcelable("dayPipe");
            resID = savedInstanceState.getString("resID");
            updatePipe = savedInstanceState.getParcelable("updatePipe");
        } else {
            authPipe = new FragmentPipe<>();
            timePipe = new FragmentPipe<>();
            dayPipe = new FragmentPipe<>();
        }
        authPipe.setReceiver(new authListener());
        timePipe.setReceiver(new timeListener());
        dayPipe.setReceiver(new dayListener());

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("authPipe", authPipe);
        outState.putParcelable("timePipe", timePipe);
        outState.putParcelable("dayPipe", dayPipe);
        outState.putString("resID", resID);
        outState.putParcelable("updatePipe", updatePipe);
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        if (args != null) {
            resID = args.getString("resID");
            updatePipe = args.getParcelable("updatePipe");
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        bind.setDishesAdapter(new CustCartDishAdapter());
        bind.setPromosAdapter(new CustCartPromoAdapter());

        bind.setAnimatorDishes(new DefaultItemAnimator());
        bind.setLayoutManagerDishes(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        bind.setAnimatorPromos(new DefaultItemAnimator());
        bind.setLayoutManagerPromos(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        bind.setCurrCal(chosenTime);

        final DatePickerFragment datePicker = DatePickerFragment.newInstance(dayPipe, chosenTime);
        final TimePickerFragment timePicker = TimePickerFragment.newInstance(timePipe, chosenTime);

        bind.setOnShowDatePickerDialog(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePicker.show(getFragmentManager(), "datePicker");
            }
        });

        bind.setOnShowTimePickerDialog(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timePicker.show(getFragmentManager(), "timePicker");
            }
        });

//        bind.setMenu(new FBMenu(resID));

        totalCost = new ObservableFloat();
        totalCost.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                bind.setCartTotalCost(totalCost.get());
            }
        });

        final SharedPreferences cartPromosPrefs = getActivity().getSharedPreferences("promos:" + resID, Activity.MODE_PRIVATE);
        final SharedPreferences cartDishesPrefs = getActivity().getSharedPreferences("dishes:" + resID, Activity.MODE_PRIVATE);
        final SharedPreferences resPrefs = getActivity().getSharedPreferences(resID, Activity.MODE_PRIVATE);

        cartDishes = (Map<String, String>) cartDishesPrefs.getAll();
        cartPromos = (Map<String, String>) cartPromosPrefs.getAll();

        totalCost.set(resPrefs.getFloat("cartTotalCost", 0.0f));

        bind.setOnCleanCartClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (totalCost.get() == 0.0f) {
                    /* Empty Reservation: do nothing
                     */
                    Toast.makeText(getActivity().getApplicationContext(), R.string.empty_cart, Toast.LENGTH_LONG).show();
                    return;
                }

                //Dopo aver mandato la roba su al server pulisco tutto il locale
                cartDishesPrefs.edit().clear().apply();
                cartPromosPrefs.edit().clear().apply();

                cartDishes.clear();
                cartPromos.clear();

                bind.cartDishesList.getAdapter().notifyDataSetChanged();
                bind.cartPromoList.getAdapter().notifyDataSetChanged();

                totalCost.set(0.0f);
                resPrefs.edit().remove("cartTotalCost").apply();
            }
        });

        bind.setOnCheckOutClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final AuthData auth = fb.getAuth();

                if (totalCost.get() == 0.0f) {
                    /*
                     * Empty Reservation: do nothing
                     */
                    Toast.makeText(getActivity().getApplicationContext(), R.string.empty_cart, Toast.LENGTH_LONG).show();
                    return;
                }

                if (chosenTime.getTimeInMillis() < System.currentTimeMillis()) {
                    /* Reservation in the past: do nothing
                     */
                    Toast.makeText(getActivity().getApplicationContext(), R.string.reservation_past, Toast.LENGTH_LONG).show();
                    return;
                }

                loading.show(getFragmentManager(), "Loading...");

                final Firebase base = new Firebase("https://poleato.firebaseio.com/");

                base.child("users/" + resID + "/timetable").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        Timetable timetable = dataSnapshot.getValue(Timetable.class);
                        GregorianCalendar endTime = new GregorianCalendar();
                        GregorianCalendar startTime = new GregorianCalendar();

                        endTime.setTimeInMillis(timetable.getEnd());
                        startTime.setTimeInMillis(timetable.getStart());

                        boolean day;

                        switch (chosenTime.get(Calendar.DAY_OF_WEEK)) {

                            case Calendar.MONDAY:
                                day = timetable.isLun();
                                break;
                            case Calendar.TUESDAY:
                                day = timetable.isMar();
                                break;
                            case Calendar.WEDNESDAY:
                                day = timetable.isMer();
                                break;
                            case Calendar.THURSDAY:
                                day = timetable.isGio();
                                break;
                            case Calendar.FRIDAY:
                                day = timetable.isVen();
                                break;
                            case Calendar.SATURDAY:
                                day = timetable.isSab();
                                break;
                            case Calendar.SUNDAY:
                                day = timetable.isDom();
                                break;
                            default:
                                day = false;
                                break;
                        }

                        if (!day) {
                            Toast.makeText(getContext(), "Non è possibile fare prenotazioni nei giorni di chiusura del ristorante.",
                                    Toast.LENGTH_LONG).show();
                            loading.dismiss();
                            return;
                        }

                        if ((chosenTime.get(Calendar.HOUR_OF_DAY) < startTime.get(Calendar.HOUR_OF_DAY)) ||

                                (chosenTime.get(Calendar.HOUR_OF_DAY) > endTime.get(Calendar.HOUR_OF_DAY)) ||

                                ((chosenTime.get(Calendar.HOUR_OF_DAY) == endTime.get(Calendar.HOUR_OF_DAY)) &&
                                        ((chosenTime.get(Calendar.MINUTE) < startTime.get(Calendar.MINUTE)) ||
                                                (chosenTime.get(Calendar.MINUTE) > endTime.get(Calendar.MINUTE))))) {

                            Toast.makeText(getContext(), "Non è possibile fare prenotazioni fuori dall'orario " +
                                    "specificato dal ristoratore " + startTime.get(Calendar.HOUR_OF_DAY) + ":" + startTime.get(Calendar.MINUTE)
                                    + " - " + endTime.get(Calendar.HOUR_OF_DAY) + ":" + endTime.get(Calendar.MINUTE), Toast.LENGTH_LONG).show();
                            loading.dismiss();
                            return;
                        }

                        if (auth == null) {
                            loading.dismiss();
                            getFragmentManager()
                                    .beginTransaction()
                                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                                    .add(R.id.fraghost, LoginFragment.newInstance(authPipe), "login")
                                    .addToBackStack(null)
                                    .commit();
                        } else
                            checkOut(auth);
                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError) {
                        loading.dismiss();
                        System.err.println(firebaseError.getDetails());
                    }
                });
            }
        });
    }

    private void checkOut(AuthData auth) {

        final SharedPreferences cartPromosPrefs = getActivity().getSharedPreferences("promos:" + resID, Activity.MODE_PRIVATE);
        final SharedPreferences cartDishesPrefs = getActivity().getSharedPreferences("dishes:" + resID, Activity.MODE_PRIVATE);
        final SharedPreferences resPrefs = getActivity().getSharedPreferences(resID, Activity.MODE_PRIVATE);

        Map<String, Integer> dishesMap = new ArrayMap<>();
        Map<String, Integer> offersMap = new ArrayMap<>();

        for (Map.Entry<String, String> entry : cartDishes.entrySet()) {
            String dishName = entry.getKey();
            QuantityCostParser p = new QuantityCostParser(entry.getValue());

            dishesMap.put(dishName, p.quantity);
        }

        for (Map.Entry<String, String> entry : cartPromos.entrySet()) {
            String promoName = entry.getKey();
            QuantityCostParser p = new QuantityCostParser(entry.getValue());

            offersMap.put(promoName, p.quantity);
        }

        final FBReservations.TakeAwayReservation newRes = new FBReservations.TakeAwayReservation();
        newRes.setDate(chosenTime.getTimeInMillis());
        newRes.setDishes(dishesMap);
        newRes.setPromos(offersMap);
        newRes.setRes_uid(resID);
        newRes.setCust_uid(auth.getUid());

        ValueEventListener listener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.getKey().equals(newRes.getCust_uid())) {
                    newRes.setCust_name((String) dataSnapshot.child("name").getValue());
                    if (newRes.getRes_name() != null) {

                        fb.push().setValue(newRes);

                        new Firebase("https://poleato.firebaseio.com/notify/restaurants/take_away/" + newRes.getRes_uid())
                                .push().setValue(new NotificationService.TakeAway(newRes.getDate()));

                        //Dopo aver mandato la roba su al server pulisco tutto il locale
                        cartDishesPrefs.edit().clear().apply();
                        cartPromosPrefs.edit().clear().apply();

                        cartDishes.clear();
                        cartPromos.clear();

                        totalCost.set(0.0f);
                        resPrefs.edit().remove("cartTotalCost").apply();

                        loading.dismiss();

                        getFragmentManager().popBackStack();
                        Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.add_reservation), Toast.LENGTH_LONG).show();
                    }
                }

                if (dataSnapshot.getKey().equals(newRes.getRes_uid())) {
                    newRes.setRes_name((String) dataSnapshot.child("name").getValue());
                    if (newRes.getCust_name() != null) {

                        fb.push().setValue(newRes);
                        new Firebase("https://poleato.firebaseio.com/notify/restaurants/take_away/" + newRes.getRes_uid())
                                .push().setValue(new NotificationService.TakeAway(newRes.getDate()));

                        //Dopo aver manato la roba su al server pulisco tutto il locale
                        cartDishesPrefs.edit().clear().apply();
                        cartPromosPrefs.edit().clear().apply();

                        cartDishes.clear();
                        cartPromos.clear();

                        totalCost.set(0.0f);
                        resPrefs.edit().remove("cartTotalCost").apply();

                        loading.dismiss();

                        getFragmentManager().popBackStack();
                        Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.add_reservation), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                System.out.println(firebaseError.toString());
            }
        };

        new Firebase("https://poleato.firebaseio.com/users/" + newRes.getRes_uid()).addListenerForSingleValueEvent(listener);
        new Firebase("https://poleato.firebaseio.com/users/" + newRes.getCust_uid()).addListenerForSingleValueEvent(listener);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        bind = CustCartBinding.inflate(inflater, container, false);

        return bind.getRoot();
    }

    public class CustCartDishViewHolder extends RecyclerView.ViewHolder {
        private CustCartItemBinding binding;

        final SharedPreferences cartDishesPrefs = getActivity().getSharedPreferences("dishes:" + resID, Activity.MODE_PRIVATE);
        final SharedPreferences resPrefs = getActivity().getSharedPreferences(resID, Activity.MODE_PRIVATE);

        public CustCartDishViewHolder(final CustCartItemBinding bind, final CustCartDishAdapter adapter) {
            super(bind.getRoot());

            bind.setOnAddClick(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String dishName = bind.getItemName();

                    QuantityCostParser p = new QuantityCostParser(cartDishesPrefs.getString(dishName, null));

                    p.cost += bind.getCost() / bind.getQuantity();
                    p.quantity += 1;

                    bind.setCost(p.cost);
                    bind.setQuantity(p.quantity);

                    Float cartTotalCost = resPrefs.getFloat("cartTotalCost", 0.0f);
                    cartTotalCost += p.cost / p.quantity;
                    resPrefs.edit().putFloat("cartTotalCost", cartTotalCost).apply();

                    totalCost.set(cartTotalCost);

                    cartDishesPrefs.edit().putString(dishName, p.parse()).commit();
                    cartDishes.put(dishName, p.parse());
                }
            });

            bind.setOnRemClick(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String dishName = bind.getItemName();

                    QuantityCostParser p = new QuantityCostParser(cartDishesPrefs.getString(dishName, null));

                    if (p.quantity > 1) {
                        p.cost -= bind.getCost() / bind.getQuantity();
                        p.quantity -= 1;

                        bind.setCost(p.cost);
                        bind.setQuantity(p.quantity);

                        cartDishesPrefs.edit().putString(dishName, p.parse()).commit();
                        cartDishes.put(dishName, p.parse());

                        Float cartTotalCost = resPrefs.getFloat("cartTotalCost", 0.0f);
                        cartTotalCost -= p.cost / p.quantity;
                        resPrefs.edit().putFloat("cartTotalCost", cartTotalCost).apply();
                        totalCost.set(cartTotalCost);
                    }
                }
            });

            bind.setOnDeleteClick(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String dishName = bind.getItemName();

                    QuantityCostParser p = new QuantityCostParser(cartDishesPrefs.getString(dishName, null));

                    cartDishesPrefs.edit().remove(dishName).apply();
                    cartDishes.remove(dishName);
                    adapter.notifyDataSetChanged();

                    Float cartTotalCost = resPrefs.getFloat("cartTotalCost", 0.0f);
                    cartTotalCost -= p.cost;
                    resPrefs.edit().putFloat("cartTotalCost", cartTotalCost).apply();

                    totalCost.set(cartTotalCost);
                }
            });

            binding = bind;
        }

        public CustCartItemBinding getBindings() {
            return binding;
        }
    }

    public class CustCartDishAdapter extends RecyclerView.Adapter<CustCartDishViewHolder> {
        @Override
        public CustCartDishViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            CustCartItemBinding bind = CustCartItemBinding.inflate(
                    getActivity().getLayoutInflater(), parent, true);
            return new CustCartDishViewHolder(bind, this);
        }

        @Override
        public void onBindViewHolder(CustCartDishViewHolder holder, int position) {
            String dishName = (String) (cartDishes.keySet().toArray()[position]);
            holder.getBindings().setItemName(dishName);

            QuantityCostParser p = new QuantityCostParser(cartDishes.get(dishName));

            holder.getBindings().setCost(p.cost);
            holder.getBindings().setQuantity(p.quantity);

        }

        @Override
        public int getItemCount() {
            return cartDishes.size();
        }
    }

    public class CustCartPromoViewHolder extends RecyclerView.ViewHolder {
        private CustCartItemBinding binding;

        final SharedPreferences cartPromosPrefs = getActivity().getSharedPreferences("promos:" + resID, Activity.MODE_PRIVATE);
        final SharedPreferences resPrefs = getActivity().getSharedPreferences(resID, Activity.MODE_PRIVATE);

        public CustCartPromoViewHolder(final CustCartItemBinding bind, final CustCartPromoAdapter adapter) {
            super(bind.getRoot());

            bind.setOnAddClick(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String promoName = bind.getItemName();

                    QuantityCostParser p = new QuantityCostParser(cartPromosPrefs.getString(promoName, null));

                    p.cost += bind.getCost() / bind.getQuantity();
                    p.quantity += 1;

                    bind.setCost(p.cost);
                    bind.setQuantity(p.quantity);

                    Float cartTotalCost = resPrefs.getFloat("cartTotalCost", 0.0f);
                    cartTotalCost += p.cost / p.quantity;
                    resPrefs.edit().putFloat("cartTotalCost", cartTotalCost).apply();

                    totalCost.set(cartTotalCost);

                    cartPromosPrefs.edit().putString(promoName, p.parse()).apply();
                    cartPromos.put(promoName, p.parse());
                }
            });

            bind.setOnRemClick(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String promoName = bind.getItemName();

                    QuantityCostParser p = new QuantityCostParser(cartPromosPrefs.getString(promoName, null));

                    if (p.quantity > 1) {
                        p.cost -= bind.getCost() / bind.getQuantity();
                        p.quantity -= 1;

                        bind.setCost(p.cost);
                        bind.setQuantity(p.quantity);

                        cartPromosPrefs.edit().putString(promoName, p.parse()).apply();
                        cartPromos.put(promoName, p.parse());

                        Float cartTotalCost = resPrefs.getFloat("cartTotalCost", 0.0f);
                        cartTotalCost -= p.cost / p.quantity;
                        resPrefs.edit().putFloat("cartTotalCost", cartTotalCost).apply();
                    }
                }
            });

            bind.setOnDeleteClick(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String promoName = bind.getItemName();

                    QuantityCostParser p = new QuantityCostParser(cartPromosPrefs.getString(promoName, null));

                    cartPromosPrefs.edit().remove(promoName).apply();
                    cartPromos.remove(promoName);
                    adapter.notifyDataSetChanged();

                    Float cartTotalCost = resPrefs.getFloat("cartTotalCost", 0.0f);
                    cartTotalCost -= p.cost;
                    resPrefs.edit().putFloat("cartTotalCost", cartTotalCost).apply();

                    totalCost.set(cartTotalCost);
                }
            });

            binding = bind;
        }

        public CustCartItemBinding getBindings() {
            return binding;
        }
    }

    public class CustCartPromoAdapter extends RecyclerView.Adapter<CustCartPromoViewHolder> {
        @Override
        public CustCartPromoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            CustCartItemBinding bind = CustCartItemBinding.inflate(
                    getActivity().getLayoutInflater(), parent, true);
            return new CustCartPromoViewHolder(bind, this);
        }

        @Override
        public void onBindViewHolder(CustCartPromoViewHolder holder, int position) {
            String promoName = (String) (cartPromos.keySet().toArray()[position]);
            holder.getBindings().setItemName(promoName);

            QuantityCostParser p = new QuantityCostParser(cartPromos.get(promoName));

            holder.getBindings().setCost(p.cost);
            holder.getBindings().setQuantity(p.quantity);

        }

        @Override
        public int getItemCount() {
            return cartPromos.size();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        updatePipe.deliver(null);
    }
}
