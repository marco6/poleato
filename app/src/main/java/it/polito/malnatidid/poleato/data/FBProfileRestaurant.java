package it.polito.malnatidid.poleato.data;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import it.polito.malnatidid.poleato.BR;
import it.polito.malnatidid.poleato.util.FirebaseLiveObject;
import it.polito.malnatidid.poleato.util.FirebaseSetter;

public class FBProfileRestaurant extends FirebaseLiveObject<FBProfileRestaurant> {
    private String name, address, phone, description;

    private boolean asian;
    private boolean indian;
    private boolean mexican;
    private boolean fastFood;
    private boolean pizza;
    private boolean kebab;
    private boolean celiac;
    private boolean vegetarian;
    private boolean greek;
    private boolean other;
    private boolean takeAway;
    private boolean served;
    private boolean selfService;
    private boolean delivery;

    private Coordinates coordinates;

    private Evaluation evaluation;

    private Timetable timetable;

    private String photo;

    public FBProfileRestaurant(String resID) {
        super(FBProfileRestaurant.class, "https://poleato.firebaseio.com/users/" + resID);
    }

    @Bindable
    public String getName() {
        return name;
    }

    @FirebaseSetter(BR.name)
    public void setName(String name) {
        this.name = name;
        notifyPropertyChanged(BR.name);
    }

    @Bindable
    public Coordinates getCoordinates() {
        return coordinates;
    }

    @FirebaseSetter(BR.coordinates)
    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    @Bindable
    public String getAddress() {
        return address;
    }

    @FirebaseSetter(BR.address)
    public void setAddress(String address) {
        this.address = address;
    }

    @Bindable
    public String getPhone() {
        return phone;
    }

    @FirebaseSetter(BR.phone)
    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Bindable
    public String getDescription() {
        return description;
    }

    @FirebaseSetter(BR.description)
    public void setDescription(String description) {
        this.description = description;
        notifyPropertyChanged(BR.description);
    }

    @Bindable
    public boolean isAsian() {
        return asian;
    }

    @FirebaseSetter(BR.asian)
    public void setAsian(boolean asian) {
        this.asian = asian;
    }

    @Bindable
    public boolean isIndian() {
        return indian;
    }

    @FirebaseSetter(BR.indian)
    public void setIndian(boolean indian) {
        this.indian = indian;
    }

    @Bindable
    public boolean isMexican() {
        return mexican;
    }

    @FirebaseSetter(BR.mexican)
    public void setMexican(boolean mexican) {
        this.mexican = mexican;
    }

    @Bindable
    public boolean isFastFood() {
        return fastFood;
    }

    @FirebaseSetter(BR.fastFood)
    public void setFastFood(boolean fastFood) {
        this.fastFood = fastFood;
    }

    @Bindable
    public boolean isPizza() {
        return pizza;
    }

    @FirebaseSetter(BR.pizza)
    public void setPizza(boolean pizza) {
        this.pizza = pizza;
    }

    @Bindable
    public boolean isKebab() {
        return kebab;
    }

    @FirebaseSetter(BR.kebab)
    public void setKebab(boolean kebab) {
        this.kebab = kebab;
    }

    @Bindable
    public boolean isCeliac() {
        return celiac;
    }

    @FirebaseSetter(BR.celiac)
    public void setCeliac(boolean celiac) {
        this.celiac = celiac;
    }

    @Bindable
    public boolean isVegetarian() {
        return vegetarian;
    }

    @FirebaseSetter(BR.vegetarian)
    public void setVegetarian(boolean vegetarian) {
        this.vegetarian = vegetarian;
    }

    @Bindable
    public boolean isGreek() {
        return greek;
    }

    @FirebaseSetter(BR.greek)
    public void setGreek(boolean greek) {
        this.greek = greek;
    }

    @Bindable
    public boolean isOther() {
        return other;
    }

    @FirebaseSetter(BR.other)
    public void setOther(boolean other) {
        this.other = other;
    }

    @Bindable
    public boolean isTakeAway() {
        return takeAway;
    }

    @FirebaseSetter(BR.takeAway)
    public void setTakeAway(boolean takeAway) {
        this.takeAway = takeAway;
    }

    @Bindable
    public boolean isServed() {
        return served;
    }

    @FirebaseSetter(BR.served)
    public void setServed(boolean served) {
        this.served = served;
    }

    @Bindable
    public boolean isSelfService() {
        return selfService;
    }

    @FirebaseSetter(BR.selfService)
    public void setSelfService(boolean selfService) {
        this.selfService = selfService;
    }

    @Bindable
    public boolean isDelivery() {
        return delivery;
    }

    @FirebaseSetter(BR.delivery)
    public void setDelivery(boolean delivery) {
        this.delivery = delivery;
    }

    @Bindable
    public Evaluation getEvaluation() {
        return evaluation;
    }

    @FirebaseSetter(BR.evaluation)
    public void setEvaluation(Evaluation evaluation) {
        this.evaluation = evaluation;
    }

    @Bindable
    public Timetable getTimetable() {
        return timetable;
    }

    @FirebaseSetter(BR.timetable)
    public void setTimetable(Timetable timetable) {
        this.timetable = timetable;
    }

    @Bindable
    public String getPhoto() {
        return photo;
    }

    @FirebaseSetter(BR.photo)
    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getType() {
        return "r";
    }

}
