package it.polito.malnatidid.poleato.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import it.polito.malnatidid.poleato.NotificationService;

public class MainActivity extends Activity implements ValueEventListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent i = new Intent(getApplicationContext(), NotificationService.class);
        startService(i);

        Firebase logger = new Firebase("https://poleato.firebaseio.com/");

        if (logger.getAuth() == null) {
            i = new Intent(this, HomeCustActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        } else {
            logger.child("/users/" + logger.getAuth().getUid() + "/type").addValueEventListener(this);
        }
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {

        String type = (String) dataSnapshot.getValue();
        Intent i;

        if ("r".equals(type))
            i = new Intent(this, HomeResActivity.class);
        else
            i = new Intent(this, HomeCustActivity.class);

        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }

    @Override
    public void onCancelled(FirebaseError firebaseError) {}

}