package it.polito.malnatidid.poleato.fragments.customer;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import it.polito.malnatidid.poleato.R;
import it.polito.malnatidid.poleato.data.FBReviews;
import it.polito.malnatidid.poleato.databinding.ProfileResReviewsItemBinding;
import it.polito.malnatidid.poleato.util.FBRecycleViewFragment;
import it.polito.malnatidid.poleato.util.FragmentPipe;
import it.polito.malnatidid.poleato.util.TabFragment;

@TabFragment(R.string.tab_review_title)
public class ResReviewsFragment extends FBRecycleViewFragment<FBReviews,
        ProfileResReviewsItemBinding> implements FragmentPipe.IReceiver<FBReviews.SingleReview> {

    private String resID;
    private FragmentPipe<FBReviews.SingleReview> reviewPipe;

    public ResReviewsFragment() {
        super(R.layout.profile_res_reviews_item, 0, true);
    }

    @Override
    protected void bindItem(BindingsHolder holder, FBReviews data, int position) {
        holder.bindings.setReview(data.get(position));
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        if (args != null && args.containsKey("resID"))
            setData(new FBReviews(resID = args.getString("resID"), "res_uid"));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("resID", resID);
        if (reviewPipe != null)
            outState.putParcelable("review", reviewPipe);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            reviewPipe = savedInstanceState.getParcelable("reviewPipe");
            if (reviewPipe != null)
                reviewPipe.setReceiver(this);
        } else {
            reviewPipe = new FragmentPipe<>();
            reviewPipe.setReceiver(this);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setActionClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager()
                        .beginTransaction()
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .add(R.id.fraghost, AddReviewFragment.newInstance(reviewPipe, resID), "add_review")
                        .addToBackStack(null)
                        .commit();
            }
        });


        if (savedInstanceState != null && savedInstanceState.containsKey("resID"))
            setData(new FBReviews(resID = savedInstanceState.getString("resID"), "res_uid"));
    }

    @Override
    public void deliver(FBReviews.SingleReview pack) {
        this.getData().push(pack);
    }
}
