package it.polito.malnatidid.poleato.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Map;

import it.polito.malnatidid.poleato.util.KeyedValue;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SpecialOffer implements Parcelable, KeyedValue<String> {

    private float price;
    private Map<String, Integer> dishes;
    private String key;
    public String photo;

    public SpecialOffer (){}

    protected SpecialOffer(Parcel in) {
        price = in.readFloat();
        key = in.readString();
        photo = in.readString();
    }

    public static final Creator<SpecialOffer> CREATOR = new Creator<SpecialOffer>() {
        @Override
        public SpecialOffer createFromParcel(Parcel in) {
            return new SpecialOffer(in);
        }

        @Override
        public SpecialOffer[] newArray(int size) {
            return new SpecialOffer[size];
        }
    };

    @Override
    public String toString() {
        return key;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Map<String, Integer> getDishes() {
        return dishes;
    }

    public void setDishes(Map<String, Integer> dishes) {
        this.dishes = dishes;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeFloat(price);
        dest.writeString(key);
        dest.writeString(photo);
    }
}
