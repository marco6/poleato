package it.polito.malnatidid.poleato.util;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import it.polito.malnatidid.poleato.R;

@Retention(RetentionPolicy.RUNTIME)
public @interface TabFragment {
    int value();
}
