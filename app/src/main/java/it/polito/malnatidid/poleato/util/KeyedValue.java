package it.polito.malnatidid.poleato.util;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public interface KeyedValue<K> {
    K getKey();

    @JsonIgnore
    void setKey(K key);
}
