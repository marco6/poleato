package it.polito.malnatidid.poleato.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.firebase.client.AuthData;
import com.firebase.client.Firebase;

import it.polito.malnatidid.poleato.R;
import it.polito.malnatidid.poleato.fragments.LoginFragment;
import it.polito.malnatidid.poleato.fragments.customer.CustProfileFragment;
import it.polito.malnatidid.poleato.fragments.customer.EditCustProfileFragment;
import it.polito.malnatidid.poleato.util.FragmentPipe;
import it.polito.malnatidid.poleato.util.Toolbar;

public class CustProfileActivity extends AppCompatActivity implements FragmentPipe.IReceiver<AuthData> {

    private FragmentPipe<AuthData> authPipe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_container);


        if (savedInstanceState == null) {
            Firebase node = new Firebase("https://poleato.firebaseio.com/");
            AuthData auth = node.getAuth();
            if (auth == null) {
                //Se no ne faccio una nuova
                authPipe = new FragmentPipe<>();
                authPipe.setReceiver(this);
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fraghost, LoginFragment.newInstance(authPipe), "login")
                        .commit();
            } else {
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fraghost, new CustProfileFragment(), "cust_profile_frag")
                        .commit();

                if (getIntent().getBooleanExtra("editProfile", false)) {
                    getSupportFragmentManager()
                            .beginTransaction()
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                            .add(R.id.fraghost, new EditCustProfileFragment(), "profile_cust_edit_info")
                            .addToBackStack(null)
                            .commit();
                }
            }
        } else {
            // Se stiamo ricostruendo carico dallo stato precedente la mia pipe
            authPipe = savedInstanceState.getParcelable("authPipe");
            if (authPipe != null)
                authPipe.setReceiver(this);
        }

        Toolbar.onCreateToolbar(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (authPipe != null)
            outState.putParcelable("authPipe", authPipe);
    }

    @Override
    public void deliver(AuthData pack) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fraghost, new CustProfileFragment(), "cust_profile_frag")
                .commit();
    }

}
