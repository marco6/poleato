package it.polito.malnatidid.poleato.fragments.customer;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.firebase.client.AuthData;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import it.polito.malnatidid.poleato.R;
import it.polito.malnatidid.poleato.data.Evaluation;
import it.polito.malnatidid.poleato.data.FBReviews;
import it.polito.malnatidid.poleato.databinding.ReviewCustBinding;
import it.polito.malnatidid.poleato.fragments.LoginFragment;
import it.polito.malnatidid.poleato.util.FragmentPipe;

public class AddReviewFragment extends Fragment {

    private ReviewCustBinding binding;
    private FragmentPipe<AuthData> authPipe;

    public static AddReviewFragment newInstance(FragmentPipe<FBReviews.SingleReview> fp, String res_uid) {
        AddReviewFragment lf = new AddReviewFragment();

        // Supply pipe input as an argument.
        Bundle args = new Bundle();
        args.putParcelable("pipe", fp);
        args.putString("res_uid", res_uid);
        lf.setArguments(args);

        return lf;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("authPipe", authPipe);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            authPipe = savedInstanceState.getParcelable("authPipe");
        } else {
            authPipe = new FragmentPipe<>();
        }
        authPipe.setReceiver(new authListener());
    }

    private class authListener implements FragmentPipe.IReceiver<AuthData> {
        @Override
        public void deliver(AuthData pack) {
            save(pack);
            getFragmentManager().popBackStack();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = ReviewCustBinding.inflate(inflater, container, false);
        binding.setOnSaveClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Firebase node = new Firebase("https://poleato.firebaseio.com/");
                AuthData auth = node.getAuth();

                if (auth == null) {
                    getFragmentManager()
                            .beginTransaction()
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                            .add(R.id.fraghost, LoginFragment.newInstance(authPipe), "login")
                            .addToBackStack(null)
                            .commit();
                } else {
                    save(auth);
                }
            }
        });

        return binding.getRoot();
    }

    private void save(AuthData auth) {

        final FBReviews.SingleReview sr = new FBReviews.SingleReview();
        sr.setTitle(binding.reviewTitle.getText().toString());
        sr.setComment(binding.reviewComment.getText().toString());
        sr.setDate(System.currentTimeMillis());
        sr.setEvaluation(new Evaluation(getReviewValue(binding.reviewClean),
                getReviewValue(binding.reviewPrice),
                getReviewValue(binding.reviewFood)));

        Firebase node = new Firebase("https://poleato.firebaseio.com/");
        final String cust_uid = auth.getUid();
        final String res_uid = getArguments().getString("res_uid");

        if (auth != null) {
            sr.setCust_uid(cust_uid);
        }
        sr.setRes_uid(res_uid);

        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String uid = dataSnapshot.getKey();

                if (cust_uid.compareTo(uid) == 0) {
                    sr.setCust_name(dataSnapshot.child("name").getValue(String.class));

                    sr.setPhoto(dataSnapshot.child("photo").getValue(String.class));

                    if (sr.getRes_name() != null) {
                        FragmentPipe<FBReviews.SingleReview> reviewPipe = getArguments().getParcelable("pipe");

                        if (reviewPipe != null) {
                            reviewPipe.deliver(sr);
                        }
                    }


                } else if (res_uid.compareTo(uid) == 0) {
                    sr.setRes_name(dataSnapshot.child("name").getValue(String.class));
                    if (sr.getCust_name() != null) {
                        FragmentPipe<FBReviews.SingleReview> reviewPipe = getArguments().getParcelable("pipe");

                        if (reviewPipe != null) {
                            reviewPipe.deliver(sr);
                        }
                    }
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                System.out.println("The read failed: " +
                        firebaseError.getMessage());
            }
        };

        node.child("users").child(cust_uid).addValueEventListener(valueEventListener);
        node.child("users").child(res_uid).addValueEventListener(valueEventListener);

        getFragmentManager().popBackStack();
        Toast.makeText(getContext(), "Recensione pubblicata!", Toast.LENGTH_LONG).show();
    }

    private double getReviewValue(RadioGroup rg) {
        int id = rg.getCheckedRadioButtonId();
        if (id == -1) {
            return 0;
        }
        return Double.valueOf(rg.findViewById(id).getTag().toString());
    }
}
