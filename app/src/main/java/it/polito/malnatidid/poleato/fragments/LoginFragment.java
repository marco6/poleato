package it.polito.malnatidid.poleato.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.firebase.client.AuthData;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import it.polito.malnatidid.poleato.activities.CustProfileActivity;
import it.polito.malnatidid.poleato.activities.HomeCustActivity;
import it.polito.malnatidid.poleato.R;
import it.polito.malnatidid.poleato.activities.HomeResActivity;
import it.polito.malnatidid.poleato.databinding.LoginBinding;
import it.polito.malnatidid.poleato.fragments.customer.RegisterClientFragment;
import it.polito.malnatidid.poleato.fragments.restaurant.RegisterRestaurantFragment;
import it.polito.malnatidid.poleato.util.FragmentPipe;

public class LoginFragment extends Fragment implements Firebase.AuthResultHandler, ValueEventListener {

    private final LoadingDialogFragment loading = new LoadingDialogFragment();
    private LoginBinding binding;
    private final Firebase logger = new Firebase("https://poleato.firebaseio.com/");
    private FragmentPipe<AuthData> pipe;

    public boolean register = false;

    public static LoginFragment newInstance(FragmentPipe<AuthData> fp) {
        LoginFragment lf = new LoginFragment();
        lf.pipe = fp;
        return lf;
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        if (args.containsKey("pipe"))
            pipe = args.getParcelable("pipe");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("pipe", pipe);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null && pipe == null)
            pipe = savedInstanceState.getParcelable("pipe");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        /*
         * Avoid automatically appear android keyboard when activity start
         */
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        if (logger.getAuth() != null) {
            return null;
        }

        binding = LoginBinding.inflate(inflater, container, false);
        binding.setLogin(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login(binding.user.getText().toString(), binding.pwd.getText().toString());
            }
        });

        binding.setClientReg(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager()
                        .beginTransaction()
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .replace(R.id.fraghost, new RegisterClientFragment(), "register_client")
                        .addToBackStack(null)
                        .commit();
            }
        });

        binding.setRestReg(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager()
                        .beginTransaction()
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .replace(R.id.fraghost, new RegisterRestaurantFragment(), "register_restaurant")
                        .addToBackStack(null)
                        .commit();
            }
        });

        return binding.getRoot();
    }

    @Override
    public void onAuthenticated(AuthData authData) {
        logger.child("/users/" + authData.getUid() + "/type").addListenerForSingleValueEvent(this);
    }

    @Override
    public void onAuthenticationError(FirebaseError firebaseError) {
        loading.dismiss();
        Toast.makeText(getContext(), firebaseError.getMessage(), Toast.LENGTH_LONG).show();
    }

    public void login(String mail, String password) {
        InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

        loading.show(getFragmentManager(), "loading");
        logger.authWithPassword(mail, password, this);
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {

        loading.dismiss();
        Toast.makeText(getContext(), "Logged in!", Toast.LENGTH_LONG).show();

        String type = (String) dataSnapshot.getValue();

        if (type.equals("r")) {
            Intent i = new Intent(getActivity(), HomeResActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            getActivity().startActivity(i);
            return;
        }

        if (register) {
            register = false;

            Intent i = new Intent(getContext(), CustProfileActivity.class);
            i.putExtra("editProfile", true);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);

        } else {
            Intent i = new Intent(getActivity(), HomeCustActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            getActivity().startActivity(i);
        }
        if (pipe != null)
            pipe.deliver(logger.getAuth());
    }

    @Override
    public void onCancelled(FirebaseError firebaseError) {
        if (getContext() != null) {
            loading.dismiss();
            logger.unauth();
            Toast.makeText(getContext(), firebaseError.getMessage(), Toast.LENGTH_LONG).show();
        }
    }
}
