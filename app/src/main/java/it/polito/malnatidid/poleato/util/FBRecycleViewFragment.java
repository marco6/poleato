package it.polito.malnatidid.poleato.util;

import android.databinding.DataBindingUtil;
import android.databinding.Observable;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import it.polito.malnatidid.poleato.R;
import it.polito.malnatidid.poleato.databinding.GenericRecycleViewBinding;

public abstract class FBRecycleViewFragment<D extends FirebaseCollectionBase,
        B extends ViewDataBinding>
        extends Fragment {

    private final Observable.OnPropertyChangedCallback adapterUpdater = new Observable.OnPropertyChangedCallback() {
        @Override
        public void onPropertyChanged(Observable sender, int propertyId) {
            if (sender instanceof FirebaseCollectionBase) {

                FirebaseCollectionBase collectionBase = (FirebaseCollectionBase) sender;

                if (propertyId == collectionBase.size()) {
                    if (reverse)
                        propertyId = collectionBase.size() - propertyId - 1;
                    adapter.notifyItemInserted(propertyId);
                } else {
                    if (reverse)
                        propertyId = collectionBase.size() - propertyId - 1;
                    if (propertyId >= 0)
                        adapter.notifyItemChanged(propertyId);
                    else
                        adapter.notifyItemRemoved(~propertyId);
                }
            }
        }
    };

    public class BindingsHolder extends RecyclerView.ViewHolder {
        public final B bindings;

        public BindingsHolder(B bindings) {
            super(bindings.getRoot());
            bindings.getRoot().setOnLongClickListener(longClick);
            this.bindings = bindings;
        }

    }

    protected class Adapter extends RecyclerView.Adapter<FBRecycleViewFragment<D, B>.BindingsHolder> {

        @Override
        public BindingsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            B bind = DataBindingUtil.inflate(getActivity().getLayoutInflater(), itemLayout, parent, false);
            return new BindingsHolder(bind);
        }

        @Override
        public int getItemCount() {
            if (data != null)
                return data.size();
            return 0;
        }

        @Override
        public void onBindViewHolder(BindingsHolder holder, int position) {
            if (reverse)
                position = data.size() - position - 1;
            animate(holder);
            bindItem(holder, data, position);
        }

        public void animate(RecyclerView.ViewHolder viewHolder) {
            final Animation animAnticipateOvershoot = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce_interpolator);
            Animation old = viewHolder.itemView.getAnimation();
            if (old != null) old.cancel();
            viewHolder.itemView.clearAnimation();
            viewHolder.itemView.setAnimation(animAnticipateOvershoot);
        }
    }

    protected final int actionImageResource, itemLayout;
    private View.OnLongClickListener longClick;
    private View.OnClickListener actionClick;
    private GenericRecycleViewBinding binding;
    private boolean reverse = false;
    private final RecyclerView.ItemAnimator itemAnimator;
    protected final Adapter adapter = new Adapter();

    protected final void setLongClick(View.OnLongClickListener longClick) {
        this.longClick = longClick;
    }

    protected final void setActionClick(View.OnClickListener actionClick) {
        this.actionClick = actionClick;
        if (binding != null)
            binding.setAction(actionClick);
    }

    public FBRecycleViewFragment(int itemLayout) {
        this(itemLayout, 0, false);
    }

    public FBRecycleViewFragment(int itemLayout, int actionImageResource) {
        this(itemLayout, actionImageResource, false);
    }

    public FBRecycleViewFragment(int itemLayout, int actionImageResource, boolean reverse) {
        this(itemLayout, actionImageResource, reverse, new DefaultItemAnimator());
    }

    public FBRecycleViewFragment(int itemLayout, int actionImageResource, boolean reverse, RecyclerView.ItemAnimator animator) {
        super();
        this.itemLayout = itemLayout;
        this.actionImageResource = actionImageResource;
        this.reverse = reverse;
        this.itemAnimator = animator;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = GenericRecycleViewBinding.inflate(inflater, container, false);

        binding.setAdapter(adapter);
        binding.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        binding.setAnimator(itemAnimator);

        binding.setAction(actionClick);
        binding.setActionImage(actionImageResource);

        if (binding.hasPendingBindings())
            binding.executePendingBindings();

        return binding.getRoot();
    }

    private D data;

    protected D getData() {
        return data;
    }

    protected void setData(D data) {
        if (this.data != null)
            this.data.removeOnPropertyChangedCallback(adapterUpdater);
        this.data = data;
        this.data.addOnPropertyChangedCallback(adapterUpdater);
        adapter.notifyDataSetChanged();
    }

    protected abstract void bindItem(BindingsHolder holder, D data, int position);

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (data != null)
            data.removeOnPropertyChangedCallback(adapterUpdater);
        binding = null;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
