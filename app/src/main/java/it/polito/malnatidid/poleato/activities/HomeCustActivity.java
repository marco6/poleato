package it.polito.malnatidid.poleato.activities;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Firebase;

import it.polito.malnatidid.poleato.R;
import it.polito.malnatidid.poleato.databinding.DrawerMenuBinding;
import it.polito.malnatidid.poleato.util.Toolbar;

public class HomeCustActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerMenuBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.drawer_menu);

        binding.setItemClick(this);

        binding.search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    search(v);
                    return true;
                }
                return false;
            }
        });

        Toolbar.onCreateToolbar(this);
    }

    public void openMap(View sender) {
        Intent i = new Intent(this, MapsActivity.class);
        startActivity(i);
    }

    public void filter(View sender) {
        Intent i = new Intent(this, ListActivity.class);
        i.putExtra("home_filter", true);
        startActivity(i);
    }

    // Per ora così... Poi è da migliorare.
    public void search(View sender) {
        Intent i = new Intent(this, ListActivity.class);
        i.putExtra("query", ((TextView) findViewById(R.id.search)).getText().toString());
        startActivity(i);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.filter:
                Intent i = new Intent(this, ListActivity.class);
                startActivity(i);
                break;
            case R.id.about:
                new AlertDialog.Builder(this)
                        .setTitle("About us")
                        .setMessage("A.A. 2015/2016 \nProf. : Giovanni Malnati \nCourse : Mobile Application Development \n\nDeveloped by: \n\t- Alessio Bonato \n\t- Vincenzo Junior Forte \n\t- Marco Manino \n\t- Alessia Mantovani \n\n\nPS: siamo fighi xD.")
                        .show();
                break;
            case R.id.logout:
                Firebase logger = new Firebase("https://poleato.firebaseio.com/");
                if (logger.getAuth() != null) {
                    logger.unauth();
                    Toast.makeText(this, "Logged out!", Toast.LENGTH_LONG).show();
                    Toolbar.onCreateToolbar(this);
                }
                break;
        }

        binding.drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }
}
