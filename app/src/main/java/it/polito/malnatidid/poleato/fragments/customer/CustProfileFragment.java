package it.polito.malnatidid.poleato.fragments.customer;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import it.polito.malnatidid.poleato.databinding.ProfileResBinding;
import it.polito.malnatidid.poleato.fragments.ReviewsFragment;
import it.polito.malnatidid.poleato.util.ViewPagerAdapter;

public class CustProfileFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ProfileResBinding binding = ProfileResBinding.inflate(inflater, container, false);

        binding.pager.setAdapter(new ViewPagerAdapter(getChildFragmentManager(),
                getResources(),
                CustInfoFragment.class,
                HistoryTableReservFragment.class,
                HistoryTakeawayReservFragment.class,
                ReviewsFragment.class
        ));

        binding.tabs.setupWithViewPager(binding.pager);

        return binding.getRoot();
    }
}
