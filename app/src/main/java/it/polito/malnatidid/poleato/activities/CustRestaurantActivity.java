package it.polito.malnatidid.poleato.activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import it.polito.malnatidid.poleato.R;
import it.polito.malnatidid.poleato.databinding.ProfileResBinding;
import it.polito.malnatidid.poleato.fragments.customer.PromoFragment;
import it.polito.malnatidid.poleato.fragments.customer.ResInfoFragment;
import it.polito.malnatidid.poleato.fragments.customer.ResMenuFragment;
import it.polito.malnatidid.poleato.fragments.customer.ResReviewsFragment;
import it.polito.malnatidid.poleato.util.Toolbar;
import it.polito.malnatidid.poleato.util.ViewPagerAdapter;

public class CustRestaurantActivity extends AppCompatActivity {

    public String resID;
    public ProfileResBinding binding;

    private final Firebase node = new Firebase("https://poleato.firebaseio.com/");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        if(intent.hasExtra("notification_id")) {
            new Firebase("https://poleato.firebaseio.com/" + intent.getStringExtra("notification_id")).setValue(true);
        }
        resID = intent.getStringExtra("resid");

        binding = DataBindingUtil.setContentView(this, R.layout.profile_res);

        Bundle args = new Bundle();
        args.putString("resID", resID);

        binding.pager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager(),
                getResources(),
                args,
                ResInfoFragment.class,
                PromoFragment.class,
                ResMenuFragment.class,
                ResReviewsFragment.class
        ));

        binding.tabs.setupWithViewPager(binding.pager);

        binding.tabs.getTabAt(intent.getIntExtra("page", 0)).select();

        Toolbar.onCreateToolbar(this);

        node.child("users/" + resID + "/name").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Toolbar.setTitle(CustRestaurantActivity.this, dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }
}
