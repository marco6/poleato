package it.polito.malnatidid.poleato.util;

import android.databinding.BaseObservable;
import android.support.annotation.NonNull;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import it.polito.malnatidid.poleato.data.FBRestaurantList;

public abstract class FirebaseCollectionBase<K, V extends KeyedValue<K>> extends BaseObservable implements Map<K, V>, ChildEventListener {

    private OnFirebaseError eHandler = null;

    private final FBFilter<K, V> filter;
    protected final HashMap<K, IntRef> keyMap = new HashMap<>();
    private final ArrayList<IntRef> keyRefs = new ArrayList<>();
    private final ArrayList<V> values = new ArrayList<>();
    protected final Query ref;
    protected final Class<V> type;

    public FirebaseCollectionBase(Class<V> value_type, String node) {
        this(value_type, node, (FBFilter<K, V>) FBFilter.identity);
    }

    public FirebaseCollectionBase(Class<V> value_type, String node, FBFilter<K, V> filter) {
        super();
        this.filter = filter;
        ref = new Firebase(node);
        ref.addChildEventListener(this);
        type = value_type;
    }

    public FirebaseCollectionBase(Class<V> value_type, String node, String childName, double equal) {
        super();
        this.filter = (FBFilter<K, V>) FBFilter.identity;
        ref = new Firebase(node).orderByChild(childName).equalTo(equal);
        ref.addChildEventListener(this);
        type = value_type;
    }

    public FirebaseCollectionBase(Class<V> value_type, String node, String childName, double start, double end) {
        super();
        this.filter = (FBFilter<K, V>) FBFilter.identity;
        ref = new Firebase(node).orderByChild(childName).startAt(start).endAt(end);
        ref.addChildEventListener(this);
        type = value_type;
    }

    public FirebaseCollectionBase(Class<V> value_type, String node, String childName, String equal) {
        super();
        this.filter = (FBFilter<K, V>) FBFilter.identity;
        Query ref = new Firebase(node).orderByChild(childName);
        type = value_type;

        if(equal != null)
            ref = ref.equalTo(equal);

        ref.addChildEventListener(this);
        this.ref = ref;
    }

    public FirebaseCollectionBase(Class<V> value_type, String node, String childName, String start, String end) {
        super();
        this.filter = (FBFilter<K, V>) FBFilter.identity;
        Query ref = new Firebase(node).orderByChild(childName);
        type = value_type;

        if(start != null)
            ref = ref.startAt(start);

        if(end != null)
            ref = ref.endAt(end);

        ref.addChildEventListener(this);
        this.ref = ref;
    }

    public FirebaseCollectionBase(Class<V> value_type, String node, String childName, boolean equal) {
        super();
        this.filter = (FBFilter<K, V>) FBFilter.identity;
        ref = new Firebase(node).orderByChild(childName).equalTo(equal);
        type = value_type;
        ref.addChildEventListener(this);
    }

    public FirebaseCollectionBase(Class<V> value_type, String node, String childName, boolean start, boolean end) {
        super();
        this.filter = (FBFilter<K, V>) FBFilter.identity;
        ref = new Firebase(node).orderByChild(childName).startAt(start).endAt(end);
        type = value_type;
        ref.addChildEventListener(this);
    }

    public FirebaseCollectionBase(Class<V> value_type, String node, String childName, String equal, FBFilter<K, V> filter) {
        super();
        this.filter = filter;
        Query ref = new Firebase(node).orderByChild(childName);
        type = value_type;

        if(equal != null)
            ref = ref.equalTo(equal);

        ref.addChildEventListener(this);
        this.ref = ref;
    }

    public void setErrorHandler(OnFirebaseError eHandler) {
        this.eHandler = eHandler;
    }

    @Override
    public void clear() {
        keyMap.clear();
        values.clear();
        ref.removeEventListener(this);
        ref.addChildEventListener(this);
    }

    // O(1*)
    @Override
    public boolean containsKey(Object key) {
        return keyMap.containsKey(key);
    }

    // O(n)
    @Override
    public boolean containsValue(Object value) {
        return values.contains(value);
    }

    @NonNull
    @Override
    public Set<Map.Entry<K, V>> entrySet() {
        // Per ora non supportato
        throw new UnsupportedOperationException();
    }

    // O(1)
    public V get(int i) {
        return values.get(i);
    }

    // O(1*)
    @Override
    public V get(Object key) {
        IntRef i = keyMap.get(key);
        if (i != null)
            return values.get(i.value);
        return null;
    }

    // O(1)
    @Override
    public boolean isEmpty() {
        return keyMap.isEmpty();
    }

    @NonNull
    @Override
    public Set<K> keySet() {
        return keyMap.keySet();
    }

    // O(|map|)
    @Override
    public void putAll(@NonNull Map<? extends K, ? extends V> map) {
        for (Map.Entry<? extends K, ? extends V> e : map.entrySet())
            put(e.getKey(), e.getValue());
    }

    // O(1)
    @Override
    public int size() {
        return keyMap.size();
    }

    @NonNull
    @Override
    public Collection<V> values() {
        return java.util.Collections.unmodifiableCollection(values);
    }

    protected void putInternal(V value) {
        IntRef i = keyMap.get(value.getKey());
        if (i != null) {
            values.set(i.value, value);
        } else {
            IntRef ir = new IntRef(values.size());
            keyMap.put(value.getKey(), ir);
            keyRefs.add(ir);
            values.add(value);
        }
    }

    // O(1*)
    @Override
    public V put(K key, V value) {
        V ret = get(key);
        ref.getRef().child(keyToString(key)).setValue(value);
        return ret;
    }

    public void push(V value) {
        Firebase f = ref.getRef().push();
        value.setKey(getKey(f.getKey()));
        f.setValue(value);
    }

    protected void removeInternal(Object key) {
        IntRef i = keyMap.remove(key);
        if (i != null) {
            values.remove(i.value);
            keyRefs.remove(i.value);
            for (int j = i.value; j < keyRefs.size(); j++)
                keyRefs.get(j).value--;
        }
    }

    // O(n)
    @Override
    public V remove(Object key) {
        V ret = get(key);
        ref.getRef().child(keyToString((K) key)).removeValue();
        return ret;
    }

    public String keyToString(K key) {
        return key.toString();
    }

    public abstract K getKey(String keyString);

    @Override
    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        K key = getKey(dataSnapshot.getKey());
        V value = dataSnapshot.getValue(type);
        value.setKey(key);
        if(filter.eval(value, dataSnapshot)) {
            putInternal(value);
            notifyPropertyChanged(keyMap.get(key).value + 1);
        }
    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
        K key = getKey(dataSnapshot.getKey());
        V value = dataSnapshot.getValue(type);
        value.setKey(key);
        if(filter.eval(value, dataSnapshot)) {
            putInternal(value);
            notifyPropertyChanged(keyMap.get(key).value);
        }
    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {
        K key = getKey(dataSnapshot.getKey());
        IntRef ir = keyMap.get(key);
        removeInternal(getKey(dataSnapshot.getKey()));
        notifyPropertyChanged(~(ir.value));
    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) { }

    @Override
    public void onCancelled(FirebaseError firebaseError) {
        if (eHandler != null)
            eHandler.manage(firebaseError);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        ref.removeEventListener(this);
    }

}
