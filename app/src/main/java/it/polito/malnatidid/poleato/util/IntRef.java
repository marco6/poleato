package it.polito.malnatidid.poleato.util;

import android.os.Parcel;
import android.os.Parcelable;

public class IntRef implements Parcelable{
    public int value;

    public IntRef(int value) {
        this.value = value;
    }

    protected IntRef(Parcel in) {
        value = in.readInt();
    }

    public static final Creator<IntRef> CREATOR = new Creator<IntRef>() {
        @Override
        public IntRef createFromParcel(Parcel in) {
            return new IntRef(in);
        }

        @Override
        public IntRef[] newArray(int size) {
            return new IntRef[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(value);
    }
}
