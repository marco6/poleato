package it.polito.malnatidid.poleato.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import it.polito.malnatidid.poleato.R;
import it.polito.malnatidid.poleato.databinding.ProfileResBinding;
import it.polito.malnatidid.poleato.fragments.ReviewsFragment;
import it.polito.malnatidid.poleato.fragments.restaurant.InfoFragment;
import it.polito.malnatidid.poleato.fragments.restaurant.MenuFragment;
import it.polito.malnatidid.poleato.fragments.restaurant.PromoFragment;
import it.polito.malnatidid.poleato.util.Toolbar;
import it.polito.malnatidid.poleato.util.ViewPagerAdapter;

public class ResProfileActivity extends AppCompatActivity {

    private final Firebase node = new Firebase("https://poleato.firebaseio.com/");

    ProfileResBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String resUid = node.getAuth().getUid();

        Bundle args = new Bundle();
        args.putString("resID", resUid);
        args.putBoolean("type_res", true);

        binding = DataBindingUtil.setContentView(this, R.layout.profile_res);

        binding.pager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager(),
                getResources(),
                args,
                InfoFragment.class,
                PromoFragment.class,
                MenuFragment.class,
                ReviewsFragment.class
        ));

        binding.tabs.setupWithViewPager(binding.pager);

        Toolbar.onCreateToolbar(this);

        node.child("users/" + resUid + "/name").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Toolbar.setTitle(ResProfileActivity.this, dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }
}
