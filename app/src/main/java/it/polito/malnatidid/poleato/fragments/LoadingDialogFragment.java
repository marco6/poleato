package it.polito.malnatidid.poleato.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

public class LoadingDialogFragment extends DialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        ProgressDialog pd = new ProgressDialog(getActivity(), getTheme()) {
            @Override
            public void onBackPressed() {
                // do nothing
            }
        };

        pd.setIndeterminate(true);
        pd.setMessage("Loading...");
        pd.setTitle("Please wait...");
        pd.setCanceledOnTouchOutside(false);
        pd.setCancelable(false);

        return pd;
    }

}
