package it.polito.malnatidid.poleato.activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;

import com.firebase.client.Firebase;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import it.polito.malnatidid.poleato.R;
import it.polito.malnatidid.poleato.databinding.HomeResBinding;
import it.polito.malnatidid.poleato.fragments.restaurant.FBHomeResFragment;
import it.polito.malnatidid.poleato.util.Toolbar;

public class HomeResActivity extends AppCompatActivity {
    HomeResBinding bind;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bind = DataBindingUtil.setContentView(this, R.layout.home_res);

        InfiniteAdapter ia = new InfiniteAdapter(getSupportFragmentManager());

        Intent intent = getIntent();

        if(intent.hasExtra("notification_id")) {
            new Firebase("https://poleato.firebaseio.com/" + intent.getStringExtra("notification_id")).removeValue();
        }

        long start = intent.getLongExtra("start", ia.today) - ia.today, adjust;
        adjust = start % InfiniteAdapter.DAY_INCREMENT;
        start -= (adjust < 0 ? adjust + InfiniteAdapter.DAY_INCREMENT : adjust);

        bind.pager.setAdapter(ia);

        int begin = InfiniteAdapter.size / 2 + (int)(start / InfiniteAdapter.DAY_INCREMENT);

        bind.pager.setCurrentItem(
                //L'infinito saaaaai cos'èèèèèèèèèèèèèè!!!
                begin, false);

        bind.tabs.setOnTabSelectedListener(ia);

        if(savedInstanceState == null)
            bind.tabs.getTabAt(intent.getIntExtra("adapter", 0)).select();
        else
            bind.tabs.getTabAt(savedInstanceState.getInt("adapter")).select();

        bind.executePendingBindings();

        Toolbar.onCreateToolbar(this);
    }

    public interface AdapterChanged {
        void fire(int index);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("adapter", bind.tabs.getSelectedTabPosition());
    }

    private class InfiniteAdapter extends FragmentPagerAdapter implements TabLayout.OnTabSelectedListener {

        private ArrayList<WeakReference<AdapterChanged>> adapterChanged = new ArrayList<>();

        public static final long DAY_INCREMENT = 1000 * 60 * 60 * 24;
        public static final int size = 1001;

        private final long today;

        private int selected = 0;

        public InfiniteAdapter(FragmentManager fm) {
            super(fm);
            GregorianCalendar gc = new GregorianCalendar();
            gc.set(Calendar.MILLISECOND, 0);
            gc.set(Calendar.SECOND, 0);
            gc.set(Calendar.MINUTE, 0);
            gc.set(Calendar.HOUR_OF_DAY, 0);
            today = gc.getTimeInMillis();
        }

        @Override
        public int getCount() {
            return size;
        }

        @Override
        public Fragment getItem(int position) {
            long day = today + DAY_INCREMENT * (position - (size / 2));
            FBHomeResFragment fragment = FBHomeResFragment.newInstance(day, day + DAY_INCREMENT, selected);
            adapterChanged.add(new WeakReference<AdapterChanged>(fragment));
            return fragment;
        }

        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            selected = tab.getPosition();
            ArrayList<WeakReference<AdapterChanged>> toRemove = new ArrayList<>();
            for(WeakReference<AdapterChanged> ref : adapterChanged) {
                AdapterChanged ac = ref.get();
                if(ac != null)
                    ac.fire(selected);
                else
                    toRemove.add(ref);
            }
            adapterChanged.removeAll(toRemove);
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {
        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Object ret = super.instantiateItem(container, position);
            if(ret instanceof AdapterChanged) {
                adapterChanged.add(new WeakReference<>((AdapterChanged) ret));
            }
            return ret;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ArrayList<WeakReference<AdapterChanged>> toRemove = new ArrayList<>();

            for(WeakReference<AdapterChanged> wr : adapterChanged)
                if(wr.get() == object || wr.get() == null)
                    toRemove.add(wr);

            adapterChanged.removeAll(toRemove);

            super.destroyItem(container, position, object);
        }
    }

}
