package it.polito.malnatidid.poleato.fragments.customer;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import it.polito.malnatidid.poleato.NotificationService;
import it.polito.malnatidid.poleato.R;
import it.polito.malnatidid.poleato.activities.CustRestaurantActivity;
import it.polito.malnatidid.poleato.data.FBProfileRestaurant;
import it.polito.malnatidid.poleato.data.FBReservations;
import it.polito.malnatidid.poleato.databinding.ProfileResInfoBinding;
import it.polito.malnatidid.poleato.util.FragmentPipe;
import it.polito.malnatidid.poleato.util.TabFragment;

@TabFragment(R.string.tab_info_title)
public class ResInfoFragment extends Fragment implements FragmentPipe.IReceiver<FBReservations.TableReservation> {

    private ProfileResInfoBinding bind;
    private String resID;
    private FragmentPipe<FBReservations.TableReservation> resultPipe;

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        if (args != null && args.containsKey("resID"))
            resID = args.getString("resID");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("resultPipe", resultPipe);
        outState.putString("resID", resID);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState != null) {
            resultPipe = savedInstanceState.getParcelable("resultPipe");
            resID = savedInstanceState.getString("resID", resID);
        } else if (resID != null) {
            resultPipe = new FragmentPipe<>();
        }

        resultPipe.setReceiver(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        bind = ProfileResInfoBinding.inflate(inflater, container, false);
        bind.setProfile(new FBProfileRestaurant(resID));

        bind.setTableClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AddReservationTableFragment addResFrag = AddReservationTableFragment.newInstance(resultPipe, resID);

                getFragmentManager()
                        .beginTransaction()
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .replace(R.id.fraghost, addResFrag, "add_reservation_cust_table")
                        .addToBackStack(null)
                        .commit();
            }
        });

        bind.setTakeawayClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CustRestaurantActivity) getActivity()).binding.pager.setCurrentItem(1);
            }
        });

        if (bind.hasPendingBindings())
            bind.executePendingBindings();

        return bind.getRoot();
    }

    @Override
    public void deliver(FBReservations.TableReservation pack) {

        final FBReservations.TableReservation newRes = pack;
        newRes.setRes_uid(resID);

        ValueEventListener listener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.getKey().equals(newRes.getCust_uid())) {
                    newRes.setCust_name((String) dataSnapshot.child("name").getValue());
                    if (newRes.getRes_name() != null) {
                        Firebase fb = new Firebase("https://poleato.firebaseio.com/");
                        fb.child("reservations/tables").push().setValue(newRes);

                        fb.child("notify/restaurants/tables/" + newRes.getRes_uid()).push()
                                .setValue(new NotificationService.Table(newRes.getDate(),newRes.getPersons()));
                    }
                }

                if (dataSnapshot.getKey().equals(newRes.getRes_uid())) {
                    newRes.setRes_name((String) dataSnapshot.child("name").getValue());
                    if (newRes.getCust_name() != null) {
                        Firebase fb = new Firebase("https://poleato.firebaseio.com/");
                        fb.child("reservations/tables").push().setValue(newRes);

                        fb.child("notify/restaurants/tables/" + newRes.getRes_uid()).push()
                                .setValue(new NotificationService.Table(newRes.getDate(),newRes.getPersons()));
                    }
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                System.out.println(firebaseError.toString());
            }
        };

        new Firebase("https://poleato.firebaseio.com/users/" + newRes.getRes_uid()).addValueEventListener(listener);
        new Firebase("https://poleato.firebaseio.com/users/" + newRes.getCust_uid()).addValueEventListener(listener);
    }
}


