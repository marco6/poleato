package it.polito.malnatidid.poleato.fragments.restaurant;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.firebase.client.Firebase;

import it.polito.malnatidid.poleato.R;
import it.polito.malnatidid.poleato.data.FBMenu;
import it.polito.malnatidid.poleato.data.SpecialOffer;
import it.polito.malnatidid.poleato.databinding.ProfileResPromoItemBinding;
import it.polito.malnatidid.poleato.util.FBRecycleViewFragment;
import it.polito.malnatidid.poleato.util.TabFragment;

@TabFragment(R.string.tab_promo_title)
public class PromoFragment extends FBRecycleViewFragment<FBMenu.OfferSection, ProfileResPromoItemBinding> {

    private String resID;

    public PromoFragment() {
        super(R.layout.profile_res_promo_item, R.drawable.ic_add_white);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("resID", resID);
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        if (args != null)
            setData(new FBMenu.OfferSection(resID = args.getString("resID")));
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null)
            setData(new FBMenu.OfferSection(resID = savedInstanceState.getString("resID")));

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setActionClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager()
                        .beginTransaction()
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .replace(R.id.fraghost, new EditPromoFragment(), "add_promo_res")
                        .addToBackStack(null)
                        .commit();
            }
        });
    }

    @Override
    protected void bindItem(final BindingsHolder holder, FBMenu.OfferSection data, int position) {
        holder.bindings.setPromo(data.get(position));

        holder.bindings.setOnDeleteClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete(holder.bindings.getPromo());
            }
        });

        holder.bindings.setOnLongClick(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                edit(holder.bindings.getPromo());
                return true;
            }
        });
    }

    protected void edit(SpecialOffer promo) {
        EditPromoFragment editPromofragment = EditPromoFragment.newInstance(promo);

        getFragmentManager()
                .beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(R.id.fraghost, editPromofragment, "add_promo_res")
                .addToBackStack(null)
                .commit();
    }

    protected void delete(SpecialOffer promo){

        Firebase promoRef = new Firebase("https://poleato.firebaseio.com/menus/" + resID + "/special_offers/" + promo.getKey());
        promoRef.removeValue();
    }
}
