package it.polito.malnatidid.poleato;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.firebase.client.AuthData;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import it.polito.malnatidid.poleato.activities.CustRestaurantActivity;
import it.polito.malnatidid.poleato.activities.HomeResActivity;

public class NotificationService extends Service implements Firebase.AuthStateListener, ValueEventListener {

    public static boolean serviceOn = false;
    public static final String TAG = "Poleato";

    private final Firebase logger = new Firebase("https://poleato.firebaseio.com/");
    private String id = null;
    private int lastNotificationID = 0;
    private NotificationManager mNotifyMgr;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");
        if (!serviceOn) {
            logger.addAuthStateListener(this);
            serviceOn = true;
        }
        mNotifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        mNotifyMgr.cancelAll();
        serviceOn = false;
    }

    @Override
    public void onAuthStateChanged(AuthData authData) {
        Log.d(TAG, "onAuthStateChanged");
        if (authData != null) {
            id = authData.getUid();
            logger.child("/users/" + id + "/type").addListenerForSingleValueEvent(this);
        } else {
            mNotifyMgr.cancelAll();
        }
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        String type = (String) dataSnapshot.getValue();

        if (type.equals("r")) {
            new TablesNotificationListener();
            new TakeAwayNotificationListener();
        } else {
            new PromotionNotificationListener();
        }
    }

    @Override
    public void onCancelled(FirebaseError firebaseError) {
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Promotion {
        public String res_id, name;

        public Promotion(String res_id, String name) {
            this.res_id = res_id;
            this.name = name;
        }

        public Promotion() { }
    }

    public static class Table {
        public long time;
        public int num;

        public Table(long time, int num) {
            this.time = time;
            this.num = num;
        }

        public Table() { }
    }

    public static class TakeAway {
        public long time;

        public TakeAway(long time) {
            this.time = time;
        }

        public TakeAway() { }
    }

    private abstract class NotificationListenerBase<C> implements Firebase.AuthStateListener, ChildEventListener {
        private final Query fb;
        private final Class<C> type;

        protected NotificationListenerBase(Query query, Class<C> type) {
            this.type = type;

            // Questa serve anche a tenere una strong reference a questo oggetto!
            logger.addAuthStateListener(this);
            fb = query;
            fb.addChildEventListener(this);

        }

        protected NotificationListenerBase(String node, Class<C> type) {
            this(new Firebase("https://poleato.firebaseio.com/" + node + id), type);
        }

        public abstract void onNotification(String id, C notification);

        @Override
        public void onAuthStateChanged(AuthData authData) {
            if (authData == null) {
                logger.removeAuthStateListener(this);
                fb.removeEventListener(this);
            }
        }

        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
            C n = dataSnapshot.getValue(type);
            onNotification(dataSnapshot.getKey(), n);
        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String s) { }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) { }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String s) { }

        @Override
        public void onCancelled(FirebaseError firebaseError) {
        }
    }

    private class PromotionNotificationListener extends NotificationListenerBase<Promotion> {

        public PromotionNotificationListener() {
            super(new Firebase("https://poleato.firebaseio.com/notify/user/promotion/").orderByChild(id).endAt(false, id), Promotion.class);
        }

        @Override
        public void onNotification(String id, Promotion notification) {
            Intent i = new Intent(NotificationService.this, CustRestaurantActivity.class);
            i.putExtra("resid", notification.res_id);
            i.putExtra("notification_id", "notify/user/promotion/" + id + "/" + NotificationService.this.id);
            i.putExtra("page", 1);
            i.setAction(id);

            PendingIntent intent = PendingIntent.getActivity(NotificationService.this, 0, i, PendingIntent.FLAG_ONE_SHOT);

            mNotifyMgr.notify(id, lastNotificationID++, new NotificationCompat.Builder(NotificationService.this)
                    .setSmallIcon(R.drawable.offer_notify)
                    .setContentTitle(String.format(getResources().getString(R.string.notify_promo), notification.name))
                    .setAutoCancel(true)
                    .setContentIntent(intent).build());
        }
    }

    private class TablesNotificationListener extends NotificationListenerBase<Table> {

        public TablesNotificationListener() {
            super("notify/restaurants/tables/", Table.class);
        }

        @Override
        public void onNotification(String id, Table notification) {
            Intent i = new Intent(NotificationService.this, HomeResActivity.class);
            i.putExtra("start", notification.time);
            i.putExtra("notification_id", "notify/restaurants/tables/" + NotificationService.this.id + "/" + id);
            i.putExtra("adapter", 1);
            i.setAction(id);

            PendingIntent intent = PendingIntent.getActivity(NotificationService.this, 0, i, PendingIntent.FLAG_ONE_SHOT);

            mNotifyMgr.notify(id, lastNotificationID++, new NotificationCompat.Builder(NotificationService.this)
                    .setSmallIcon(R.drawable.ic_table_selected)
                    .setContentTitle(getResources().getString(R.string.notify_table))
                    .setAutoCancel(true)
                    .setContentText(String.format(getResources().getString(R.string.notify_table_descr), notification.num, SimpleDateFormat.getDateTimeInstance().format(new Timestamp(notification.time))))
                    .setContentIntent(intent).build());
        }
    }

    private class TakeAwayNotificationListener extends NotificationListenerBase<TakeAway> {
        protected TakeAwayNotificationListener() {
            super("notify/restaurants/take_away/", TakeAway.class);
        }

        @Override
        public void onNotification(String id, TakeAway notification) {
            Intent i = new Intent(NotificationService.this, HomeResActivity.class);
            i.putExtra("start", notification.time);
            i.putExtra("notification_id", "notify/restaurants/take_away/" + NotificationService.this.id + "/" + id);
            i.putExtra("adapter", 0);
            i.setAction(id);

            PendingIntent intent = PendingIntent.getActivity(NotificationService.this, 0, i, PendingIntent.FLAG_ONE_SHOT);

            mNotifyMgr.notify(id, lastNotificationID++, new NotificationCompat.Builder(NotificationService.this)
                    .setSmallIcon(R.drawable.ic_takeaway_selected)
                    .setAutoCancel(true)
                    .setContentTitle(getResources().getString(R.string.notify_takeaway))
                    .setContentText(String.format(getResources().getString(R.string.notify_takeaway_descr), SimpleDateFormat.getDateTimeInstance().format(new Timestamp(notification.time))))
                    .setContentIntent(intent).build());
        }
    }
}
