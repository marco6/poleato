package it.polito.malnatidid.poleato.data;

public class Timetable {

    private long start, end;
    private boolean lun, mar, mer, gio, ven, sab, dom;

    public Timetable() {};

    public Timetable(long start, long end, boolean lun, boolean mar, boolean mer, boolean gio, boolean ven, boolean sab, boolean dom) {
        this.start = start;
        this.end = end;
        this.lun = lun;
        this.mar = mar;
        this.mer = mer;
        this.gio = gio;
        this.ven = ven;
        this.sab = sab;
        this.dom = dom;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public long getEnd() {
        return end;
    }

    public void setEnd(long end) {
        this.end = end;
    }

    public boolean isLun() {
        return lun;
    }

    public void setLun(boolean lun) {
        this.lun = lun;
    }

    public boolean isMar() {
        return mar;
    }

    public void setMar(boolean mar) {
        this.mar = mar;
    }

    public boolean isMer() {
        return mer;
    }

    public void setMer(boolean mer) {
        this.mer = mer;
    }

    public boolean isGio() {
        return gio;
    }

    public void setGio(boolean gio) {
        this.gio = gio;
    }

    public boolean isVen() {
        return ven;
    }

    public void setVen(boolean ven) {
        this.ven = ven;
    }

    public boolean isSab() {
        return sab;
    }

    public void setSab(boolean sab) {
        this.sab = sab;
    }

    public boolean isDom() {
        return dom;
    }

    public void setDom(boolean dom) {
        this.dom = dom;
    }
}
