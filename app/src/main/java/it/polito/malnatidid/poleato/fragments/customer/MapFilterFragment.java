package it.polito.malnatidid.poleato.fragments.customer;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.SeekBar;

import com.firebase.client.DataSnapshot;

import java.util.ArrayList;

import it.polito.malnatidid.poleato.data.Coordinates;
import it.polito.malnatidid.poleato.data.FBRestaurantList;
import it.polito.malnatidid.poleato.databinding.FilterCustMapBinding;
import it.polito.malnatidid.poleato.util.BooleanOperator;
import it.polito.malnatidid.poleato.util.FBFilter;
import it.polito.malnatidid.poleato.util.FragmentPipe;
import it.polito.malnatidid.poleato.util.GPSTracker;

public class MapFilterFragment extends Fragment {

    private FilterCustMapBinding bind;
    private FragmentPipe<BooleanOperator<FBRestaurantList.LightResProfile, DataSnapshot>> filterPipe;

    public static MapFilterFragment newInstance(FragmentPipe<BooleanOperator<FBRestaurantList.LightResProfile, DataSnapshot>> filterPipe) {
        MapFilterFragment fragment = new MapFilterFragment();
        fragment.filterPipe = filterPipe;
        return fragment;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("filterPipe", filterPipe);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null)
            filterPipe = savedInstanceState.getParcelable("filterPipe");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        bind = FilterCustMapBinding.inflate(inflater, container, false);

        bind.setOnFilterClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filter();
                getFragmentManager().popBackStack();
            }
        });

        bind.distanceSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (seekBar.getProgress() != seekBar.getMax()) {
                    bind.maxDistance.setText(String.valueOf((seekBar.getProgress() + 1) * 100));
                } else
                    bind.maxDistance.setText("Max");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        if (bind.hasPendingBindings())
            bind.executePendingBindings();
        return bind.getRoot();
    }

    public void filter() {
        BooleanOperator<FBRestaurantList.LightResProfile, DataSnapshot> final_op;
        ArrayList<BooleanOperator<FBRestaurantList.LightResProfile, DataSnapshot>> or_level = new ArrayList<>();

        if (bind.takeAway.isChecked())
            or_level.add(new FBFilter.FieldCheck<String, FBRestaurantList.LightResProfile>("takeAway"));

        if (bind.delivery.isChecked())
            or_level.add(new FBFilter.FieldCheck<String, FBRestaurantList.LightResProfile>("delivery"));

        if (bind.selfService.isChecked())
            or_level.add(new FBFilter.FieldCheck<String, FBRestaurantList.LightResProfile>("selfService"));

        if (bind.served.isChecked())
            or_level.add(new FBFilter.FieldCheck<String, FBRestaurantList.LightResProfile>("served"));

        if (bind.asian.isChecked())
            or_level.add(new FBFilter.FieldCheck<String, FBRestaurantList.LightResProfile>("asian"));

        if (bind.indian.isChecked())
            or_level.add(new FBFilter.FieldCheck<String, FBRestaurantList.LightResProfile>("indian"));

        if (bind.mexican.isChecked())
            or_level.add(new FBFilter.FieldCheck<String, FBRestaurantList.LightResProfile>("mexican"));

        if (bind.fastFood.isChecked())
            or_level.add(new FBFilter.FieldCheck<String, FBRestaurantList.LightResProfile>("fastFood"));

        if (bind.pizza.isChecked())
            or_level.add(new FBFilter.FieldCheck<String, FBRestaurantList.LightResProfile>("pizza"));

        if (bind.kebab.isChecked())
            or_level.add(new FBFilter.FieldCheck<String, FBRestaurantList.LightResProfile>("kebab"));

        if (bind.celiac.isChecked())
            or_level.add(new FBFilter.FieldCheck<String, FBRestaurantList.LightResProfile>("celiac"));

        if (bind.vegetarian.isChecked())
            or_level.add(new FBFilter.FieldCheck<String, FBRestaurantList.LightResProfile>("vegetarian"));

        if (bind.greek.isChecked())
            or_level.add(new FBFilter.FieldCheck<String, FBRestaurantList.LightResProfile>("greek"));

        final_op = new FBFilter.OR<>(or_level);

        String distanceString = bind.maxDistance.getText().toString();

        if (bind.distanceSeekBar.getProgress() != bind.distanceSeekBar.getMax()) {

            final Coordinates myCoordinates = new Coordinates();

            myCoordinates.setLatitude(GPSTracker.inst.getLatitude());
            myCoordinates.setLongitude(GPSTracker.inst.getLongitude());

            final_op = new FBFilter.AND<>(final_op,
                    new FBFilter.DistanceCheck(Double.parseDouble(distanceString), myCoordinates)
            );
        }

        double priceValue = getPriceValue(bind.radioPrice);

        if (priceValue != 5)
            final_op = new FBFilter.AND<>(final_op,
                    new FBFilter.DoubleCheck<String, FBRestaurantList.LightResProfile>(
                            "evaluation/price", priceValue)
            );

        filterPipe.deliver(final_op);
    }

    private double getPriceValue(RadioGroup rg) {
        int id = rg.getCheckedRadioButtonId();
        if (id == -1) {
            return 0;
        }
        return Double.valueOf(rg.findViewById(id).getTag().toString());
    }
}
