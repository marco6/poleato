package it.polito.malnatidid.poleato.fragments.customer;

import android.app.Activity;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.databinding.Observable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Toast;

import it.polito.malnatidid.poleato.R;
import it.polito.malnatidid.poleato.data.Dish;
import it.polito.malnatidid.poleato.data.FBMenu;
import it.polito.malnatidid.poleato.databinding.GenericExpandableGroupItemBinding;
import it.polito.malnatidid.poleato.databinding.GenericExpandableListViewBinding;
import it.polito.malnatidid.poleato.databinding.ProfileResMenuItemBinding;
import it.polito.malnatidid.poleato.util.FragmentPipe;
import it.polito.malnatidid.poleato.util.QuantityCostParser;
import it.polito.malnatidid.poleato.util.TabFragment;

@TabFragment(R.string.tab_menu_title)
public class ResMenuFragment extends Fragment implements FragmentPipe.IReceiver<Void> {

    private FBMenu menu;
    private GenericExpandableListViewBinding binding;
    private String resID;
    private FragmentPipe<Void> updatePipe;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState != null){
            resID = savedInstanceState.getString("resID");
            updatePipe = savedInstanceState.getParcelable("updatePipe");
        }else
            updatePipe = new FragmentPipe<>();

        updatePipe.setReceiver(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("resID", resID);
        outState.putParcelable("updatePipe", updatePipe);
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        if (args != null){
            resID = args.getString("resID");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = GenericExpandableListViewBinding.inflate(inflater, container, false);

        binding.imageButton.setImageResource(R.drawable.ic_carrello_white);


        menu = new FBMenu(resID);
        binding.setAdapter(new MenuExpandableListAdapter());

        Bundle args = new Bundle();
        args.putString("resID", resID);
        args.putParcelable("updatePipe", updatePipe);
        final CartFragment frag = new CartFragment();
        frag.setArguments(args);

        binding.setOnOk(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                getFragmentManager()
                        .beginTransaction()
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .replace(R.id.fraghost, frag, "cart_fragment")
                        .addToBackStack(null)
                        .commit();
            }
        });

        if (binding.hasPendingBindings())
            binding.executePendingBindings();

        return binding.getRoot();
    }

    @Override
    public void deliver(Void pack) {
        binding.getAdapter().notifyDataSetChanged();
    }

    public class MenuExpandableListAdapter extends BaseExpandableListAdapter {

        private class OnPropertyChangedCallback extends Observable.OnPropertyChangedCallback {

            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                MenuExpandableListAdapter.this.notifyDataSetChanged();
            }
        }

        public MenuExpandableListAdapter() {
            super();
            OnPropertyChangedCallback p = new OnPropertyChangedCallback();
            for (int j = 0; j < menu.sections.length; j++)
                menu.sections[j].addOnPropertyChangedCallback(p);
        }

        @Override
        public int getGroupCount() {
            return menu.sections.length;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return menu.sections[groupPosition].size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return menu.sections[groupPosition];
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return menu.sections[groupPosition].get(childPosition);
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return ((long) groupPosition << 32) | childPosition;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            final GenericExpandableGroupItemBinding bind;

            if (menu.sections[groupPosition].size() == 0)
                return new View(getContext());
            else {
                if (convertView == null || convertView.getClass() == View.class)
                    bind = GenericExpandableGroupItemBinding.inflate(getActivity().getLayoutInflater(), parent, false);
                else
                    bind = DataBindingUtil.getBinding(convertView);
            }

            bind.setGroupName(menu.sections[groupPosition].section.value);

            if (bind.hasPendingBindings())
                bind.executePendingBindings();

            return bind.getRoot();
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

            final ProfileResMenuItemBinding bind;
            final Dish newDish = menu.sections[groupPosition].get(childPosition);

            final SharedPreferences cartDishesPrefs = getActivity().getSharedPreferences("dishes:" + resID, Activity.MODE_PRIVATE);
            final SharedPreferences resPrefs = getActivity().getSharedPreferences(resID, Activity.MODE_PRIVATE);

            if (convertView == null) {
                bind = ProfileResMenuItemBinding.inflate(
                        getActivity().getLayoutInflater(), parent, false);

                bind.resInfoEditButton.setBackgroundResource(R.drawable.round_button);
                bind.resInfoEditButton.setImageResource(R.drawable.ic_add_white);

            } else
                bind = DataBindingUtil.getBinding(convertView);

            bind.setOnDeleteClick(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    QuantityCostParser p = new QuantityCostParser(cartDishesPrefs.getString(newDish.getKey(), null));

                    p.quantity += 1;
                    p.cost += newDish.getPrice();

                    cartDishesPrefs.edit().putString(newDish.getKey(), p.parse()).apply();

                    Float cartTotalCost = resPrefs.getFloat("cartTotalCost", 0.0f);
                    cartTotalCost += newDish.getPrice();
                    resPrefs.edit().putFloat("cartTotalCost", cartTotalCost).apply();

                    bind.setQuantity(p.quantity);

                    Toast.makeText(getActivity().getApplicationContext(), bind.getDish().getKey() + " " + getResources().getString(R.string.add_reservation_menu_item), Toast.LENGTH_LONG).show();
                }
            });

            QuantityCostParser p = new QuantityCostParser(cartDishesPrefs.getString(newDish.getKey(), null));
            bind.setQuantity(p.quantity);
            bind.setDish(newDish);
            if (bind.hasPendingBindings())
                bind.executePendingBindings();
            return bind.getRoot();
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return false;
        }
    }
}