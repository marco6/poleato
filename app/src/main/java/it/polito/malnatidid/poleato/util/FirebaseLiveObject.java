package it.polito.malnatidid.poleato.util;

import android.databinding.BaseObservable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class FirebaseLiveObject<T> extends BaseObservable implements ChildEventListener {
    private OnFirebaseError eHandler = null;
    private final Firebase node;
    private final Map<String, Triplet<Integer, Method, Class<?>>> stringToSetter = new HashMap<>();

    protected FirebaseLiveObject(Class<T> type, String node) {
        this(type, new Firebase(node));
    }

    protected FirebaseLiveObject(Class<T> type, Firebase node) {
        super();
        this.node = node;
        for (Method m : type.getMethods()) {
            FirebaseSetter setter = m.getAnnotation(FirebaseSetter.class);
            if(setter != null) {
                StringBuffer sb = new StringBuffer(m.getName());
                sb.setCharAt(3, Character.toLowerCase(sb.charAt(3)));
                String normalName = sb.substring(3);
                stringToSetter.put(normalName, new Triplet<Integer, Method, Class<?>>(setter.value(), m, m.getParameterTypes()[0]));
            }
        }
        node.addChildEventListener(this);
    }

    public void setErrorHandler(OnFirebaseError eHandler) {
        this.eHandler = eHandler;
    }

    @Override
    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        if(stringToSetter.containsKey(dataSnapshot.getKey())) {
            Triplet<Integer, Method, Class<?>> p = stringToSetter.get(dataSnapshot.getKey());
            try {
                p.second.invoke(this, dataSnapshot.getValue(p.third));
                notifyPropertyChanged(p.first);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
        if(stringToSetter.containsKey(dataSnapshot.getKey())) {
            Triplet<Integer, Method, Class<?>> p = stringToSetter.get(dataSnapshot.getKey());
            try {
                p.second.invoke(this, dataSnapshot.getValue(p.third));
                notifyPropertyChanged(p.first);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) { }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) { }

    @Override
    public void onCancelled(FirebaseError firebaseError) {
        if(eHandler != null)
            eHandler.manage(firebaseError);
    }

    public void commit() {
        node.setValue(this);
    }
}
