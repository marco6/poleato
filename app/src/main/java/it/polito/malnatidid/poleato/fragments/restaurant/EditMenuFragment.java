package it.polito.malnatidid.poleato.fragments.restaurant;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import java.util.ArrayList;

import it.polito.malnatidid.poleato.data.Dish;
import it.polito.malnatidid.poleato.data.FBMenu;
import it.polito.malnatidid.poleato.databinding.ProfileResMenuEditBinding;

public class EditMenuFragment extends Fragment {

    private ProfileResMenuEditBinding bind;
    private Dish dish;
    private String sectionName;

    public static EditMenuFragment newInstance(Dish dish, FBMenu.MenuSection.Section type) {

        EditMenuFragment fragment = new EditMenuFragment();
        fragment.dish = dish;
        fragment.sectionName = type.uriSection;
        return fragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState != null) {
            dish = savedInstanceState.getParcelable("dish");
            sectionName = savedInstanceState.getString("sectionName");
        }

        if (dish == null) {
            bind.setEditName(true);
        } else {
            bind.setEditName(false);
            bind.setDish(dish);
        }

        ArrayList<String> types = new ArrayList<>();
        types.add(FBMenu.MenuSection.Section.APPETIZERS.uriSection);
        types.add(FBMenu.MenuSection.Section.FIRST_COURSES.uriSection);
        types.add(FBMenu.MenuSection.Section.SECOND_COURSES.uriSection);
        types.add(FBMenu.MenuSection.Section.SIDE_DISHES.uriSection);
        types.add(FBMenu.MenuSection.Section.DESSERTS.uriSection);
        types.add(FBMenu.MenuSection.Section.BEVERAGES.uriSection);

        bind.setTypes(types);

        if (bind.hasPendingBindings())
            bind.executePendingBindings();
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        if (args != null) {
            dish = args.getParcelable("dish");
            sectionName = args.getString("sectionName");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (dish != null)
            outState.putParcelable("dish", dish);
        if (sectionName != null){
            outState.putString("sectionName", sectionName);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        bind = ProfileResMenuEditBinding.inflate(inflater, container, false);

        bind.setOnSaveClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
            }
        });

        return bind.getRoot();
    }

    public void save() {

        Dish newDish = new Dish();

        newDish.setPhoto("pizza");
        newDish.setPrice(Float.valueOf("0" + bind.cost.getText().toString()));

        Firebase menuRef = new Firebase("https://poleato.firebaseio.com/menus");
        if(sectionName == null)
            sectionName = (String)bind.typeSpinner.getSelectedItem();

        Firebase dishRef = menuRef.child(menuRef.getAuth().getUid() + "/dishes/" + sectionName + "/" + bind.dishName.getText());
        dishRef.setValue(newDish);

        /*
         * Force keyboard closure when return on InfoFragment
         */
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

        getActivity().getSupportFragmentManager().popBackStack();
    }


}
