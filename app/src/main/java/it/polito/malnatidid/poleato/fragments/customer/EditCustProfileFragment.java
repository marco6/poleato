package it.polito.malnatidid.poleato.fragments.customer;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.firebase.client.Firebase;

import it.polito.malnatidid.poleato.data.FBProfileCustomer;
import it.polito.malnatidid.poleato.databinding.ProfileCustEditInfoBinding;

public class EditCustProfileFragment extends Fragment {
    private FBProfileCustomer custProfile;
    private ProfileCustEditInfoBinding bind;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Firebase node = new Firebase("https://poleato.firebaseio.com/");

        bind = ProfileCustEditInfoBinding.inflate(inflater, container, false);

        bind.setOnSaveClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveProfile();
            }
        });

        if (node.getAuth() != null) {
            custProfile = new FBProfileCustomer(node.getAuth().getUid());
            bind.setProfile(custProfile);
        }

        if (bind.hasPendingBindings())
            bind.executePendingBindings();

        return bind.getRoot();
    }

    /**
     * Called when the user clicks the Save button
     */
    public void saveProfile() {

        custProfile.setAddress(bind.addressEdit.getText().toString());
        custProfile.setPhone(bind.phoneEdit.getText().toString());
        custProfile.setName(bind.nameEdit.getText().toString());
        custProfile.setSurname(bind.surnameEdit.getText().toString());
        custProfile.commit();

        /*
         * Force keyboard closure when return on InfoFragment
         */
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        getFragmentManager().popBackStack();
    }
}