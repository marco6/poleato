package it.polito.malnatidid.poleato.util;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    private class TabInfo {
        public final Class<? extends Fragment> type;
        public final String name;
        public Bundle args;

        public TabInfo(Class<? extends Fragment> type, Resources res, Bundle args) {
            TabFragment notes = type.getAnnotation(TabFragment.class);
            this.args = args;
            this.type = type;
            if (notes == null) {
                this.name = type.getName();
            } else {
                name = res.getString(notes.value());
            }
        }
    }

    private final TabInfo[] tabs;

    @SafeVarargs
    public ViewPagerAdapter(FragmentManager fm, Resources res, Class<? extends Fragment>... fragments) {
        this(fm, res, null, fragments);
    }

    @SafeVarargs
    public ViewPagerAdapter(FragmentManager fm, Resources res, Bundle args, Class<? extends Fragment>... fragments) {
        super(fm);
        tabs = new TabInfo[fragments.length];
        // Ora carico i nomi
        for (int i = 0; i < fragments.length; i++)
            tabs[i] = new TabInfo(fragments[i], res, args);
    }

    @Override
    public Fragment getItem(int position) {
        try {
            Fragment f = tabs[position].type.newInstance();
            if (tabs[position].args != null) {
                f.setArguments(tabs[position].args);
            }
            return f;
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int getCount() {
        return tabs.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabs[position].name;
    }
}
