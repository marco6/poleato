package it.polito.malnatidid.poleato.util;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;

public class ParcelableHashMap<V extends Parcelable> extends HashMap<String, V> implements Parcelable{

    public ParcelableHashMap(Parcel in) {
        super();

        int n = in.readInt();

        for(int i=0; i<n; i++)
            put(in.readString(), (V)in.readParcelable(getClass().getClassLoader()));
    }

    public ParcelableHashMap(){
        super();
    }

    public static final Creator<ParcelableHashMap> CREATOR = new Creator<ParcelableHashMap>() {
        @Override
        public ParcelableHashMap createFromParcel(Parcel in) {
            return new ParcelableHashMap(in);
        }

        @Override
        public ParcelableHashMap[] newArray(int size) {
            return new ParcelableHashMap[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(size());
        for(Entry<String, V> e : this.entrySet()) {
            dest.writeString(e.getKey());
            dest.writeParcelable(e.getValue(), flags);
        }
    }
}
