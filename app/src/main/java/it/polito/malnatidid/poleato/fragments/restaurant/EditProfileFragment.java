package it.polito.malnatidid.poleato.fragments.restaurant;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;

import org.json.JSONException;

import java.util.Calendar;
import java.util.GregorianCalendar;

import it.polito.malnatidid.poleato.R;
import it.polito.malnatidid.poleato.data.Coordinates;
import it.polito.malnatidid.poleato.data.FBProfileRestaurant;
import it.polito.malnatidid.poleato.data.Timetable;
import it.polito.malnatidid.poleato.databinding.ProfileResEditInfoBinding;
import it.polito.malnatidid.poleato.fragments.TimePickerFragment;
import it.polito.malnatidid.poleato.util.FragmentPipe;
import it.polito.malnatidid.poleato.util.MapsUtility;
import it.polito.malnatidid.poleato.util.Triplet;

public class EditProfileFragment extends Fragment {

    private FBProfileRestaurant profile;
    private ProfileResEditInfoBinding bind;

    private Firebase node = new Firebase("https://poleato.firebaseio.com/");
    private String uid;
    private FragmentPipe<Pair<Integer, Integer>> beginTimePipe;
    private FragmentPipe<Pair<Integer, Integer>> endTimePipe;
    private final GregorianCalendar chosenBeginTime = new GregorianCalendar();
    private final GregorianCalendar chosenEndTime = new GregorianCalendar();

    private class timeListener implements FragmentPipe.IReceiver<Pair<Integer, Integer>> {
        private String type;

        public timeListener(String type) {
            this.type = type;
        }

        @Override
        public void deliver(Pair<Integer, Integer> pack) {
            if (type.equals("begin")) {
                chosenBeginTime.set(Calendar.HOUR_OF_DAY, pack.first);
                chosenBeginTime.set(Calendar.MINUTE, pack.second);
                bind.setCurrBegCal(chosenBeginTime);
            } else if (type.equals("end")) {
                chosenEndTime.set(Calendar.HOUR_OF_DAY, pack.first);
                chosenEndTime.set(Calendar.MINUTE, pack.second);
                bind.setCurrEndCal(chosenEndTime);
            } else
                System.err.println("ERROR: wrong listener type");
        }
    }

    public EditProfileFragment() {
        chosenBeginTime.set(Calendar.MILLISECOND, 0); // Clear milliseconds
        chosenBeginTime.set(Calendar.SECOND, 0); // Clear seconds

        chosenEndTime.set(Calendar.MILLISECOND, 0); // Clear milliseconds
        chosenEndTime.set(Calendar.SECOND, 0); // Clear seconds
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelable("beginTimePipe", beginTimePipe);
        outState.putParcelable("endTimePipe", endTimePipe);
        outState.putString("uid", uid);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState != null) {
            beginTimePipe = savedInstanceState.getParcelable("beginTimePipe");
            endTimePipe = savedInstanceState.getParcelable("endTimePipe");
            uid = savedInstanceState.getString("uid");
        } else {
            beginTimePipe = new FragmentPipe<>();
            endTimePipe = new FragmentPipe<>();
        }

        beginTimePipe.setReceiver(new timeListener("begin"));
        endTimePipe.setReceiver(new timeListener("end"));

        final TimePickerFragment beginTimePicker = TimePickerFragment.newInstance(beginTimePipe, chosenBeginTime);
        final TimePickerFragment endTimePicker = TimePickerFragment.newInstance(endTimePipe, chosenEndTime);

        bind.setOnShowBeginTimePickerDialog(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                beginTimePicker.show(getFragmentManager(), "beginTimePicker");
            }
        });

        bind.setOnShowEndTimePickerDialog(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endTimePicker.show(getFragmentManager(), "endTimePicker");
            }
        });

        if (bind.hasPendingBindings())
            bind.executePendingBindings();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        bind = ProfileResEditInfoBinding.inflate(inflater, container, false);

        bind.setOnSaveClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    saveProfile();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        if (node.getAuth() != null) {
            uid = node.getAuth().getUid();
            profile = new FBProfileRestaurant(uid);
            bind.setProfile(profile);
        }

        bind.setCurrBegCal(chosenBeginTime);
        bind.setCurrEndCal(chosenEndTime);

        node.child("users/" + uid + "/timetable/start").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                chosenBeginTime.setTimeInMillis((long) dataSnapshot.getValue());
                bind.setCurrBegCal(chosenBeginTime);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                System.out.println(firebaseError.toString());
            }
        });

        node.child("users/" + uid + "/timetable/end").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                chosenEndTime.setTimeInMillis((long) dataSnapshot.getValue());
                bind.setCurrEndCal(chosenEndTime);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                System.out.println(firebaseError.toString());
            }
        });


        return bind.getRoot();
    }

    /**
     * Called when the user clicks the Save button
     */
    public void saveProfile() throws JSONException {

        String newAddress = bind.addressEdit.getText().toString();

        if(!profile.getAddress().equals(newAddress)) {
            Triplet<Double, Double, String> t = MapsUtility.getCoordinates(getActivity(), newAddress);

            if (t != null) {
                GeoFire geoFire = new GeoFire(node.child("geofire"));
                profile.setAddress(newAddress);
                profile.setCoordinates(new Coordinates(t.first, t.second));
                geoFire.setLocation(uid, new GeoLocation((double) t.first, (double) t.second));
            } else
                Toast.makeText(getActivity(), R.string.address_error, Toast.LENGTH_LONG).show();
        }

        profile.setPhone(bind.contactEdit.getText().toString());
        profile.setDelivery(bind.deliveryEdit.isChecked());
        profile.setSelfService(bind.selServiceEdit.isChecked());
        profile.setTakeAway(bind.takeAwayEdit.isChecked());
        profile.setServed(bind.servedEdit.isChecked());

        profile.setPizza(bind.pizza.isChecked());
        profile.setAsian(bind.asian.isChecked());
        profile.setCeliac(bind.celiac.isChecked());
        profile.setGreek(bind.greek.isChecked());
        profile.setFastFood(bind.fastfood.isChecked());
        profile.setIndian(bind.indian.isChecked());
        profile.setKebab(bind.kebab.isChecked());
        profile.setMexican(bind.mexican.isChecked());
        profile.setVegetarian(bind.vegetarian.isChecked());
        profile.setOther(bind.others.isChecked());

        profile.setTimetable(new Timetable(chosenBeginTime.getTimeInMillis(), chosenEndTime.getTimeInMillis(),
                bind.day1.isChecked(),
                bind.day2.isChecked(),
                bind.day3.isChecked(),
                bind.day4.isChecked(),
                bind.day5.isChecked(),
                bind.day6.isChecked(),
                bind.day7.isChecked()));

        profile.setDescription(bind.descriptionEdit.getText().toString());

        profile.commit();

        /*
         * Force keyboard closure when return on InfoFragment
         */
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        getFragmentManager().popBackStack();
    }

}
