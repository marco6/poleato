package it.polito.malnatidid.poleato.fragments.customer;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.client.Firebase;

import it.polito.malnatidid.poleato.R;
import it.polito.malnatidid.poleato.data.FBReservations;
import it.polito.malnatidid.poleato.databinding.ReservationTableItemBinding;
import it.polito.malnatidid.poleato.util.FBRecycleViewFragment;
import it.polito.malnatidid.poleato.util.TabFragment;

@TabFragment(R.string.pending_table)
public class HistoryTableReservFragment extends FBRecycleViewFragment<FBReservations.TablesSection, ReservationTableItemBinding> {

    public HistoryTableReservFragment() {
        super(R.layout.reservation_table_item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setData(new FBReservations.TablesSection(new Firebase("https://poleato.firebaseio.com").getAuth().getUid(),
                true, (System.currentTimeMillis() - 1000*60*60), -1 >>> 1));

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    protected void bindItem(BindingsHolder holder, final FBReservations.TablesSection data, final int position) {
        holder.bindings.setRes(data.get(position));

        if( data.get(position).getDate() > System.currentTimeMillis()) {
            holder.bindings.setOnDeleteClick(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    Firebase fb = new Firebase("https://poleato.firebaseio.com/reservations/tables/" + data.get(position).getKey());
                    fb.removeValue();
                }
            });
        }
    }
}
