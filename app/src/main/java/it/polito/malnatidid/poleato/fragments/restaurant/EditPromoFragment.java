package it.polito.malnatidid.poleato.fragments.restaurant;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.firebase.client.Firebase;

import java.util.HashMap;
import java.util.Map;

import it.polito.malnatidid.poleato.NotificationService;
import it.polito.malnatidid.poleato.R;
import it.polito.malnatidid.poleato.data.Dish;
import it.polito.malnatidid.poleato.data.FBMenu;
import it.polito.malnatidid.poleato.data.SpecialOffer;
import it.polito.malnatidid.poleato.databinding.ProfileResPromoEditBinding;
import it.polito.malnatidid.poleato.databinding.ResPromoEditItemBinding;
import it.polito.malnatidid.poleato.util.FBImageCache;
import it.polito.malnatidid.poleato.util.IntRef;
import it.polito.malnatidid.poleato.util.ParcelableHashMap;

public class EditPromoFragment extends Fragment {

    private ProfileResPromoEditBinding bind;
    private SpecialOffer promo;
    private ParcelableHashMap<IntRef> dishes;

    public String photo = "";
    public FBMenu menu;

    private AlertDialog chooser;

    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int GALLERY_PICTURE = 2;

    private Firebase fb = new Firebase("https://poleato.firebaseio.com/");

    public static EditPromoFragment newInstance(SpecialOffer promo) {

        EditPromoFragment fragment = new EditPromoFragment();
        fragment.promo = promo;
        return fragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("promo"))
                promo = savedInstanceState.getParcelable("promo");

            dishes = savedInstanceState.getParcelable("dishes");
        }

        if (promo == null) {
            bind.setEditName(true);
            if (dishes == null)
                dishes = new ParcelableHashMap<>();
        } else {
            if (dishes == null) {
                dishes = new ParcelableHashMap<>();
                for (Map.Entry<String, Integer> entry : promo.getDishes().entrySet())
                    dishes.put(entry.getKey(), new IntRef(entry.getValue()));
            }
            bind.setOffer(promo);
            bind.setEditName(false);
        }

        menu = new FBMenu(fb.getAuth().getUid());

        bind.setFBmenu(menu);

        chooser = new AlertDialog.Builder(getContext())
                .setTitle(R.string.select_picture)
                .setMessage(R.string.mode_picture_select)
                .setPositiveButton(R.string.gallery, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        Intent pickPhoto = new Intent(Intent.ACTION_GET_CONTENT);
                        pickPhoto.setType("image/*");
                        startActivityForResult(pickPhoto, GALLERY_PICTURE);
                    }
                })
                .setNegativeButton(R.string.camera, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null)
                            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    }
                }).create();

        bind.setOnAddPhotoClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooser.show();
            }
        });


        if (bind.hasPendingBindings())
            bind.executePendingBindings();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {

            if (resultCode == Activity.RESULT_OK) {

                if (promo != null) {
                    Firebase updater = new Firebase("https://poleato.firebaseio.com/menus/");
                    updater = updater.child(updater.getAuth().getUid() + "/special_offers/" + promo.getKey() + "/photo");

                    switch (requestCode) {
                        case REQUEST_IMAGE_CAPTURE:
                            photo = FBImageCache.getInstance().upload(
                                    (Bitmap) data.getExtras().get("data"));
                            updater.setValue(photo);
                            break;
                        case GALLERY_PICTURE:
                            photo = FBImageCache.getInstance().upload(
                                    getContext(), data.getData());
                            updater.setValue(photo);
                            break;
                    }
                } else {
                    switch (requestCode) {
                        case REQUEST_IMAGE_CAPTURE:
                            photo = FBImageCache.getInstance().upload(
                                    (Bitmap) data.getExtras().get("data"));
                            break;
                        case GALLERY_PICTURE:
                            photo = FBImageCache.getInstance().upload(
                                    getContext(), data.getData());
                            break;
                    }
                }
                bind.setPhoto(photo);
            }
        }
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        if (args != null) {
            if (args.containsKey("promo"))
                promo = args.getParcelable("promo");
            if (args.containsKey("dishes"))
                dishes = args.getParcelable("dishes");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (promo != null)
            outState.putParcelable("promo", promo);

        outState.putParcelable("dishes", dishes);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        bind = ProfileResPromoEditBinding.inflate(inflater, container, false);

        bind.setOnAddClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addDish();
            }
        });

        bind.menuPromoList.setAdapter(new ProfileResPromoEditAdapter());
        bind.menuPromoList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        bind.menuPromoList.setItemAnimator(new DefaultItemAnimator());

        bind.setOnSaveClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                savePromo();
            }
        });

        return bind.getRoot();
    }

    private class ProfileResPromoEditViewHolder extends RecyclerView.ViewHolder {
        private ResPromoEditItemBinding binding;

        public ProfileResPromoEditViewHolder(final ResPromoEditItemBinding bind) {
            super(bind.getRoot());

            bind.setOnDeleteClick(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    delete(bind.getDishName());
                }
            });

            bind.setOnAddClick(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    add(bind.getDishName());
                }
            });
            binding = bind;
        }

        public ResPromoEditItemBinding getBindings() {
            return binding;
        }
    }

    private class ProfileResPromoEditAdapter extends RecyclerView.Adapter<ProfileResPromoEditViewHolder> {
        @Override
        public ProfileResPromoEditViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            ResPromoEditItemBinding bind = ResPromoEditItemBinding.inflate(
                    getActivity().getLayoutInflater(), parent, true);
            return new ProfileResPromoEditViewHolder(bind);
        }

        @Override
        public void onBindViewHolder(ProfileResPromoEditViewHolder holder, int position) {
            holder.getBindings().setDishName((String) dishes.keySet().toArray()[position]);
            holder.getBindings().setDishQuantity(((IntRef) (dishes.values().toArray()[position])).value);
        }

        @Override
        public int getItemCount() {
            if (dishes == null)
                return 0;
            return dishes.size();
        }
    }

    public void addDish() {
        Dish currentDish = (Dish) bind.spinnerFirstDish.getSelectedItem();

        if (dishes.containsKey(currentDish.getName()))
            dishes.get(currentDish.getName()).value++;
        else
            dishes.put(currentDish.getName(), new IntRef(1));

        bind.menuPromoList.getAdapter().notifyDataSetChanged();
    }

    public void savePromo() {

        if ((Float.parseFloat(bind.promoPrice.getText().toString()) < 0)||(dishes.size() == 0) || "".equals(bind.promotionName.getText().toString())) {
            /*
             * Empty promotion or price not valid: do nothing
             */
            Toast.makeText(getActivity().getApplicationContext(), R.string.invalid_promotion, Toast.LENGTH_LONG).show();
            return;
        }

        SpecialOffer newPromo = new SpecialOffer();

        newPromo.setPrice(Float.parseFloat("0" + bind.promoPrice.getText().toString()));

        Map<String, Integer> newDishes = new HashMap<>();

        for (Map.Entry<String, IntRef> entry : dishes.entrySet())
            newDishes.put(entry.getKey(), entry.getValue().value);

        newPromo.setDishes(newDishes);
        newPromo.setKey(bind.promotionName.getText().toString());
        if (promo != null)
            newPromo.setPhoto(menu.offers.get(bind.promotionName.getText().toString()).getPhoto());
        else
            newPromo.setPhoto(photo);


        fb.child("/menus/" + fb.getAuth().getUid() + "/special_offers/" + newPromo.getKey()).setValue(newPromo);
        fb.child("/notify/user/promotion").push().setValue(new NotificationService.Promotion(fb.getAuth().getUid(), newPromo.getKey()));

        /*
         * Force keyboard closure when return on InfoFragment
         */
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        getFragmentManager().popBackStack();
    }

    protected void delete(String dishName) {

        if (dishes.get(dishName).value == 1)
            dishes.remove(dishName);
        else
            dishes.get(dishName).value--;

        bind.menuPromoList.getAdapter().notifyDataSetChanged();
    }

    protected void add(String dishName) {
        dishes.get(dishName).value++;
        bind.menuPromoList.getAdapter().notifyDataSetChanged();
    }

}
