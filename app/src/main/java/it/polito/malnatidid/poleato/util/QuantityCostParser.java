package it.polito.malnatidid.poleato.util;


public class QuantityCostParser {

    public int quantity;
    public float cost;

    public QuantityCostParser(String s){

        if(s == null){
            quantity = 0;
            cost = 0.0f;
        }else {
            quantity = Integer.parseInt(s.split("/")[0]);
            cost = Float.parseFloat(s.split("/")[1]);
        }
    }

    public String parse(){
        return String.valueOf(quantity) + "/" + String.valueOf(cost);
    }

}
