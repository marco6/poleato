package it.polito.malnatidid.poleato.data;

import it.polito.malnatidid.poleato.util.FirebaseCollectionBase;
import it.polito.malnatidid.poleato.util.KeyedValue;

public class FBReviews extends FirebaseCollectionBase<String, FBReviews.SingleReview> {

    public FBReviews(String uid, String childName) {
        super(SingleReview.class, "https://poleato.firebaseio.com/reviews/", childName, uid);
    }

    @Override
    public String getKey(String keyString) {
        return keyString;
    }

    public static class SingleReview implements KeyedValue<String> {
        private String key;
        private String title;
        private String comment;
        private long date;
        private Evaluation evaluation;
        private String cust_name;
        private String cust_uid;
        private String res_name;
        private String res_uid;
        private String photo;

        public SingleReview() {
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public String getCust_name() {
            return cust_name;
        }

        public void setCust_name(String cust_name) {
            this.cust_name = cust_name;
        }

        public String getCust_uid() {
            return cust_uid;
        }

        public void setCust_uid(String cust_uid) {
            this.cust_uid = cust_uid;
        }

        public String getRes_name() {
            return res_name;
        }

        public void setRes_name(String res_name) {
            this.res_name = res_name;
        }

        public String getRes_uid() {
            return res_uid;
        }

        public void setRes_uid(String res_uid) {
            this.res_uid = res_uid;
        }

        public Evaluation getEvaluation() {
            return evaluation;
        }

        public void setEvaluation(Evaluation evaluation) {
            this.evaluation = evaluation;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public long getDate() {
            return date;
        }

        public void setDate(long date) {
            this.date = date;
        }

        @Override
        public String getKey() {
            return key;
        }

        @Override
        public void setKey(String key) {
            this.key = key;
        }
    }

}
