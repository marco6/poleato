package it.polito.malnatidid.poleato.util;

public class Triplet <F, S, T> {

    public final F first;
    public final S second;
    public final T third;

    public Triplet(F one, S two, T three){
        first = one;
        second = two;
        third = three;
    }
}
