package it.polito.malnatidid.poleato.fragments.customer;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.firebase.client.AuthData;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.firebase.client.annotations.NotNull;

import java.util.Calendar;
import java.util.GregorianCalendar;

import it.polito.malnatidid.poleato.R;
import it.polito.malnatidid.poleato.data.FBReservations;
import it.polito.malnatidid.poleato.data.Timetable;
import it.polito.malnatidid.poleato.databinding.AddReservationCustTableBinding;
import it.polito.malnatidid.poleato.fragments.DatePickerFragment;
import it.polito.malnatidid.poleato.fragments.LoadingDialogFragment;
import it.polito.malnatidid.poleato.fragments.LoginFragment;
import it.polito.malnatidid.poleato.fragments.TimePickerFragment;
import it.polito.malnatidid.poleato.util.FragmentPipe;
import it.polito.malnatidid.poleato.util.Triplet;

public class AddReservationTableFragment extends Fragment
        implements View.OnClickListener {

    private String resID;

    public static AddReservationTableFragment newInstance(FragmentPipe<FBReservations.TableReservation> pipe, String resID) {
        AddReservationTableFragment lf = new AddReservationTableFragment();
        lf.resultPipe = pipe;
        lf.resID = resID;
        return lf;
    }

    private class timeListener implements FragmentPipe.IReceiver<Pair<Integer, Integer>> {
        @Override
        public void deliver(Pair<Integer, Integer> pack) {
            chosenTime.set(Calendar.HOUR_OF_DAY, pack.first);
            chosenTime.set(Calendar.MINUTE, pack.second);
            bind.setCurrCal(chosenTime);
        }
    }

    private class dayListener implements FragmentPipe.IReceiver<Triplet<Integer, Integer, Integer>> {
        @Override
        public void deliver(Triplet<Integer, Integer, Integer> pack) {
            chosenTime.set(pack.first, pack.second, pack.third);
            bind.setCurrCal(chosenTime);
        }
    }

    private class authListener implements FragmentPipe.IReceiver<AuthData> {
        @Override
        public void deliver(AuthData pack) {
            save(pack);
            getFragmentManager().popBackStack();
        }
    }

    private AddReservationCustTableBinding bind;
    private FragmentPipe<Pair<Integer, Integer>> timePipe;
    private FragmentPipe<Triplet<Integer, Integer, Integer>> dayPipe;
    private FragmentPipe<AuthData> authPipe;
    private FragmentPipe<FBReservations.TableReservation> resultPipe;
    private final LoadingDialogFragment loading = new LoadingDialogFragment();

    private final GregorianCalendar chosenTime = new GregorianCalendar();

    public AddReservationTableFragment() {
        chosenTime.set(Calendar.MILLISECOND, 0); // Clear milliseconds
        chosenTime.set(Calendar.SECOND, 0); // Clear seconds
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("authPipe", authPipe);
        outState.putParcelable("timePipe", timePipe);
        outState.putParcelable("dayPipe", dayPipe);
        outState.putParcelable("resultPipe", resultPipe);
        outState.putString("resID", resID);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final DatePickerFragment datePicker = DatePickerFragment.newInstance(dayPipe, chosenTime);
        final TimePickerFragment timePicker = TimePickerFragment.newInstance(timePipe, chosenTime);

        bind.setOnShowDatePickerDialog(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePicker.show(getFragmentManager(), "datePicker");
            }
        });

        bind.setOnShowTimePickerDialog(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timePicker.show(getFragmentManager(), "timePicker");
            }
        });

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            authPipe = savedInstanceState.getParcelable("authPipe");
            timePipe = savedInstanceState.getParcelable("timePipe");
            dayPipe = savedInstanceState.getParcelable("dayPipe");
            resultPipe = savedInstanceState.getParcelable("resultPipe");
            resID = savedInstanceState.getString("resID");
        } else {
            authPipe = new FragmentPipe<>();
            timePipe = new FragmentPipe<>();
            dayPipe = new FragmentPipe<>();
        }
        authPipe.setReceiver(new authListener());
        timePipe.setReceiver(new timeListener());
        dayPipe.setReceiver(new dayListener());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView( inflater,  container,  savedInstanceState);
        bind = AddReservationCustTableBinding.inflate(inflater, container, false);

        bind.setOnOkClick(this);

        bind.setCurrCal(chosenTime);

        if (bind.hasPendingBindings())
            bind.executePendingBindings();

        return bind.getRoot();
    }

    @Override
    public void onClick(View v) {

        if (chosenTime.getTimeInMillis() < System.currentTimeMillis()) {
            /* Reservation in the past: do nothing*/
            Toast.makeText(getActivity().getApplicationContext(), R.string.reservation_past, Toast.LENGTH_LONG).show();
            return;
        }

        loading.show(getFragmentManager(), "Loading...");

        final Firebase base = new Firebase("https://poleato.firebaseio.com/");

        base.child("users/" + resID + "/timetable").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Timetable timetable = dataSnapshot.getValue(Timetable.class);
                GregorianCalendar endTime = new GregorianCalendar();
                GregorianCalendar startTime = new GregorianCalendar();

                endTime.setTimeInMillis(timetable.getEnd());
                startTime.setTimeInMillis(timetable.getStart());

                loading.dismiss();

                boolean day;

                switch (chosenTime.get(Calendar.DAY_OF_WEEK)) {

                    case Calendar.MONDAY:
                        day = timetable.isLun();
                        break;
                    case Calendar.TUESDAY:
                        day = timetable.isMar();
                        break;
                    case Calendar.WEDNESDAY:
                        day = timetable.isMer();
                        break;
                    case Calendar.THURSDAY:
                        day = timetable.isGio();
                        break;
                    case Calendar.FRIDAY:
                        day = timetable.isVen();
                        break;
                    case Calendar.SATURDAY:
                        day = timetable.isSab();
                        break;
                    case Calendar.SUNDAY:
                        day = timetable.isDom();
                        break;
                    default:
                        day = false;
                        break;

                }

                if (!day) {
                    Toast.makeText(getContext(), "Non è possibile fare prenotazioni nei giorni di chiusura del ristorante.",
                            Toast.LENGTH_LONG).show();
                    return;
                }

                if ((chosenTime.get(Calendar.HOUR_OF_DAY) < startTime.get(Calendar.HOUR_OF_DAY)) ||

                        (chosenTime.get(Calendar.HOUR_OF_DAY) > endTime.get(Calendar.HOUR_OF_DAY)) ||

                        ((chosenTime.get(Calendar.HOUR_OF_DAY) == endTime.get(Calendar.HOUR_OF_DAY)) &&
                                ((chosenTime.get(Calendar.MINUTE) < startTime.get(Calendar.MINUTE)) ||
                                        (chosenTime.get(Calendar.MINUTE) > endTime.get(Calendar.MINUTE))))) {

                    Toast.makeText(getContext(), "Non è possibile fare prenotazioni fuori dall'orario " +
                            "specificato dal ristoratore " + startTime.get(Calendar.HOUR_OF_DAY) + ":" + startTime.get(Calendar.MINUTE)
                            + " - " + endTime.get(Calendar.HOUR_OF_DAY) + ":" + endTime.get(Calendar.MINUTE), Toast.LENGTH_LONG).show();
                    return;
                }

                AuthData auth = base.getAuth();

                if (auth == null) {
                    getFragmentManager()
                            .beginTransaction()
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                            .add(R.id.fraghost, LoginFragment.newInstance(authPipe), "login")
                            .addToBackStack(null)
                            .commit();
                } else
                    save(auth);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                loading.dismiss();
                System.err.println(firebaseError.getDetails());
            }
        });
    }

    private void save(@NotNull AuthData auth) {

        FBReservations.TableReservation newTableRes = new FBReservations.TableReservation();

        newTableRes.setCust_uid(auth.getUid());
        newTableRes.setDate(chosenTime.getTimeInMillis());
        newTableRes.setPersons(bind.numberPicker.getValue());

        resultPipe.deliver(newTableRes);

        getFragmentManager().popBackStack();
        Toast.makeText(getContext(), R.string.succesfull_table_reservation, Toast.LENGTH_LONG).show();
    }
}
